/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */
package com.capricorn;

public final class R {
	public static final class attr {
		public static final int childSize = 0x7f010084;
		public static final int fromDegrees = 0x7f010082;
		public static final int leftHolderWidth = 0x7f010085;
		public static final int toDegrees = 0x7f010083;
	}
	public static final class drawable {
		public static final int composer_button = 0x7f020097;
		public static final int composer_button_normal = 0x7f020098;
		public static final int composer_button_pressed = 0x7f020099;
		public static final int composer_icn_plus = 0x7f02009a;
		public static final int composer_icn_plus_normal = 0x7f02009b;
		public static final int composer_icn_plus_pressed = 0x7f02009c;
	}
	public static final class id {
		public static final int control_hint = 0x7f0b0078;
		public static final int control_layout = 0x7f0b0077;
		public static final int item_layout = 0x7f0b0076;
	}
	public static final class layout {
		public static final int arc_menu = 0x7f03001b;
		public static final int ray_menu = 0x7f03003e;
	}
	public static final class styleable {
		public static final int[] ArcLayout = { 0x7f010082, 0x7f010083, 0x7f010084 };
		public static final int ArcLayout_childSize = 2;
		public static final int ArcLayout_fromDegrees = 0;
		public static final int ArcLayout_toDegrees = 1;
		public static final int[] ArcMenu = { 0x7f010082, 0x7f010083, 0x7f010084 };
		public static final int ArcMenu_childSize = 2;
		public static final int ArcMenu_fromDegrees = 0;
		public static final int ArcMenu_toDegrees = 1;
		public static final int[] RayLayout = { 0x7f010085 };
		public static final int RayLayout_leftHolderWidth = 0;
	}
}
