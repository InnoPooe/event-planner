package com.eventplanner;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.widget.TextView;
import cn.pedant.SweetAlert.SweetAlertDialog;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.eventplanner.databasetables.TableEvents;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class EventVenueOnMapActivity extends SherlockFragmentActivity{
	private TableEvents _SelectedEvent;
	
	private double _Latitude, _Longitude;
	
	public GoogleMap mMap;
	
	private TextView textEventName, textClientName, textEventVenue, textEventDate, textEventType;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mappreview_activity);
        
        getSupportActionBar().show();
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.background_img4));
        
        getSupportActionBar().setTitle("Event Planner");
        getSupportActionBar().setSubtitle("Managing Events");
         
        getSupportActionBar().setLogo(R.drawable.ic_launcher);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
         
        if(savedInstanceState == null){
        	if(getIntent().getExtras() != null){
        		_SelectedEvent = (TableEvents) getIntent().getExtras().getSerializable("_SelectedEvent");
        	}
        }
        else{
        	_SelectedEvent = (TableEvents) savedInstanceState.getSerializable("_SelectedEvent");
        	_Latitude = savedInstanceState.getDouble("_Latitude");
        	_Longitude = savedInstanceState.getDouble("_Longitude");
        }
        
        textEventName = (TextView) findViewById(R.id.textEventName);
        textClientName = (TextView) findViewById(R.id.textClientName);
        textEventVenue  = (TextView) findViewById(R.id.textEventVenue);
        textEventDate = (TextView) findViewById(R.id.textEventDate);
        textEventType = (TextView) findViewById(R.id.textEventType);
        
        SimpleDateFormat f = new SimpleDateFormat("dd MMM yy HH:mm", Locale.US);
        
        
        
        mMap = ((SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
		mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		ZoomToDefault(-29.619031937438404, 24.002343825995922, 5);
		
		if(_SelectedEvent != null){
        	textEventDate.setText(f.format(_SelectedEvent.getEventDate()));
        	textEventName.setText(_SelectedEvent.getDescription());
        	textEventType.setText(_SelectedEvent.getEventType().getDescription());
        	textClientName.setText(_SelectedEvent.getClient().getLastName() + " " + _SelectedEvent.getClient().getFullName());
        	textEventVenue.setText(_SelectedEvent.getEventVenue());
        	
    		getMyLocationAddress();
        }
    }
	
	@Override
	public void onSaveInstanceState(Bundle state) {
	    super.onSaveInstanceState(state);
	
	    state.putSerializable("_SelectedEvent", _SelectedEvent);
	    state.putDouble("_Latitude", _Latitude);
	    state.putDouble("_Longitude", _Longitude);
	}
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
		
		try
		{
			menu.add("Map View").setNumericShortcut('M')
    		.setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
			
			menu.add("Sattelite View").setNumericShortcut('S')
    		.setShowAsAction(MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
		}
		catch(Exception Ex)
		{
			Ex.printStackTrace();
		}
		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
    	if(item.getItemId() == android.R.id.home)
    	{
			finish();
    	}
    	else if(item.getNumericShortcut() == 'M')
		{
			mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		}
		else if(item.getNumericShortcut() == 'S')
		{
			mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
		}
    	return false;
    }
	 
	public void ZoomToDefault(double lat, double lon, int zoomlevel)
    {
    	try
    	{
	    	CameraUpdate center=
	    	        CameraUpdateFactory.newLatLng(new LatLng(lat, lon));
	    	
	    	CameraUpdate zoom = CameraUpdateFactory.zoomTo(zoomlevel);
	    	
	    	mMap.clear();
	    	mMap.moveCamera(center);
	    	mMap.animateCamera(zoom);
    	}
    	catch(Exception Ex)
    	{
    		Ex.printStackTrace();
    	}
    }
	
	public void getMyLocationAddress() {
        
        Geocoder geocoder= new Geocoder(this, Locale.ENGLISH);
        SweetAlertDialog pDialog = null;
        try{
			  pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE)
			  .setTitleText("Loading");
			  pDialog.show();
			  pDialog.setCancelable(false);

			  boolean requestMade = false;
			  List<Address> addresses = null;
			  
			  while(pDialog.isShowing()){
				  //_SelectedEvent.getEventVenue()
				 if(!requestMade){
					 addresses = geocoder.getFromLocationName(_SelectedEvent.getEventVenue(), 1);
					 
					 if(pDialog != null)
						  	pDialog.dismiss();
					 
					 requestMade = true;
				 } 
			  }
			  
			  if(addresses != null && addresses.size() > 0) {
			   
			      Address location = addresses.get(0);
			      
			      _Latitude = location.getLatitude();
			      _Longitude = location.getLongitude();
			      
			      LatLng _LatLng = new LatLng(_Latitude,_Longitude);
			      
			      mMap.addMarker(new MarkerOptions()
		            .position(_LatLng)
		            .title("Event: " + _SelectedEvent.getDescription())
		            .snippet("Venue: " + _SelectedEvent.getEventVenue())
		            .icon(BitmapDescriptorFactory.defaultMarker()));
			      
			      CameraPosition position = new CameraPosition.Builder()
				    .target(_LatLng)
				    .zoom(16).build();
				
				    mMap.animateCamera(CameraUpdateFactory.newCameraPosition(position));
			      
			  }
			  else{
				  new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
			      .setTitleText("Oops...")
			      .setContentText("Location Not Found").show();
			  }
          
        } 
        catch (IOException e) {
                 // TODO Auto-generated catch block
             e.printStackTrace();
             new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
             .setTitleText("Oops...")
             .setContentText("Location Not Found")
             .show();
             
             if(pDialog != null)
				  	pDialog.dismiss();
        }
    }
}
