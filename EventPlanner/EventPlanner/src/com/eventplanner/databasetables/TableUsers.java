package com.eventplanner.databasetables;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tableusers")
public class TableUsers implements Serializable{

	private static final long serialVersionUID = 1L;

	@DatabaseField(id = true)
    private String id;
	
	@DatabaseField(canBeNull = false, unique = true)
    private String cloundid;
	
	@DatabaseField(canBeNull = false)
	private String fullname;
	
	@DatabaseField(canBeNull = false)
	private String lastname;
	
	@DatabaseField(canBeNull = false)
	private String mobilenumber;
	
	@DatabaseField(canBeNull = false)
	private String email;
	
	@DatabaseField(canBeNull = false)
	private String password;
	
	@DatabaseField(canBeNull = false)
	private Date date_added;
	
	public TableUsers() {
		// TODO Auto-generated constructor stub
	}

	public TableUsers(String cloundid, String fullname, String lastname, String mobilenumber,  String email, String password) {
		// TODO Auto-generated constructor stub
		
		Calendar _Cal = Calendar.getInstance();
		
		UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        
        this.id = randomUUIDString;
        this.cloundid = cloundid;
        this.fullname = fullname;
        this.lastname = lastname;
        this.mobilenumber = mobilenumber;
        this.email = email;
        this.password = password;
        this.date_added = _Cal.getTime();
	}
	
	public TableUsers(String id, String fullname, String lastname, String mobilenumber, String telnumber, String email, String password) {
		// TODO Auto-generated constructor stub
        
        this.id = id;
        this.fullname = fullname;
        this.lastname = lastname;
        this.mobilenumber = mobilenumber;
        this.email = email;
        this.password = password;
	}
	
	public String getID(){
		return this.id;
	}
	
	public String getCloudID(){
		return this.cloundid;
	}
	
	public String getFullName()
	{
		return this.fullname;
	}
	
	public String getLastName(){
		return this.lastname;
	}
	
	public String getMobileNumber(){
		return this.mobilenumber;
	}

	public String getEmail(){
		return this.email;
	}
	
	public String getPassword(){
		return this.password;
	}
}
