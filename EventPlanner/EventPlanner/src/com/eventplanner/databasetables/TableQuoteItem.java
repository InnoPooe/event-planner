package com.eventplanner.databasetables;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tablequoteitem")
public class TableQuoteItem implements Serializable{
	private static final long serialVersionUID = 1L;

	@DatabaseField(id = true)
    private String id;
	
	@DatabaseField(foreign = true, canBeNull = false, foreignAutoRefresh = true, columnName = "fk_package_id")
	private TablePackages _TablePackage;
	
	@DatabaseField(foreign = true, canBeNull = false, foreignAutoRefresh = true, columnName = "fk_event_id")
	private TableEvents _TableEvent;
	
	@DatabaseField(canBeNull = false)
	private int quantity;
	
	@DatabaseField(canBeNull = false)
	private float totalprice;
	
	@DatabaseField(canBeNull = false)
	private Date date_created;
	
	@DatabaseField(canBeNull = true)
	private Date date_updated;
	
	@DatabaseField(canBeNull = false, defaultValue = "1")
	private int pendingsync;
	
	public TableQuoteItem() {
		
	}
	
	public TableQuoteItem(TablePackages _TablePackage, TableEvents _TableEvent, int quantity, float totalprice) {
		Calendar _Cal = Calendar.getInstance();
		
		UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        
        this.id = randomUUIDString;
        this._TablePackage = _TablePackage;
        this._TableEvent = _TableEvent;
        this.date_created = _Cal.getTime();
        this.quantity = quantity;
        this.totalprice = totalprice;
        this.pendingsync = 1;
	}

	public TableQuoteItem(String id, TablePackages _TablePackage, TableEvents _TableEvent, 
			int quantity, float totalprice, int pendingsync) {
		Calendar _Cal = Calendar.getInstance();
        
        this.id = id;
        this._TablePackage = _TablePackage;
        this._TableEvent = _TableEvent;
        this.quantity = quantity;
        this.totalprice = totalprice;
        this.date_updated = _Cal.getTime();
        this.pendingsync = pendingsync;
	}
	
	
	public TableQuoteItem(String id, TablePackages _TablePackage, TableEvents _TableEvent, int quantity, 
			float totalprice, Date date_created, Date date_updated, int pendingsync) {
        
        this.id = id;
        this._TablePackage = _TablePackage;
        this._TableEvent = _TableEvent;
        this.quantity = quantity;
        this.totalprice = totalprice;
        this.date_updated = date_updated;
        this.date_created = date_created;
        this.pendingsync = pendingsync;
	}
	
	public String getID(){
		return this.id;
	}
	
	public TablePackages getPackage(){
		return this._TablePackage;
	}
	
	public TableEvents getEvent(){
		return this._TableEvent;
	}
	
	public int getQuantity(){
		return this.quantity;
	}
	
	public float getTotalPrice(){
		return this.totalprice;
	}
	
	public Date getDateCreated(){
		return this.date_created;
	}
	
	public Date getDateUpdated(){
		return this.date_updated;
	}
	
	public int getPendingSync(){
		return this.pendingsync;
	}
}
