package com.eventplanner.databasetables;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

import com.j256.ormlite.field.DatabaseField;

public class TableSettings implements Serializable{

	private static final long serialVersionUID = 1L;

	@DatabaseField(id = true)
    private String id;
	
	@DatabaseField(canBeNull = false, defaultValue = "0")
	private int clouddatadownloaded;
	
	@DatabaseField(canBeNull = false, defaultValue = "1")
	private int enableautosync;
	
	@DatabaseField(canBeNull = false, defaultValue = "1")
	private int enablereminders;
	
	@DatabaseField(canBeNull = false, defaultValue = "1")
	private int vibrateonreminder;
	
	@DatabaseField(canBeNull = false, defaultValue = "1")
	private int playtoneonreminders;
	
	@DatabaseField(canBeNull = true)
	private Date lastsyncdatedownload;
	
	@DatabaseField(canBeNull = true)
	private Date lastsyncdateupload;
	
	@DatabaseField(canBeNull = true)
	private String subject;
	
	@DatabaseField(canBeNull = true)
	private String message;
	
	@DatabaseField(canBeNull = true)
	private Date lastreminderdate;
	
	public TableSettings() {
		// TODO Auto-generated constructor stub
	}
	
	
	public TableSettings(int clouddatadownloaded, int enableautosync, 
			int enablereminders, int vibrateonreminder, int playtoneonreminders,
			String subject, String message) {
		// TODO Auto-generated constructor stub
		
		UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        
        this.id = randomUUIDString;
        this.clouddatadownloaded = clouddatadownloaded;
        this.enableautosync = enableautosync;
        this.enablereminders = enablereminders;
        this.vibrateonreminder = vibrateonreminder;
        this.playtoneonreminders = playtoneonreminders;
        this.message = message;
        this.subject = subject;
	}
	
	public void setSubject(String subject){
		this.subject = subject;
	}
	
	public void setMessage(String message){
		this.message = message;
	}
	
	public void setCloutDataDownloaded(int clouddatadownloaded){
		this.clouddatadownloaded = clouddatadownloaded;
	}
	
	public void setEnableAutoSync(int enableautosync){
		this.enableautosync = enableautosync;	
    }
	
	public void setEnableReminders(int enablereminders){
		this.enablereminders = enablereminders;
	}
	
	public void setVibrateOnReminder(int vibrateonreminder){
		this.vibrateonreminder = vibrateonreminder;
	}
	
	public void setPlayToneOnReminder(int playtoneonreminders){
		this.playtoneonreminders = playtoneonreminders;
	}
	
	public void setLastSyncDateDownload(Date lastsyncdate){
		this.lastsyncdatedownload = lastsyncdate;
	}
	
	public Date getLastSyncDateDownload(){
		return this.lastsyncdatedownload;
	}
	
	public void setLastSyncDateUpload(Date lastsyncdate){
		this.lastsyncdateupload = lastsyncdate;
	}
	
	public void setLastReminderDate(Date lastreminderdate){
		this.lastreminderdate = lastreminderdate;
	}
	
	public Date getLastSyncDateUpload(){
		return this.lastsyncdateupload;
	}
	
	public int getCloutDataDownloaded(){
		return this.clouddatadownloaded;
	}
	
	public Date getLastReminderDate(){
		return this.lastreminderdate;
	}
	
	public int getEnableAutoSync(){
		return this.enableautosync;	
    }
	
	public int getEnableReminders(){
		return this.enablereminders;
	}
	
	public int getVibrateOnReminder(){
		return this.vibrateonreminder;
	}
	
	public int getPlayToneOnReminder(){
		return this.playtoneonreminders;
	}
	
	public String getSubject(){
		return this.subject;
	}
	
	public String getMessage(){
		return this.message;
	}
}
