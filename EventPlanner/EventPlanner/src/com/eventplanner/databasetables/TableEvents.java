package com.eventplanner.databasetables;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tableevents")
public class TableEvents implements Serializable{
	private static final long serialVersionUID = 1L;

	@DatabaseField(id = true)
    private String id;
	
	@DatabaseField(canBeNull = false)
	private String description;
	
	@DatabaseField(foreign = true, canBeNull = false, foreignAutoRefresh = true, columnName = "fk_eventtype_id")
	private TableEventTypes _TableEventType;
	
	@DatabaseField(foreign = true, canBeNull = false, foreignAutoRefresh = true, columnName = "fk_client_id")
	private TableClients _TableClient;
	
	@DatabaseField(canBeNull = false)
	private float primary_cost;
	
	@DatabaseField(canBeNull = false)
	private Date event_date;
	
	@DatabaseField(canBeNull = false)
	private String event_venue;
	
	@DatabaseField(canBeNull = false)
	private int iscancelled;
	
	@DatabaseField(canBeNull = false)
	private int ispaymentsettled; 
	
	@DatabaseField(canBeNull = true)
	private String comments;
	
	@DatabaseField(canBeNull = false, defaultValue = "1")
	private int pendingsync;
	
	public TableEvents() {
		// TODO Auto-generated constructor stub
	}
	
	public TableEvents(String description, TableEventTypes _TableEventType, TableClients _TableClient,
			float primary_cost, String eventdate, String event_venue, int ispaymentsettled, String comments) {
		// TODO Auto-generated constructor stub
		SimpleDateFormat formater = new SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.US);
		
		try { 
			Date event_date = formater.parse(eventdate);
			this.event_date = event_date;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        
        this.id = randomUUIDString;
        this.description = description;
        this.iscancelled = 0;
        this.comments = comments;
        this._TableClient = _TableClient;
        this._TableEventType = _TableEventType;
        this.event_venue = event_venue;
        this.ispaymentsettled = ispaymentsettled;
        this.primary_cost = primary_cost;
        this.pendingsync = 1;
	}

	public TableEvents(String id, String description, TableEventTypes _TableEventType, TableClients _TableClient, String eventdate,
			float primary_cost, String event_venue, int ispaymentsettled, String comments, int pendingsync) {
		// TODO Auto-generated constructor stub
		//Calendar _Cal = Calendar.getInstance();
		SimpleDateFormat formater = new SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.US);
		
		try {
			Date event_date = formater.parse(eventdate);
			this.event_date = event_date;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
         
        this.id = id;
        this.description = description;
        this.iscancelled = 0;
        this.comments = comments;
        this._TableEventType = _TableEventType;
        this.event_venue = event_venue;
        this.ispaymentsettled = ispaymentsettled;
        this.primary_cost = primary_cost;
        this.pendingsync = pendingsync;
        this._TableClient = _TableClient;
	}
	
	public TableEvents(String id, String description, TableEventTypes _TableEventType, TableClients _TableClient,
			String eventdate, float primary_cost, String event_venue, int ispaymentsettled, 
			int iscancelled, String comments, int pendingsync) {
		// TODO Auto-generated constructor stub
		//Calendar _Cal = Calendar.getInstance();
		SimpleDateFormat formater = new SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.US);
		
		try {
			Date event_date = formater.parse(eventdate);
			this.event_date = event_date;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        this.id = id;
        this.description = description;
        this.iscancelled = iscancelled;
        this.comments = comments;
        this._TableEventType = _TableEventType;
        this._TableClient = _TableClient;
        this.event_venue = event_venue;
        this.ispaymentsettled = ispaymentsettled;
        this.primary_cost = primary_cost;
        this.pendingsync = pendingsync;
	}
	
	public String getID(){
		return this.id;
	}
	
	public String getDescription(){
		return this.description;
	}
	
	public String getComments(){
		return this.comments;
	}
	
	public int getIsCancelled(){
		return this.iscancelled;
	}
	
	public int getIsPaymentSettled(){
		return this.ispaymentsettled;
	}
	
	public Date getEventDate(){
		return this.event_date;
	}
	
	public float getPrimaryCost(){
		return this.primary_cost;
	}
	
	public String getEventVenue(){
		return this.event_venue;
	}
	
	public TableClients getClient(){
		return this._TableClient;
	}
	
	public TableEventTypes getEventType(){
		return this._TableEventType;
	}
	
	public int getPendingSync(){
		return this.pendingsync;
	}
}
