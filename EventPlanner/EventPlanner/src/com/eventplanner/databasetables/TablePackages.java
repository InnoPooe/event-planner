package com.eventplanner.databasetables;

import java.io.Serializable;
import java.util.UUID;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "tablepackages")
public class TablePackages implements Serializable{
	private static final long serialVersionUID = 1L;

	@DatabaseField(id = true)
    private String id;
	
	@DatabaseField(canBeNull = false)
	private String description;
	
	@DatabaseField(canBeNull = false)
	private double price;
	
	@DatabaseField(canBeNull = false)
	private int isactive;
	
	@DatabaseField(canBeNull = false, defaultValue = "1")
	private int pendingsync;
	
	public TablePackages() {
		// TODO Auto-generated constructor stub
	}

	public TablePackages(String description, double price) {
		UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        
        this.id = randomUUIDString;
        this.description = description;
        this.price = price;
        this.isactive = 1;
        this.pendingsync = 1;
	}
	
	public TablePackages(String id, String description, double price, int isactive, int pendingsync) {
		// TODO Auto-generated constructor stub
		this.id = id;
        this.description = description;
        this.price = price;
        this.isactive = isactive;
        this.pendingsync = pendingsync;
	}
	
	public String getID(){
		return this.id;
	}
	
	public void setDescription(String description){
		this.description = description;
	}
	
	public void setPrice(double price){
		this.price = price;
	}
	
	public void setIsActive(int isactive){
		this.isactive = isactive;
	}
	
	public void setPendingSync(){
		this.pendingsync = 1;
	}
	
	public String getDescription(){
		return this.description;
	}
	
	public double getPrice(){
		return this.price;
	}
	
	public int getIsActive(){
		return this.isactive;
	}
	
	public int getPendingSync(){
		return this.pendingsync;
	}
}
