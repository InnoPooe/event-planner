package com.eventplanner.databasetables;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


@DatabaseTable(tableName = "tableclients")
public class TableClients implements Serializable{
	private static final long serialVersionUID = 1L;

	@DatabaseField(id = true)
    private String id;
	
	@DatabaseField(canBeNull = false)
	private String fullname;
	
	@DatabaseField(canBeNull = false)
	private String lastname;
	
	@DatabaseField(canBeNull = false)
	private String mobilenumber;
	
	@DatabaseField(canBeNull = true)
	private String telnumber;
	
	@DatabaseField(canBeNull = false)
	private String email;
	
	@DatabaseField(canBeNull = false)
	private String address;
	
	@DatabaseField(canBeNull = false)
	private Date date_added;
	
	@DatabaseField(canBeNull = false)
	private int isactive;
	
	@DatabaseField(canBeNull = false, defaultValue = "1")
	private int pendingsync;
	
	public TableClients() {
		// TODO Auto-generated constructor stub
	}

	public TableClients(String fullname, String lastname, String mobilenumber, String telnumber, String email, String address) {
		// TODO Auto-generated constructor stub
		
		Calendar _Cal = Calendar.getInstance();
		
		UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        
        this.id = randomUUIDString;
        this.fullname = fullname;
        this.lastname = lastname;
        this.mobilenumber = mobilenumber;
        this.telnumber = telnumber;
        this.email = email;
        this.address = address;
        this.date_added = _Cal.getTime();
        this.isactive = 1;
        this.pendingsync = 1;
	}
	
	public TableClients(String id, String fullname, String lastname, String mobilenumber, 
			String telnumber, String email, String address, Date date_added, int isactive, int pendingsync) {
		// TODO Auto-generated constructor stub
        
        this.id = id;
        this.fullname = fullname;
        this.lastname = lastname;
        this.mobilenumber = mobilenumber;
        this.telnumber = telnumber;
        this.email = email;
        this.address = address;
        this.date_added = date_added;
        this.isactive = isactive;
        this.pendingsync = pendingsync;
	}
	
	public void setIsActive(int isactive){
		this.isactive = isactive;
	}
	
	public String getID(){
		return this.id;
	}
	
	public String getFullName()
	{
		return this.fullname;
	}
	
	public String getLastName(){
		return this.lastname;
	}
	
	public String getMobileNumber(){
		return this.mobilenumber;
	}
	
	public String getTelNumber(){
		return this.telnumber;
	}

	public String getEmail(){
		return this.email;
	}
	
	public String getAddress(){
		return this.address;
	}
	
	public int getIsActive(){
		return this.isactive;
	}
	
	public Date getDateAdded(){
		return this.date_added;
	}
	
	public int getPendingSync(){
		return this.pendingsync;
	}
}
