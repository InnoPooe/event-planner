package com.eventplanner.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import com.eventplanner.DashboardActivity;
import com.eventplanner.R;
import com.eventplanner.database.EventPlannerRepository;
import com.eventplanner.database.EventPlannerSQLiteDB;
import com.eventplanner.databasetables.TableEvents;
import com.eventplanner.databasetables.TableSettings;
import com.eventplanner.databasetables.TableUsers;
import com.eventplanner.dataclass.GlobalVariables;
import com.eventplanner.dataclass.ParseCloudQueryObject;
import java.util.List;

import android.app.Notification; 
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Vibrator;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class SyncCloudAndLocalDataService extends Service
		implements TextToSpeech.OnInitListener{

	Intent _ParentIntent;
	
	private EventPlannerSQLiteDB databaseHelper;
	private EventPlannerRepository dataRepository;
	private ParseCloudQueryObject ParseSyncCloud;
	private TableSettings _Settings;
	
    SharedPreferences _SharedPreferences;
    
    private NotificationManager mNotificationManager;
	public static int NOTIFICATION_ID = 1;
	
    Context context;
    MediaPlayer myPlayer;
    AudioManager mAudioManager;
    int userVolume;
    private TableUsers _TableUser;
    
    private List<TableEvents> _EventsDue;
    
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate() {
	   super.onCreate();
	   try
	   {
	       databaseHelper = new EventPlannerSQLiteDB(getApplicationContext());
	       dataRepository = new EventPlannerRepository(databaseHelper);
	       ParseSyncCloud = new ParseCloudQueryObject(dataRepository);
	       
	       _TableUser = dataRepository.GetUser();
    	   _Settings = dataRepository.GetSettings();
    	   
	       if(_TableUser != null){
	    	   GlobalVariables._LoggedUser = _TableUser;
	       }
	       
	       mAudioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);

	        //remeber what the user's volume was set to before we change it.
	       userVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_ALARM);
	 
	       myPlayer = new MediaPlayer();
	   }
	   catch(Exception Ex)
	   {
		   Ex.printStackTrace();
	   }
	}
	
	public void UploadLocalDataToClound(){
		try
		{
		   boolean isSyncEnabled = false;
		   boolean isReminderEnabled = true;
		   
	 	   if(_Settings != null){
	 		   isSyncEnabled = _Settings.getEnableAutoSync() == 1?true:false;
	 		   isReminderEnabled = _Settings.getEnableReminders() == 1?true:false;
	 	   }
	 	   
	 	   if(isSyncEnabled){
	 		   boolean continueSync = false;
	 		   boolean continueSyncDownload = false;
	 		   
	 		  if(_Settings.getLastSyncDateDownload() == null){
	 			 continueSyncDownload = true;
	 		   }else{
	 			   long diff =  new Date().getTime() - _Settings.getLastSyncDateDownload().getTime();
	 			   long seconds = diff / 1000;
	 			   long minutes = seconds / 60;
	 			   long hours = minutes / 60;
	 			   
	 			  continueSyncDownload = hours >= 12? true:false;
	 		   }
	 		  
	 		   if(_Settings.getLastSyncDateUpload() == null){
	 			  continueSync = true;
	 		   }else{
	 			   long diff = new Date().getTime() - _Settings.getLastSyncDateUpload().getTime();
	 			   long seconds = diff / 1000;
	 			   long minutes = seconds / 60;
	 			   long hours = minutes / 60;
	 			   
	 			   continueSync = hours >= 12? true:false;
	 		   }
	 		   
	 		   if(continueSync){
	 			   GlobalVariables global = new GlobalVariables();
	 			   
	 			  global.UploadLocalDataToClound(_Settings, dataRepository);
	 			   
	 			   _Settings.setLastSyncDateUpload(new Date());
	 			   dataRepository.SaveOrUpdateSettings(_Settings);
	 		   }
	 		   
	 		   if(continueSyncDownload){
	 			  ParseSyncCloud.SyncCloundDataToDevice(GlobalVariables._LoggedUser.getCloudID());
	 			  
	 			  _Settings.setLastSyncDateDownload(new Date());
	 			  dataRepository.SaveOrUpdateSettings(_Settings);
			   }
	 	   }
	 	   
	 	   if(isReminderEnabled){
	 		   boolean vibrate = _Settings.getVibrateOnReminder() == 1?true:false;
	 		   boolean ringtone = _Settings.getPlayToneOnReminder() == 1?true:false;
	 		   boolean remind = false;
	 		   
	 		  if(_Settings.getLastReminderDate() == null){
	 			 remind = true;
	 		   }else{
	 			   long diff = new Date().getTime() -_Settings.getLastReminderDate().getTime();
	 			   long seconds = diff / 1000;
	 			   long minutes = seconds / 60;
	 			   long hours = minutes / 60;
	 			   
	 			  remind = hours >= 12? true:false;
	 		   }
	 		  
	 		  if(remind){
	 			 List<TableEvents> _ActiveEvents = dataRepository.GetAllActiveEvents();
	 			  if(_ActiveEvents != null && _ActiveEvents.size() > 0){
	 				  String _Message = "";
	 				  String msg = "The following event(s): ";
	 				  
	 				  int eventdue = 0;
	 				 _EventsDue = new ArrayList<TableEvents>();
	 				 
	 				  for(TableEvents event: _ActiveEvents){
	 					  
	 					  long diff = event.getEventDate().getTime() - new Date().getTime();
	 					  long seconds = diff / 1000;
	 		 			  long minutes = seconds / 60;
	 		 			  long hours = minutes / 60;
	 		 			  
	 		 			  if(hours > 12 && hours <=24 ){
	 		 				  
	 		 				_EventsDue.add(event);
	 		 				  
		 					  if(_Message.length() > 0)
		 					  {
		 						 _Message += ", " + event.getDescription();
		 					  }
		 					  else{
		 						 _Message += event.getDescription();
		 					  }
		 					  
		 					  eventdue++;
	 		 			  }
	 				  }
	 				  
	 				  if(eventdue > 0){
	 					  try{
			 				  String aMessage = msg + _Message + " are due tomorrow!";
			 				  SendNotification(aMessage);
			 				  
			 				  if(vibrate)
			 					 ((Vibrator)getSystemService(VIBRATOR_SERVICE)).vibrate(3000);
			 				  
			 				  if(ringtone){
			 					 Uri ringtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
			 					 boolean isPlaying = false;
			 					 
			 			 		mAudioManager.setStreamVolume(AudioManager.STREAM_ALARM, mAudioManager.getStreamMaxVolume(AudioManager.STREAM_ALARM), AudioManager.FLAG_PLAY_SOUND);
			 					
			 					 if(!myPlayer.isPlaying()){
			 						myPlayer.setDataSource(getApplicationContext(), ringtoneUri);
			 						myPlayer.setAudioStreamType(AudioManager.STREAM_ALARM);
			 						myPlayer.setLooping(false);
			 						myPlayer.prepare();
			 						myPlayer.start();
			 						
			 						isPlaying = myPlayer.isPlaying();
			 					 }
			 					 
			 					 while(isPlaying){
			 						isPlaying = myPlayer.isPlaying();
			 					 }
			 					 
			 					 mAudioManager.setStreamVolume(AudioManager.STREAM_ALARM, userVolume, AudioManager.FLAG_PLAY_SOUND);
			 				  }
			 				  
			 				  _Settings.setLastReminderDate(new Date());
			 				  dataRepository.SaveOrUpdateSettings(_Settings);
	 					  }
	 					  catch(Exception Ex){
	 						  Ex.printStackTrace();
	 					  }
	 				  }
	 				  
	 			  }
	 		  }
	 		   
	 	   }
	 	   
		}
		catch(Exception Ex){
			Ex.printStackTrace();
		}
	}
	
	@Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try
 	    { 
 		   Log.e("SyncCloudAndLocalDataService", "SyncCloudAndLocalDataService Started");
 		   
 		   final Handler handler = new Handler(); 
 	       Runnable runable = new Runnable() { 
 	
 	            @Override 
 	            public void run() { 
 	                try{
 	                	UploadLocalDataToClound();
 	                    handler.postDelayed(this, 1800000);
 	                }
 	                catch (Exception e) {
 	                    // TODO: handle exception
 	                }
 	                finally{
 	                    //also call the same runnable 
 	                    handler.postDelayed(this, 1800000); 
 	                }
 	            } 
 	        }; 
 	        handler.postDelayed(runable, 1000); 
 	        
 	   }
 	   catch(Exception Ex)
 	   {
 		   Ex.printStackTrace();
 	   }
        return START_STICKY;
    }
	
	@Override
	public void onStart(Intent intent, int startId) {
	   
	}
	
	@Override
	public void onDestroy() {
	   try
	   { 
		   try 
		   {
				
		   } 
		   catch (Exception Ex) {
				// TODO: handle exception
			   Ex.printStackTrace();
		   }
	   }
	   catch(Exception Ex)
	   {
		   Ex.printStackTrace();
	   }
	}
	
	@SuppressWarnings("unused")
	private void SendBroadCastToRefresh()
	{
		_ParentIntent = new Intent(GlobalVariables.BROADCAST_ACTION);
		_ParentIntent.putExtra("Refresh", "Refresh");
		sendBroadcast(_ParentIntent);
	}

	private void SendNotification(String _Message) {
	
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        Random diceRoller = new Random();
	    NOTIFICATION_ID = diceRoller.nextInt(10000) + 1;
	      
	    Intent _Intent = new Intent(this, DashboardActivity.class);
	    
	    Bundle b = new Bundle();
	    b.putSerializable("_EventsDue", (Serializable) _EventsDue);
	    b.putInt("NOTIFICATION_ID", NOTIFICATION_ID);
	    
	    //_Intent.put("_EventsDue", _EventsDue);
        _Intent.putExtra("NOTIFICATION", b);
        
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, _Intent, Notification.FLAG_AUTO_CANCEL);
        
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
        .setSmallIcon(R.drawable.ic_launcher)
        .setContentTitle("Event Planner")
        .setStyle(new NotificationCompat.BigTextStyle()
        .bigText(_Message))
	    .setDefaults(Notification.DEFAULT_ALL)
	    .setAutoCancel(true)
        .setContentText(_Message);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
	
	@Override
	public void onInit(int status) {
		
	}
}
