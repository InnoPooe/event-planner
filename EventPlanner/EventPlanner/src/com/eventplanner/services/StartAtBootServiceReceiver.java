package com.eventplanner.services;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class StartAtBootServiceReceiver  extends BroadcastReceiver 
{
	
	@Override
	public void onReceive(Context context, Intent intent) 
	{
		try
		{
			if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
				Intent _Intent = new Intent(context, SyncCloudAndLocalDataService.class);
				context.startService(_Intent);
			}
		}
		catch(Exception Ex)
		{
			Ex.printStackTrace();
		}
	}
}
