package com.eventplanner;



import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.dd.processbutton.iml.ActionProcessButton;
import com.eventplanner.database.EventPlannerRepository;
import com.eventplanner.database.EventPlannerSQLiteDB;
import com.eventplanner.databasetables.TableUsers;
import com.eventplanner.dataclass.GlobalVariables;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Android login screen Activity
 */
public class SignupActivity extends SherlockActivity implements ValidationListener,
ProgressGenerator.OnCompleteListener{
	
    private EditText editEmail;
    private EditText editPassword;
    private EditText editFullname;
    private EditText editLastname;
    private EditText editMobileNo;
    private EditText editPasswordConfirm;
    
	
	private Validator validator;
    
	private ProgressGenerator progressGenerator;
    private ActionProcessButton btnSignUp;
    
    private EventPlannerSQLiteDB databaseHelper;
	private EventPlannerRepository dataRepository;
	
	GlobalVariables global = new GlobalVariables();
	Rule<EditText> _ValidatePasswordMatch;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        
        getSupportActionBar().show();
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.background_img4));
      
     	getSupportActionBar().setTitle("Event Planner");
        getSupportActionBar().setSubtitle("Managing Events");
       
         getSupportActionBar().setLogo(R.drawable.ic_launcher);
         getSupportActionBar().setDisplayHomeAsUpEnabled(true);
         getSupportActionBar().setHomeButtonEnabled(true);
         getSupportActionBar().setDisplayShowHomeEnabled(false);
        
        databaseHelper = new EventPlannerSQLiteDB(this);
        dataRepository = new EventPlannerRepository(databaseHelper);
        
        editEmail = (EditText)findViewById(R.id.email);
        editPassword = (EditText)findViewById(R.id.password);
        editFullname = (EditText)findViewById(R.id.fullname);
        editPasswordConfirm = (EditText)findViewById(R.id.passwordconfirm);
        editLastname = (EditText)findViewById(R.id.lastname);
        editMobileNo = (EditText)findViewById(R.id.mobileno);
        
        
        progressGenerator = new ProgressGenerator(this);
        btnSignUp = (ActionProcessButton) findViewById(R.id.btnSignIn);
        
        validator = new Validator(this);
        validator.setValidationListener(this);
        
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               validator.validate();
            }
        });
        
        _ValidatePasswordMatch = new Rule<EditText>("Password Mismatch. Re-enter Confirm Password!") {
			
			@Override
			public boolean isValid(EditText domainNameEditText) {
				String domainName = domainNameEditText.getText().toString();
				String compareName = editPassword.getText().toString();
				return global.ValidatePasswordMatch(compareName, domainName);
			}
		};
		
		validator.put(editFullname, global.RequiredRule);
		validator.put(editLastname, global.RequiredRule);
		validator.put(editEmail, global.RequiredRule);
		validator.put(editEmail, global._ValidateEmailAddress);
		validator.put(editMobileNo, global._ValidatePhoneNumber);
		validator.put(editPassword, global.RequiredRule);
		validator.put(editPasswordConfirm, global.RequiredRule);
		validator.put(editPasswordConfirm, _ValidatePasswordMatch);
    }
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
    	if(item.getItemId() == android.R.id.home)
    	{ 
    		setResult(2);
			finish();
    	}
    	
    	return false;
	}
	
	@Override 
    public void onBackPressed() {
		setResult(2);
		finish();
    };
    
	@Override
	public void onValidationSucceeded() {
		// TODO Auto-generated method stub
		GlobalVariables.ButtonAnimationControl = 1000;
		progressGenerator.start(btnSignUp);
        btnSignUp.setEnabled(false);
        editEmail.setEnabled(false);
        editPassword.setEnabled(false);
         
        ParseUser test = ParseUser.getCurrentUser();
        if(test != null){
        	ParseUser.logOut();
        }
        
        ParseUser user = new ParseUser();
		user.setUsername(editEmail.getText().toString());
		user.setPassword(editPassword.getText().toString());
		user.setEmail(editEmail.getText().toString());
		user.put("fullName", editFullname.getText().toString());
		user.put("lastName", editLastname.getText().toString());
		user.put("mobileNumber", editMobileNo.getText().toString());
		
		user.signUpInBackground(new SignUpCallback() {
			public void done(ParseException e) {
				if (e == null) {
					// Show a simple Toast message upon successful registration
					Toast.makeText(getApplicationContext(),
							"Successfully Signed up, please log in.",
							Toast.LENGTH_LONG).show();
					
					final ParseUser UserLogged = ParseUser.getCurrentUser();
					
					if(UserLogged != null){
						String cloundid = UserLogged.getObjectId();
						String fullname = editFullname.getText().toString(); 
						String lastname = editLastname.getText().toString(); 
						String mobilenumber = editMobileNo.getText().toString();
						String email = editEmail.getText().toString();
						String password = editPassword.getText().toString();
						
						TableUsers _TableUsers = new TableUsers(cloundid, fullname, lastname, mobilenumber, email, password);

						boolean saved = dataRepository.SaveOrUpdateUser(_TableUsers);
						if(saved){
							//Toast.makeText(getApplicationContext(), "User Locally Saved", Toast.LENGTH_SHORT).show();
						}
						
						setResult(0);
						finish();
						
						//startActivityForResult(new Intent(SignupActivity.this, DashboardActivity.class), RequestCode);
					}
				} else {
					btnSignUp.setEnabled(true);
			        editEmail.setEnabled(true);
			        editPassword.setEnabled(true);
					
					Toast.makeText(getApplicationContext(),
							"Sign up Error", Toast.LENGTH_LONG)
							.show();
				}
			}
		});
	}

	@Override
	public void onValidationFailed(View failedView, Rule<?> failedRule) {
		// TODO Auto-generated method stub
		String message = failedRule.getFailureMessage();

        if (failedView instanceof EditText) {
            failedView.requestFocus();
            ((EditText) failedView).setError(message);
        } else {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
	}

	@Override
	public void onComplete() {
		// TODO Auto-generated method stub
	}
}
