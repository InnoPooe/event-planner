package com.eventplanner;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;
import cn.pedant.SweetAlert.SweetAlertDialog;
import cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;
import com.andexert.expandablelayout.library.ExpandableLayout;
import com.eventplanner.adapters.SelectPackageAdapter;
import com.eventplanner.database.EventPlannerRepository;
import com.eventplanner.database.EventPlannerSQLiteDB;
import com.eventplanner.databasetables.TableClients;
import com.eventplanner.databasetables.TableEventTypes;
import com.eventplanner.databasetables.TableEvents;
import com.eventplanner.databasetables.TablePackages;
import com.eventplanner.databasetables.TableQuoteItem;
import com.eventplanner.databasetables.TableSettings;
import com.eventplanner.dataclass.EventInfo;
import com.eventplanner.dataclass.GlobalVariables;
import com.eventplanner.dataclass.PackageItem;
import com.eventplanner.dataclass.QuotePDFGenerator;
import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.fourmob.datetimepicker.date.DatePickerDialog.OnDateSetListener;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.sleepbot.datetimepicker.time.RadialPickerLayout;
import com.sleepbot.datetimepicker.time.TimePickerDialog;
import com.tekle.oss.android.animation.AnimationFactory;
import com.tekle.oss.android.animation.AnimationFactory.FlipDirection;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import de.timroes.swipetodismiss.SwipeDismissList;

@SuppressLint("InflateParams")
public class EventFormActivity extends SherlockFragmentActivity implements ValidationListener,
ProgressGenerator.OnCompleteListener, OnDateSetListener, TimePickerDialog.OnTimeSetListener {

	private EditText editEventName;
    private EditText editEventVenue;
    private EditText editEventDate, editEventTime;
    private Spinner cmbEventType;
    private EditText editPrimaryCost;
    private EditText editComments;
    private View _SelectedDateTimeView;
    
    private CheckBox chkPaymentSettled;
	private Spinner cmbClientNames;
	
	private Validator validator;

	int NumOfSteps = 2, _CurrentPosition = 1;
	private Button _ButtonNext, _ButtonPrev;
	
	private View _ChildView;
	private LinearLayout _ParentScrollView;
	private LayoutInflater _ViewInflater;
	
	TextView _StepText;
	TextView _StepHeadingText;
	
	EventPlannerSQLiteDB databaseHelper;
	EventPlannerRepository dataRepository;
	private LayoutInflater _LayoutInflater;
	
	private List<TableClients> _TableClients = null;
	private List<TableEventTypes> _TableEventTypes = null;
	private List<TablePackages> _TablePackages = null;
	private List<PackageItem> _PackageItems = null;
	private ArrayList<TableQuoteItem> _TableQuoteItems = null;
	private EventInfo _EventInfo = null;
	
	GlobalVariables global = new GlobalVariables();
	
	private ArrayAdapter<String> _EventTypeAdater, _ClientNamesAdapter, _PackagesAdater;
	private SelectPackageAdapter _SelectPackageAdapter, _SelectPackageAdapterHeading;
	
	private Context context;
	
	private TableClients _SelectedClient = null;
	private TableEventTypes _SelectedEventType = null;
	private TablePackages _SelectedPackage = null;
	
	private TableSettings _TableSetting;
	
	private int ClientPosition, EventTypePosition, PackagePosition;
	private String _EventIdToUpdate;
	
	private ListView _ListPackages, _ListPackageHeading;
	private ExpandableLayout _ExpandableLayout;
	private Spinner cmbPackages;
	private EditText editQuantity;
	private Button btnAddPackage;
	
	public static final String DATEPICKER_TAG = "datepicker";
    public static final String TIMEPICKER_TAG = "timepicker";
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;
    
    private SimpleDateFormat formater = new SimpleDateFormat("dd MMM yy mmss", Locale.US);
    
    private boolean IsEditMode = false;
    
	private static final Style INFINITE = new Style.Builder().
		    setBackgroundColorValue(Style.holoBlueLight).build();
	
	private static final Style INFINITEERROR = new Style.Builder().
		    setBackgroundColorValue(Style.holoRedLight).build();
	
	private static final de.keyboardsurfer.android.widget.crouton.Configuration CONFIGURATION_INFINITE = new de.keyboardsurfer.android.widget.crouton.Configuration.Builder()
	          .setDuration(de.keyboardsurfer.android.widget.crouton.Configuration.DURATION_INFINITE)
	          .build();
	
	private void showBuiltInCrouton(final Style croutonStyle, String croutonText) {
	    showCrouton(croutonText, croutonStyle, de.keyboardsurfer.android.widget.crouton.Configuration.DEFAULT);
	 }
	
	private void showCrouton(String croutonText, Style croutonStyle, de.keyboardsurfer.android.widget.crouton.Configuration configuration) {
	    final boolean infinite = INFINITE == croutonStyle || INFINITEERROR == croutonStyle;
	    
	       
	    final Crouton crouton;
	      crouton = Crouton.makeText(this, croutonText, croutonStyle);
	    
	    if (infinite) {
	    }
	    crouton.setConfiguration((de.keyboardsurfer.android.widget.crouton.Configuration) (infinite ? CONFIGURATION_INFINITE : configuration)).show();
	}
	
    @SuppressWarnings("unchecked")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.captureevent_layout);
        
        getSupportActionBar().show();
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.background_img4));
        
        boolean isEditMode = false;
        
     	 getSupportActionBar().setTitle("Event Planner");
         getSupportActionBar().setSubtitle("Managing Events");
        
         getSupportActionBar().setLogo(R.drawable.ic_launcher);
         getSupportActionBar().setDisplayHomeAsUpEnabled(true);
         getSupportActionBar().setHomeButtonEnabled(true);
         getSupportActionBar().setDisplayShowHomeEnabled(false);
        
        setProgressBarIndeterminateVisibility(false);
        context = this;
        
        NumOfSteps = 2;
        _CurrentPosition = 1;
        
        databaseHelper = new EventPlannerSQLiteDB(this);
        dataRepository = new EventPlannerRepository(databaseHelper);
        
        final Calendar calendar = Calendar.getInstance();
        datePickerDialog = DatePickerDialog.newInstance(this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), true);
        timePickerDialog = TimePickerDialog.newInstance(this, calendar.get(Calendar.HOUR_OF_DAY) ,calendar.get(Calendar.MINUTE), false, false);

        
        new SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.US);
        
        _TableClients = dataRepository.GetAllClients();
        _TableEventTypes = dataRepository.GetAllEventTypes();
        _TablePackages = dataRepository.GetAllPackages();
        _TableSetting = dataRepository.GetSettings();
        
        _ButtonNext = (Button)findViewById(R.id.button_next);
        _ButtonPrev = (Button)findViewById(R.id.button_previous);
        _StepText = (TextView)findViewById(R.id.step_name);
        _StepHeadingText = (TextView)findViewById(R.id.step_title);
        _ViewInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        
        _ButtonNext.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View _View) {
				validator.validate();
			}
		});
        
        _ButtonPrev.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View _View) {
				// TODO Auto-generated method stub
				_CurrentPosition--;
				ControlButtons(_CurrentPosition);
			}
		});
        
        
        validator = new Validator(this);
        validator.setValidationListener(this);
        
        if(savedInstanceState != null){
        	_CurrentPosition = savedInstanceState.getInt("_CurrentPosition");
        	_PackageItems = (List<PackageItem>) savedInstanceState.getSerializable("_PackageItems");
        	_EventInfo = (EventInfo) savedInstanceState.getSerializable("_EventInfo");
        //	_PackagesSelected = (List<TablePackages>) savedInstanceState.getSerializable("_PackagesSelected");
        	
        	ClientPosition = savedInstanceState.getInt("ClientPosition");
        	EventTypePosition = savedInstanceState.getInt("EventTypePosition");
        	PackagePosition = savedInstanceState.getInt("PackagePosition");
        	IsEditMode = savedInstanceState.getBoolean("IsEditMode");
        	_EventIdToUpdate = savedInstanceState.getString("_EventIdToUpdate");
        }
        else{
        	if(getIntent().getExtras() != null){
        	Bundle bundle = getIntent().getExtras().getBundle("Data");
        	
        	if(bundle != null)
        		IsEditMode = bundle.getBoolean("IsEditMode");
        		isEditMode = IsEditMode;
        		TableEvents Event = (TableEvents) bundle.getSerializable("_TableEvent");
        		_EventIdToUpdate = Event.getID();
        		
        		boolean ispaymentsettled = Event.getIsPaymentSettled() == 1? true:false;
        		SimpleDateFormat f = new SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.US);
        		
        		_EventInfo = new EventInfo(Event.getID(), Event.getDescription(), Event.getEventType(), 
        				Event.getClient(), Event.getPrimaryCost(), 
        				f.format(Event.getEventDate()), Event.getEventVenue(), 
        				ispaymentsettled, Event.getComments());
        		
        		List<TableQuoteItem> _TableQuoteItems = dataRepository.GetQuoteItemByEventID(Event.getID());
            	_PackageItems = new ArrayList<PackageItem>();
            	
            	if(_TableQuoteItems != null){
            		for(TableQuoteItem qItem: _TableQuoteItems){
            			PackageItem item = new PackageItem(qItem.getPackage().getID(), String.valueOf(qItem.getQuantity()),
            					qItem.getPackage().getDescription(), String.valueOf(qItem.getPackage().getPrice()), 
            					String.valueOf(qItem.getTotalPrice()), false);
            			
            			_PackageItems.add(item);
            		}
            	}
        	}
        }

        ControlButtons(_CurrentPosition);
        if(isEditMode){
        	EventTypePosition = _EventTypeAdater.getPosition(GetSelectedEventTypeName(_EventInfo._TableEventType.getID()));
        	cmbEventType.setSelection(EventTypePosition);
        	
        	ClientPosition = _ClientNamesAdapter.getPosition(GetSelectedClientName(_EventInfo._TableClient.getID()));
        	cmbClientNames.setSelection(ClientPosition);
        }
    }
    
   // public void 
    
    public String GetSelectedEventTypeName(String Value){
		String returnv = null;
    	
    	try 
    	{
        	for(TableEventTypes t : _TableEventTypes)
    		{
        		if(Value.equalsIgnoreCase(t.getID()))
    			{
    				returnv = t.getDescription();
    				break;
    			}
    		}
		} catch (Exception e) {
			// TODO: handle exception
		}		
    	return returnv;
	}
    
    public String GetSelectedClientName(String Value){
		String returnv = null;
    	
    	try 
    	{
        	for(TableClients c : _TableClients)
    		{
        		if(Value.equalsIgnoreCase(c.getID()))
    			{
    				returnv = c.getLastName() + " " + c.getFullName();
    				break;
    			}
    		}
		} catch (Exception e) {
			// TODO: handle exception
		}		
    	return returnv;
	}
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
		
		try
		{
			if(_CurrentPosition >= NumOfSteps){
				menu.add("Save").setNumericShortcut('S')
				.setIcon(R.drawable.save_icon)
	    		.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
			}
		}
		catch(Exception Ex)
		{
			Ex.printStackTrace();
		}
		
		return true;
	}
    
    @Override
	public void onSaveInstanceState(Bundle state) {
	    super.onSaveInstanceState(state);
	
	    state.putInt("_CurrentPosition", _CurrentPosition);
	    state.putInt("ClientPosition", ClientPosition);
	    state.putInt("PackagePosition", PackagePosition);
	    state.putInt("EventTypePosition", EventTypePosition);
	    state.putSerializable("_PackageItems", (Serializable) _PackageItems);
	    state.putSerializable("_EventInfo", _EventInfo);
	    //state.putSerializable("_PackagesSelected", (Serializable) _PackagesSelected);
	    state.putBoolean("IsEditMode", IsEditMode);
	    state.putString("_EventIdToUpdate", _EventIdToUpdate);
	}
    
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
    	if(item.getItemId() == android.R.id.home)
    	{
    		setResult(2);
			finish();
    	}
    	else if(item.getNumericShortcut() == 'S'){
    		if(IsEditMode){
    			new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
	            .setTitleText("Are you sure?")
	            .setContentText("Event details will be update on your device.")
	            .setCancelText("No!")
	            .setConfirmText("Yes!")
	            .showCancelButton(true)
	            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
	                @Override
	                public void onClick(SweetAlertDialog sDialog) {
	                    // reuse previous dialog instance, keep widget user state, reset them if you need
	                    sDialog.setTitleText("Oops!")
	                            .setContentText("Operation cancelled.")
	                            .setConfirmText("OK")
	                            .showCancelButton(false)
	                            .setCancelClickListener(null)
	                            .setConfirmClickListener(null)
	                            .changeAlertType(SweetAlertDialog.ERROR_TYPE);
	                }
	            })
	            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
	                @Override
	                public void onClick(SweetAlertDialog sDialog) {
	                	
	                	int paymentsettled = _EventInfo.ispaymentsettled? 1: 0;
						
						TableEvents _TableEvent = new TableEvents(_EventInfo.dbId, _EventInfo.description,
								_EventInfo._TableEventType, _SelectedClient, _EventInfo.event_date, 
								_EventInfo.primary_cost, _EventInfo.event_venue, 
								paymentsettled, _EventInfo.comments, 1);
						
						
						dataRepository.SaveOrUpdateEvent(_TableEvent);
					
						if(_PackageItems != null && _PackageItems.size() > 0){
							for(PackageItem item: _PackageItems){
								TablePackages _package = dataRepository.GetPackage(item.dbId);
								
								if(_package != null){
									TableQuoteItem _TableQuoteItem = null;
									
									if(item.isNewitem){
										_TableQuoteItem = new TableQuoteItem(_package, _TableEvent,
											Integer.parseInt(item.quantiy), Float.parseFloat(item.subtotal));
									}
									else{
										
										TableQuoteItem qItem = dataRepository.GetQuoteItemByIDs(_EventInfo.dbId, _package.getID());
										
										_TableQuoteItem = new TableQuoteItem(qItem.getID(), _package, 
												qItem.getEvent(), Integer.parseInt(item.quantiy), Float.parseFloat(item.subtotal), 1);
									}
									
									dataRepository.SaveOrUpdateQuoteItem(_TableQuoteItem);
								}
							}
						}
						
						_EventInfo = null;
						_PackageItems = new ArrayList<PackageItem>();
						//_PackagesSelected = new ArrayList<TablePackages>();
						
						_CurrentPosition = 1;
						ControlButtons(_CurrentPosition);
						
						sDialog.setTitleText("Sweet!")
	                     .setContentText("Event Was Successfuly Updated.")
	                     .setConfirmText("OK")
	                     .showCancelButton(false)
	                     .setCancelClickListener(null)
	                     .setConfirmClickListener(null)
	                     .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
	                }
	            }).show();
    		}
    		else{
	    		new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
	            .setTitleText("Are you sure?")
	            .setContentText("Event details will be saved to your device.")
	            .setCancelText("No!")
	            .setConfirmText("Yes!")
	            .showCancelButton(true)
	            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
	                @Override
	                public void onClick(SweetAlertDialog sDialog) {
	                    // reuse previous dialog instance, keep widget user state, reset them if you need
	                    sDialog.setTitleText("Oops!")
	                            .setContentText("Operation cancelled.")
	                            .setConfirmText("OK")
	                            .showCancelButton(false)
	                            .setCancelClickListener(null)
	                            .setConfirmClickListener(null)
	                            .changeAlertType(SweetAlertDialog.ERROR_TYPE);
	                }
	            })
	            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
	                @Override
	                public void onClick(SweetAlertDialog sDialog) {
	                	
	                	int paymentsettled = _EventInfo.ispaymentsettled? 1: 0;
						
						final TableEvents _TableEvent = new TableEvents(_EventInfo.description, _EventInfo._TableEventType, 
								_EventInfo._TableClient, _EventInfo.primary_cost, _EventInfo.event_date,
								_EventInfo.event_venue, paymentsettled, _EventInfo.comments);
						
						
						boolean isCommited = dataRepository.SaveOrUpdateEvent(_TableEvent);
						
						if(isCommited){
							
							if(_PackageItems != null && _PackageItems.size() > 0){
								_TableQuoteItems = new ArrayList<TableQuoteItem>();
								
								for(PackageItem item: _PackageItems){
									TablePackages _package = dataRepository.GetPackage(item.dbId);
									if(_package != null){
										TableQuoteItem _TableQuoteItem = new TableQuoteItem(_package, _TableEvent,
												Integer.parseInt(item.quantiy), Float.parseFloat(item.subtotal));
										
										boolean isSaved = dataRepository.SaveOrUpdateQuoteItem(_TableQuoteItem);
										
										if(!isSaved){
											showBuiltInCrouton(INFINITEERROR, item.packagename  + " Was Not Added To Event");
										}else{
											_TableQuoteItems.add(_TableQuoteItem);
										}
									}
								}
							}
							
							_EventInfo = null;
							_PackageItems = new ArrayList<PackageItem>();
							//_PackagesSelected = new ArrayList<TablePackages>();
							
							_CurrentPosition = 1;
							ControlButtons(_CurrentPosition);
							
							sDialog.setTitleText("Event Successfuly Added.")
		                     .setContentText("Click 'Send Quote' to immediately send Quote to client or 'No, Thanks' to cancel.")
		                     .setConfirmText("Send Quote")
		                     .setCancelText("No, Thanks")
		                     .showCancelButton(true)
		                     .setCancelClickListener(new OnSweetClickListener() {
								
								@Override
								public void onClick(SweetAlertDialog sweetAlertDialog) {
									// TODO Auto-generated method stub
									sweetAlertDialog.dismiss();
								}
							}).setConfirmClickListener(new OnSweetClickListener() {
								
								@SuppressLint("SdCardPath")
								@Override
								public void onClick(SweetAlertDialog sweetAlertDialog) {
									// TODO Auto-generated method stub
									sweetAlertDialog.dismissWithAnimation();
									
									_LayoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);

						        	final View convertView = _LayoutInflater.inflate(R.layout.email_messagebody_layout, null);
									
						        	final Builder dialog = new AlertDialog.Builder(context);
	    		        			dialog.setView(convertView);
	    		        			dialog.setCancelable(false);
	    		        			dialog.setTitle("Custom Message");
						        	
	    		        			final EditText subject = (EditText) convertView.findViewById(R.id.textSubject);
	    		        			final EditText message = (EditText) convertView.findViewById(R.id.textMessageBody);
	    		        			
	    		        			subject.setText(_TableSetting.getSubject());
	    		        			message.setText(_TableSetting.getMessage());
	    		        			
	    		        			dialog.setNegativeButton("Skip", new DialogInterface.OnClickListener() {
										
										@Override
										public void onClick(DialogInterface dialog, int which) {
											// TODO Auto-generated method stub
											Date t = new Date();
		    		            			String clientName = _TableEvent.getClient().getLastName();
		    		            			
		    		            			setProgressBarIndeterminateVisibility(true);
		    		            			
			    		                	String fileName = "/sdcard/" + clientName  + " " + formater.format(t) + ".pdf";
			    		            		QuotePDFGenerator _Generator = new QuotePDFGenerator(fileName, _TableEvent, _TableQuoteItems);
			    		            		_Generator.GetGeneratedQuoteDocumentStream();
			    		            		
			    		            		
			    		            		new SendAsyncMail(_TableEvent, fileName, _TableSetting.getMessage(), _TableSetting.getMessage()).execute();
										}
									}).setPositiveButton("Send with message", new DialogInterface.OnClickListener() {
										
										@Override
										public void onClick(DialogInterface dialog, int which) {
											// TODO Auto-generated method stub
											Date t = new Date();
		    		            			String clientName = _TableEvent.getClient().getLastName();
		    		            			String strMessage = message.getText().toString();
											String strSubject = subject.getText().toString();
											
		    		            			setProgressBarIndeterminateVisibility(true);
		    		            			
			    		                	String fileName = "/sdcard/" + clientName  + " " + formater.format(t) + ".pdf";
			    		            		QuotePDFGenerator _Generator = new QuotePDFGenerator(fileName, _TableEvent, _TableQuoteItems);
			    		            		_Generator.GetGeneratedQuoteDocumentStream();
			    		            		
			    		            		
			    		            		new SendAsyncMail(_TableEvent, fileName, strMessage, strSubject).execute();
										}
									}).show();
									
									
								}
							}).changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
						}
						else{
							showBuiltInCrouton(INFINITEERROR, "Event Was Not Added, Please Verify Event Information.");
						}	
	                }
	            }).show();
    		}
    	}
    	return false;
    }
    
    private String status = "";
    private class SendAsyncMail extends AsyncTask<Void, Integer, String> {
		String fileName;
		TableEvents event;
		String message, subject;
		
		public SendAsyncMail(TableEvents event, String fileName, String message, String subject){
			this.event = event;
			this.fileName = fileName;
			this.message = message;
			this.subject = subject;
		}
		@Override
		protected String doInBackground(Void... arg0) {
		    status = global.SendGeneratedQuote(fileName, event,message, subject);
			return status;
		}
		
		@Override
		protected void onProgressUpdate(Integer... progress) {
	         //btnSendMail.setProgress(progress[0]);
	     }
		 
		@Override
		protected void onPostExecute(String result) {
			setProgressBarIndeterminateVisibility(false);
			
			if(result == "Sent") { 
				showBuiltInCrouton(INFINITE, "Email was sent successfully."); 
	          } else { 
	        	  showBuiltInCrouton(INFINITEERROR, "Email was not sent.");
	          } 
        }
    }
    
	@Override
	public void onComplete() {
		// TODO Auto-generated method stub
		
	}
	
	private java.text.DateFormat savedFormat;
    public java.text.DateFormat getDateFormat() {
        if(savedFormat == null)
            savedFormat = DateFormat.getDateTimeInstance();
        return savedFormat;
    }
    
	public void StoreEventInformation(){
		try{
			editEventName = (EditText)_ParentScrollView.findViewById(R.id.editEventName);
	        editEventDate = (EditText)_ParentScrollView.findViewById(R.id.editEventDate);
	        editEventTime = (EditText)_ParentScrollView.findViewById(R.id.editEventTime);
	        editPrimaryCost = (EditText)_ParentScrollView.findViewById(R.id.editPrimaryCost);
	        editComments = (EditText)_ParentScrollView.findViewById(R.id.editComments);
	        cmbEventType = (Spinner)_ParentScrollView.findViewById(R.id.cmbEventType);
	        chkPaymentSettled = (CheckBox)_ParentScrollView.findViewById(R.id.chkPaymentSettled);
	        editEventVenue = (EditText)_ParentScrollView.findViewById(R.id.editEventVenue);
	        
	        if(!IsEditMode){
		        _EventInfo = new EventInfo(editEventName.getText().toString(), _SelectedEventType, 
		        		_SelectedClient, Float.parseFloat(editPrimaryCost.getText().toString()), 
		        		editEventDate.getText().toString() + " " +  editEventTime.getText().toString(), editEventVenue.getText().toString(), 
		        		chkPaymentSettled.isChecked(), editComments.getText().toString());
	        }
	        else{
	        	_EventInfo = new EventInfo(_EventIdToUpdate, editEventName.getText().toString(), _SelectedEventType, 
		        		_SelectedClient, Float.parseFloat(editPrimaryCost.getText().toString()), 
		        		editEventDate.getText().toString() + " " +  editEventTime.getText().toString(), editEventVenue.getText().toString(), 
		        		chkPaymentSettled.isChecked(), editComments.getText().toString());
	        }
		}
		catch(Exception Ex){
			
		}
	}
	
	public void GetEventInformation(){

		try{
			editEventName = (EditText)_ParentScrollView.findViewById(R.id.editEventName);
	        editEventDate = (EditText)_ParentScrollView.findViewById(R.id.editEventDate);
	        editEventTime = (EditText)_ParentScrollView.findViewById(R.id.editEventTime);
	        editPrimaryCost = (EditText)_ParentScrollView.findViewById(R.id.editPrimaryCost);
	        editComments = (EditText)_ParentScrollView.findViewById(R.id.editComments);
	        cmbEventType = (Spinner)_ParentScrollView.findViewById(R.id.cmbEventType);
	        chkPaymentSettled = (CheckBox)_ParentScrollView.findViewById(R.id.chkPaymentSettled);
	        editEventVenue = (EditText)_ParentScrollView.findViewById(R.id.editEventVenue);
	        
	        if(_EventInfo != null){
		        String[] datetime = _EventInfo.event_date.split(" ");
		        
		        editEventName.setText(_EventInfo.description);
		        editEventDate.setText(datetime[0]);
		        editEventTime.setText(datetime[1]);
		        editPrimaryCost.setText(String.valueOf(_EventInfo.primary_cost));
		        editComments.setText(_EventInfo.comments);
		        editEventVenue.setText(_EventInfo.event_venue);
		        chkPaymentSettled.setChecked(_EventInfo.ispaymentsettled);
	        }
		}
		catch(Exception Ex){
			
		}
	}
	
	public void ControlButtons(int CurrentPosition)
	{
		switch(CurrentPosition)
		{
			case 1:
				InflateFormView(R.layout.eventinfo_layout);
				_StepText.setText("Event");
				_StepHeadingText.setText("Step " + String.valueOf(CurrentPosition) + " of " + String.valueOf(NumOfSteps));
				
		        GetEventInformation();
		        
		        validator.put(editEventName, global.RequiredRule);
				validator.put(editEventDate, global.RequiredRule);
				validator.put(editEventTime, global.RequiredRule);
				validator.put(editEventVenue, global.RequiredRule);
				validator.put(editPrimaryCost, global.RequiredRule);
		        
				cmbClientNames = (Spinner)_ParentScrollView.findViewById(R.id.cmbClientName);
				cmbEventType = (Spinner)_ParentScrollView.findViewById(R.id.cmbEventType);
		        
				editEventDate.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						_SelectedDateTimeView = v;
						datePickerDialog.setVibrate(true);
				        datePickerDialog.setYearRange(1985, 2028);
				        datePickerDialog.setCloseOnSingleTapDay(false);
				        datePickerDialog.show(getSupportFragmentManager(), DATEPICKER_TAG);
					}
				});
				
				editEventTime.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
		                _SelectedDateTimeView = v;
						// TODO Auto-generated method stub
						timePickerDialog.setVibrate(true);
		                timePickerDialog.setCloseOnSingleTapMinute(false);
		                timePickerDialog.show(getSupportFragmentManager(), TIMEPICKER_TAG);
					}
				});
		        
				if(_TableClients != null){
		        	PopulateClientAdapter(_TableClients);
		        	cmbClientNames.setAdapter(_ClientNamesAdapter);
		        	_ClientNamesAdapter.notifyDataSetChanged();
		        	cmbClientNames.setOnItemSelectedListener(onItemSelectedListenerClients);
		        	cmbClientNames.setSelection(ClientPosition);
		        }
		        
		        if(_TableEventTypes != null){
		        	PopulateEventTyeAdapter(_TableEventTypes);
		        	cmbEventType.setAdapter(_EventTypeAdater);
		        	_EventTypeAdater.notifyDataSetChanged();
		        	cmbEventType.setOnItemSelectedListener(onItemSelectedListenerEventTypes);
			        cmbEventType.setSelection(EventTypePosition);
		        }
		        
		        editEventName.requestFocus();
			break;
		case 2:
			StoreEventInformation();
			InflateFormView(R.layout.select_packages_layout);
			_StepText.setText("Event Packages");
			_StepHeadingText.setText("Step " + String.valueOf(CurrentPosition) + " of " + String.valueOf(NumOfSteps));
			
			
			_ListPackages = (ListView)_ParentScrollView.findViewById(R.id.package_list);
			_ListPackageHeading = (ListView)_ParentScrollView.findViewById(R.id.package_list_heading);
			_ExpandableLayout = (ExpandableLayout) _ParentScrollView.findViewById(R.id.expandable);
			
			cmbPackages = (Spinner) _ExpandableLayout.findViewById(R.id.cmbPackages);
			editQuantity = (EditText) _ExpandableLayout.findViewById(R.id.editQuantity);
			btnAddPackage = (Button) _ExpandableLayout.findViewById(R.id.button_addpackage);
			
			List<PackageItem> heading = new ArrayList<PackageItem>();
			heading.add(new PackageItem("", "Quantity", "Package", "Price", "Sub Total", false));
			
			_SelectPackageAdapterHeading = new SelectPackageAdapter(this, R.layout.package_row_item, heading, true);
			_ListPackageHeading.setAdapter(_SelectPackageAdapterHeading);
			
			int modeInt = 0;
			SwipeDismissList.UndoMode mode = SwipeDismissList.UndoMode.values()[modeInt];
			
			new SwipeDismissList(
					// 1st parameter is the ListView you want to use
					_ListPackages,
					
					new SwipeDismissList.OnDismissCallback() {
					
					public SwipeDismissList.Undoable onDismiss(AbsListView listView, final int position) {

						// Get item that should be deleted from the adapter.
						final PackageItem item = _SelectPackageAdapter.getItem(position);
						//final TablePackages packageItem = _PackagesSelected.get(position);
						// Delete that item from the adapter.
						_SelectPackageAdapter.remove(item);
						//_PackagesSelected.remove(packageItem);
						
						// Return an Undoable, for that deletion. If you write return null
						// instead, this deletion won't be undoable.
						return new SwipeDismissList.Undoable() {
							
							@Override
							public String getTitle() {
								return item.packagename + " deleted";
							}

							@Override
							public void undo() {
								// Reinsert the item at its previous position.
								_SelectPackageAdapter.insert(item, position);
								//_PackagesSelected.add(position, packageItem);
							}
							
							@Override
							public void discard() {
								// Just write a log message (use logcat to see the effect)
								Log.w("DISCARD", "item " + item + " now finally discarded");
							}
						};

					}
				},mode).setAutoHideDelay(3000);
			
			if(_TablePackages != null){
				PopulatePackagesAdapter(_TablePackages);
				cmbPackages.setAdapter(_PackagesAdater);
				_PackagesAdater.notifyDataSetChanged();
				
				cmbPackages.setOnItemSelectedListener(onItemSelectedListenerPackages);
		        cmbPackages.setSelection(PackagePosition);
				
				btnAddPackage.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						String quantity = editQuantity.getText().toString();
						if(quantity.length() > 0){
							int Q = Integer.valueOf(quantity);
							Q = Q == 0? 1: Q;
							float price = 0f;
							float subtotal = 0f;
							String packagename = "";
							
							if(_SelectedPackage != null){
								price = (float) _SelectedPackage.getPrice();
								packagename = _SelectedPackage.getDescription();
								subtotal = price * Q;
								
								PackageItem item = new PackageItem(_SelectedPackage.getID(), quantity,
										packagename, String.valueOf(price), String.valueOf(subtotal), true);
								
								if(_PackageItems == null)
									_PackageItems = new ArrayList<PackageItem>();
								
								_PackageItems.add(item);
								
								if(_SelectPackageAdapter == null){
									_SelectPackageAdapter = new SelectPackageAdapter(context, R.layout.package_row_item, _PackageItems, false);
									_ListPackages.setAdapter(_SelectPackageAdapter);
								}
								else{
									_SelectPackageAdapter.notifyDataSetChanged();
								}
								
								//if(_PackagesSelected == null)
								//	_PackagesSelected = new ArrayList<TablePackages>();
								
								//_PackagesSelected.add(_SelectedPackage);
							}
							
							editQuantity.setText(null);
						}else{
							showBuiltInCrouton(Style.ALERT, "Please Enter Quantity");
						}
					}
				});
			}
			
			break;
		}
		
		if(CurrentPosition >= NumOfSteps)
		{
			EnableNextButton(false);
			EnablePrevButton(true);
			
			if(_PackageItems != null && _PackageItems.size() > 0)
			{
				PopulatePackagesGrid();
			}
		}
		else if(CurrentPosition <= 1)
		{
			EnableNextButton(true);
			EnablePrevButton(false);
		}
		else
		{
			EnableNextButton(true);
			EnablePrevButton(true);
		}
		
		invalidateOptionsMenu();
	}
	
	public void PopulatePackagesGrid()
	{
		_SelectPackageAdapter = new SelectPackageAdapter(context, R.layout.package_row_item, _PackageItems, false);
		_ListPackages.setAdapter(_SelectPackageAdapter);
	}
	
	public void EnableNextButton(boolean enable)
	{
		if(enable)
		{
			//_ButtonNext.setBackgroundResource(R.drawable.custom_btn_enabled);
			_ButtonNext.setEnabled(true);
		}
		else
		{
			//_ButtonNext.setBackgroundResource(R.drawable.custom_btn_disabled);
			_ButtonNext.setEnabled(false);
		}
	}
	
	public void EnablePrevButton(boolean enable)
	{
		if(enable)
		{
			//_ButtonPrev.setBackgroundResource(R.drawable.custom_btn_enabled);
			_ButtonPrev.setEnabled(true);
		}
		else
		{
			//_ButtonPrev.setBackgroundResource(R.drawable.custom_btn_disabled);
			_ButtonPrev.setEnabled(false);
		}	
	}
	
	public void InflateFormView(int _ChildViewID)
    {
		if(_ParentScrollView == null)
    		_ParentScrollView = (LinearLayout) findViewById(R.id.content_wrapper_scroll);
    	
    	
    	if(_ChildView != null)
		{
    		_ParentScrollView.removeView(_ChildView);
    		_ChildView = _ViewInflater.inflate(_ChildViewID, null);
		 	_ParentScrollView.addView(_ChildView);
		}
		else
		{
			_ChildView = _ViewInflater.inflate(_ChildViewID, null);
			_ParentScrollView.addView(_ChildView);
		}
    	
    	final ViewAnimator viewAnimator = (ViewAnimator)_ParentScrollView.findViewById(R.id.viewFlipper);
    	
    	if(viewAnimator != null)
    		AnimationFactory.flipTransition(viewAnimator, FlipDirection.LEFT_RIGHT);
    }

	private void PopulateEventTyeAdapter(List<TableEventTypes> lsData)
	{
		String[] _ArraySt = new String[lsData.size()];
		int arrcount = 0;
		
		for(TableEventTypes _LookUpData : lsData)
		{
			_ArraySt[arrcount] = _LookUpData.getDescription();
			arrcount++;
		}
		
		_EventTypeAdater = new ArrayAdapter<String>(this,
				R.layout.simple_list_view, _ArraySt);
		
		_EventTypeAdater.setDropDownViewResource(R.layout.custom_spinner);
	}
	
	private void PopulateClientAdapter(List<TableClients> lsData)
	{
		String[] _ArraySt = new String[lsData.size()];
		int arrcount = 0;
		
		for(TableClients _LookUpData : lsData)
		{
			_ArraySt[arrcount] = _LookUpData.getLastName() + " " +_LookUpData.getFullName() ;
			arrcount++;
		}
		
		_ClientNamesAdapter = new ArrayAdapter<String>(this,
				R.layout.simple_list_view, _ArraySt);
		
		_ClientNamesAdapter.setDropDownViewResource(R.layout.custom_spinner);
	}
	
	private void PopulatePackagesAdapter(List<TablePackages> lsData)
	{
		String[] _ArraySt = new String[lsData.size()];
		int arrcount = 0;
		
		for(TablePackages _LookUpData : lsData)
		{
			_ArraySt[arrcount] = _LookUpData.getDescription();
			arrcount++;
		}
		
		_PackagesAdater = new ArrayAdapter<String>(this,
				R.layout.simple_list_view, _ArraySt);
		
		_PackagesAdater.setDropDownViewResource(R.layout.custom_spinner);
	}
	
	
	OnItemSelectedListener onItemSelectedListenerPackages = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position,
				long id2) {
			// TODO Auto-generated method stub
			PackagePosition = position;
			_SelectedPackage = getPackageByID(parent.getItemAtPosition(position).toString());
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {}
	};

	OnItemSelectedListener onItemSelectedListenerEventTypes = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position,
				long id2) {
			// TODO Auto-generated method stub
			EventTypePosition = position;
			_SelectedEventType = getEventTypeByID(parent.getItemAtPosition(position).toString());
			editPrimaryCost.setText(String.valueOf(_SelectedEventType.getPrice()));
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {}
	};
	
	OnItemSelectedListener onItemSelectedListenerClients = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position,
				long id2) {
			// TODO Auto-generated method stub
			ClientPosition = position;
			_SelectedClient = getClientByID(parent.getItemAtPosition(position).toString());
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {}
	};
	public TablePackages getPackageByID(String Value)
    {
    	TablePackages result = null;
    	
    	try {
			 
        	for(TablePackages _Obj : _TablePackages)
    		{
        		if(Value.equalsIgnoreCase(_Obj.getDescription()))
    			{
        			result = _Obj;
    				break;
    			}
    		}
		} catch (Exception e) {
			// TODO: handle exception
		}		
    	return result;
    }
	
	public TableClients getClientByID(String Value)
    {
		TableClients result = null;
    	
    	try {
			 
        	for(TableClients _Obj : _TableClients)
    		{
        		String clientname = _Obj.getLastName() + " " + _Obj.getFullName();
        		
        		if(Value.equalsIgnoreCase(clientname))
    			{
        			result = _Obj;
    				break;
    			}
    		}
		} catch (Exception e) {
			// TODO: handle exception
		}		
    	return result;
    }
	
	public TableEventTypes getEventTypeByID(String Value)
    {
		TableEventTypes result = null;
    	
    	try {
			 
        	for(TableEventTypes _Obj : _TableEventTypes)
    		{
        		if(Value.equalsIgnoreCase(_Obj.getDescription()))
    			{
        			result = _Obj;
    				break;
    			}
    		}
		} catch (Exception e) {
			// TODO: handle exception
		}		
    	return result;
    }
	
	@Override
    public void onBackPressed() {
		setResult(2);
		finish();
	}
	
	@Override
	public void onValidationSucceeded() {
		// TODO Auto-generated method stub
		_CurrentPosition++;
		ControlButtons(_CurrentPosition);
	}

	@Override
	public void onValidationFailed(View failedView, Rule<?> failedRule) {
		// TODO Auto-generated method stub
		String message = failedRule.getFailureMessage();

        if (failedView instanceof EditText) {
            failedView.requestFocus();
            ((EditText) failedView).setError(message);
        } else {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
	}

	@Override
	public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
		EditText v = (EditText)_SelectedDateTimeView;
		v.setText(hourOfDay + ":" + String.format("%02d", minute));
	}

	@Override
	public void onDateSet(DatePickerDialog datePickerDialog, int year,
			int month, int day) {
		EditText v = (EditText)_SelectedDateTimeView;
		v.setText(year + "/" + String.format("%02d", (month+1)) + "/" + String.format("%02d", day));
	}

}
