package com.eventplanner;

import java.io.Serializable;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import cn.pedant.SweetAlert.SweetAlertDialog;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;
import com.capricorn.ArcMenu;
import com.dd.processbutton.iml.ActionProcessButton;
import com.eventplanner.R;
import com.eventplanner.adapters.SelectPackageAdapter;
import com.eventplanner.database.EventPlannerRepository;
import com.eventplanner.database.EventPlannerSQLiteDB;
import com.eventplanner.databasetables.TableClients;
import com.eventplanner.databasetables.TableEventTypes;
import com.eventplanner.databasetables.TableEvents;
import com.eventplanner.databasetables.TablePackages;
import com.eventplanner.databasetables.TableQuoteItem;
import com.eventplanner.dataclass.GlobalVariables;
import com.eventplanner.dataclass.PackageItem;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class EventViewEditActivity extends SherlockFragmentActivity {

	private EditText editEventName;
    private EditText editEventVenue;
    private EditText editEventDate;
    private Spinner cmbEventType;
    private EditText editPrimaryCost;
    private EditText editComments;
    private CheckBox chkPaymentSettled;
	private Spinner cmbClientNames;
	
	TextView _StepText;
	TextView _StepHeadingText;
	
	EventPlannerSQLiteDB databaseHelper;
	EventPlannerRepository dataRepository;
	
	private List<TableClients> _TableClients = null;
	private List<TableEventTypes> _TableEventTypes = null;
	private List<TablePackages> _TablePackages = null;
	private List<PackageItem> _PackageItems = null;;
	//private EventInfo _EventInfo = null;
	
	GlobalVariables global = new GlobalVariables();
	
	private ArrayAdapter<String> _EventTypeAdater, _ClientNamesAdapter, _PackagesAdater;
	private SelectPackageAdapter _SelectPackageAdapter, _SelectPackageAdapterHeading;
	
	private Context context;
	
	private TableEventTypes _SelectedEventType = null;
	private TableEvents _SelectedEvent = null;
	
	private List<TablePackages> _PackagesSelected;
	
	private int ClientPosition, EventTypePosition, PackagePosition;
	
	private ListView _ListPackages, _ListPackageHeading;
	private SlidingUpPanelLayout _SlidingUpPanelLayout;
    
	private NumberFormat fmt;
	
	private static final int[] ITEM_DRAWABLES = { R.drawable.empty_circle, R.drawable.phone_icon,
		R.drawable.profile_icon, R.drawable.location_icon, R.drawable.delele_icon };
	
	private Bundle _Bundle;
	private Intent _Intent;
	private TableClients _TableClient;
	
	private static final Style INFINITE = new Style.Builder().
		    setBackgroundColorValue(Style.holoBlueLight).build();
	
	private static final Style INFINITEERROR = new Style.Builder().
		    setBackgroundColorValue(Style.holoRedLight).build();
	
	private static final de.keyboardsurfer.android.widget.crouton.Configuration CONFIGURATION_INFINITE = new de.keyboardsurfer.android.widget.crouton.Configuration.Builder()
	          .setDuration(de.keyboardsurfer.android.widget.crouton.Configuration.DURATION_INFINITE)
	          .build();
	
	private void showBuiltInCrouton(final Style croutonStyle, String croutonText) {
	    showCrouton(croutonText, croutonStyle, de.keyboardsurfer.android.widget.crouton.Configuration.DEFAULT);
	 }
	
	private void showCrouton(String croutonText, Style croutonStyle, de.keyboardsurfer.android.widget.crouton.Configuration configuration) {
	    final boolean infinite = INFINITE == croutonStyle || INFINITEERROR == croutonStyle;
	    
	       
	    final Crouton crouton;
	      crouton = Crouton.makeText(this, croutonText, croutonStyle);
	    
	    if (infinite) {
	    }
	    crouton.setConfiguration((de.keyboardsurfer.android.widget.crouton.Configuration) (infinite ? CONFIGURATION_INFINITE : configuration)).show();
	}
	
    @SuppressWarnings("unchecked")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.event_view_edit_layout);
        
		getSupportActionBar().show();
		getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.background_img4));
		
		getSupportActionBar().setTitle("Event Planner");
		getSupportActionBar().setSubtitle("Managing Events");
         
		getSupportActionBar().setLogo(R.drawable.ic_launcher);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);
		getSupportActionBar().setDisplayShowHomeEnabled(false);
        
        setProgressBarIndeterminateVisibility(false);
        context = this;
        
        
        Locale locale = new Locale("en_ZA");
		fmt = NumberFormat.getCurrencyInstance(locale);
        
        databaseHelper = new EventPlannerSQLiteDB(this);
        dataRepository = new EventPlannerRepository(databaseHelper);
        
        _TableClients = dataRepository.GetAllClients();
        _TableEventTypes = dataRepository.GetAllEventTypes();
        _TablePackages = dataRepository.GetAllPackages();
        
        editEventName = (EditText)findViewById(R.id.editEventName);
        editEventDate = (EditText)findViewById(R.id.editEventDate);
        editPrimaryCost = (EditText)findViewById(R.id.editPrimaryCost);
        editComments = (EditText)findViewById(R.id.editComments);
        cmbClientNames = (Spinner)findViewById(R.id.cmbClientName);
		cmbEventType = (Spinner)findViewById(R.id.cmbEventType);
        chkPaymentSettled = (CheckBox)findViewById(R.id.chkPaymentSettled);
        editEventVenue = (EditText)findViewById(R.id.editEventVenue);
        _SlidingUpPanelLayout = (SlidingUpPanelLayout)findViewById(R.id.sliding_layout);
		
        _ListPackageHeading = (ListView) _SlidingUpPanelLayout.findViewById(R.id.eventlist_heading);
        _ListPackages = (ListView) _SlidingUpPanelLayout.findViewById(R.id.eventlist);
        
        try{
        	InputMethodManager imm = (InputMethodManager)getSystemService(
			Context.INPUT_METHOD_SERVICE);
			//txtName is a reference of an EditText Field
			imm.hideSoftInputFromWindow(editEventName.getWindowToken(), 0);
        }catch(Exception e){
        	e.printStackTrace();
        }
        

        ArcMenu arcMenu = (ArcMenu) findViewById(R.id.arc_menu);
        initArcMenu(arcMenu, ITEM_DRAWABLES);

        if(_TableClients != null){
        	PopulateClientAdapter(_TableClients);
        	cmbClientNames.setAdapter(_ClientNamesAdapter);
        	_ClientNamesAdapter.notifyDataSetChanged();
        	cmbClientNames.setOnItemSelectedListener(onItemSelectedListenerClients);
        	cmbClientNames.setSelection(ClientPosition);
        }
        
        if(_TableEventTypes != null){
        	PopulateEventTyeAdapter(_TableEventTypes);
        	cmbEventType.setAdapter(_EventTypeAdater);
        	_EventTypeAdater.notifyDataSetChanged();
        	cmbEventType.setOnItemSelectedListener(onItemSelectedListenerEventTypes);
	        cmbEventType.setSelection(EventTypePosition);
        }
        
        if(savedInstanceState != null){
        	_PackageItems = (List<PackageItem>) savedInstanceState.getSerializable("_PackageItems");
        	_SelectedEvent = (TableEvents) savedInstanceState.getSerializable("_SelectedEvent");
        	_PackagesSelected = (List<TablePackages>) savedInstanceState.getSerializable("_PackagesSelected");
        	
        	ClientPosition = savedInstanceState.getInt("ClientPosition");
        	EventTypePosition = savedInstanceState.getInt("EventTypePosition");
        	PackagePosition = savedInstanceState.getInt("PackagePosition");
        }
        else{

            Bundle bundle = getIntent().getExtras().getBundle("Data");
        	
        	if(bundle != null){
        		_SelectedEvent = (TableEvents) bundle.getSerializable("_TableEvent");
        	}
            
        	List<TableQuoteItem> _TableQuoteItems = dataRepository.GetQuoteItemByEventID(_SelectedEvent.getID());
        	_PackageItems = new ArrayList<PackageItem>();
        	
        	if(_TableQuoteItems != null){
        		for(TableQuoteItem qItem: _TableQuoteItems){
        			PackageItem item = new PackageItem(qItem.getPackage().getID(), String.valueOf(qItem.getQuantity()),
        					qItem.getPackage().getDescription(), String.valueOf(qItem.getPackage().getPrice()), 
        					String.valueOf(qItem.getTotalPrice()), false);
        			
        			_PackageItems.add(item);
        		}
        	}
        }
        
        List<PackageItem> heading = new ArrayList<PackageItem>();
		heading.add(new PackageItem("", "Quantity", "Package", "Price", "Sub Total", true));
		
		_SelectPackageAdapterHeading = new SelectPackageAdapter(this, R.layout.package_row_item, heading, true);
		_ListPackageHeading.setAdapter(_SelectPackageAdapterHeading);
		
		 GetEventInformation();
		 
		 EventTypePosition = _EventTypeAdater.getPosition(GetSelectedEventTypeName(_SelectedEvent.getEventType().getID()));
     	cmbEventType.setSelection(EventTypePosition);
     	
     	ClientPosition = _ClientNamesAdapter.getPosition(GetSelectedClientName(_SelectedEvent.getClient().getID()));
     	cmbClientNames.setSelection(ClientPosition);
     	
     	DisableAllControls();
     	
        if(_PackageItems != null && _PackageItems.size() > 0)
		{
			PopulatePackagesGrid();
		}
    }
    
    private void initArcMenu(ArcMenu menu, int[] itemDrawables) {
        final int itemCount = itemDrawables.length;
        for (int i = 0; i < itemCount; i++) {
            ImageView item = new ImageView(this);
            item.setImageResource(itemDrawables[i]);

            final int position = i;
            menu.addItem(item, new OnClickListener() {

                @Override
                public void onClick(View v) {
                	
                	switch (position) {
					case 0: 
						
						break;
					case 1:
						//Call
						if(_SelectedEvent != null){
							_TableClient = _SelectedEvent.getClient();
							
							new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
					        .setTitleText("Are you sure?")
					        .setContentText("A call will be made to Client (" + _TableClient.getMobileNumber() +") linked to this event.")
					        .setCancelText("No!")
					        .setConfirmText("Yes!")
					        .showCancelButton(true)
					        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
					            @Override
					            public void onClick(SweetAlertDialog sDialog) {
					                // reuse previous dialog instance, keep widget user state, reset them if you need
					                sDialog.setTitleText("Oops!")
					                        .setContentText("Operation cancelled.")
					                        .setConfirmText("OK")
					                        .showCancelButton(false)
					                        .setCancelClickListener(null)
					                        .setConfirmClickListener(null)
					                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
					            }
					        })
					        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
					            @Override
					            public void onClick(SweetAlertDialog sDialog) {
					            	sDialog.dismissWithAnimation();
					            	
					            	Intent intent = new Intent(Intent.ACTION_CALL);

					            	intent.setData(Uri.parse("tel:" +_TableClient.getMobileNumber()));
					            	context.startActivity(intent);
					            }
					        }).show();
						}
						break;
					case 2:
						//Client Profile
						if(_SelectedEvent != null){
							_TableClient = _SelectedEvent.getClient();
							_Bundle = new Bundle();
							_Bundle.putSerializable("_TableClient", _TableClient);
							_Bundle.putBoolean("IsEditMode", false);
							_Intent = new Intent(EventViewEditActivity.this, ClientFormActivity.class);
							_Intent.putExtras(_Bundle);
							startActivity(_Intent);
						}
						break;
					case 3:
						if(_SelectedEvent != null){
							_TableClient = _SelectedEvent.getClient();
							_Bundle = new Bundle();
							_Bundle.putSerializable("_SelectedEvent", _SelectedEvent);
							_Intent = new Intent(EventViewEditActivity.this, EventVenueOnMapActivity.class);
							_Intent.putExtras(_Bundle);
							startActivity(_Intent);
						}
						break;
					case 4:
						//Delete
						new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
	    		        .setTitleText("Are you sure?")
	    		        .setContentText("Event will be deleted permanetly!")
	    		        .setCancelText("No!")
	    		        .setConfirmText("Yes!")
	    		        .showCancelButton(true)
	    		        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
	    		            @Override
	    		            public void onClick(SweetAlertDialog sDialog) {
	    		                // reuse previous dialog instance, keep widget user state, reset them if you need
	    		                sDialog.setTitleText("Oops!")
	    		                        .setContentText("Operation cancelled.")
	    		                        .setConfirmText("OK")
	    		                        .showCancelButton(false)
	    		                        .setCancelClickListener(null)
	    		                        .setConfirmClickListener(null)
	    		                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
	    		            }
	    		        })
	    		        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
	    		            @Override
	    		            public void onClick(SweetAlertDialog sDialog) {
	    		            	try{List<TableQuoteItem> _QuoteItems = dataRepository.GetQuoteItemByEventID(_SelectedEvent.getID());
	    		            		
	    		            		
	    		            		if(_QuoteItems != null)
	    		            			dataRepository.DeleteQuoteItems(_QuoteItems);
	    		            		
	    		            		dataRepository.DeleteEvent(_SelectedEvent.getID());
	    		            		
	    		            		sDialog.setTitleText("Sweet!")
	    		                     .setContentText("Event details successfuly deleted.")
	    		                     .setConfirmText("OK")
	    		                     .showCancelButton(false)
	    		                     .setCancelClickListener(null)
	    		                     .setConfirmClickListener(null)
	    		                     .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
	    		            		
	    		            		setResult(1);
	    		            		finish();
	    		            		
	    						}
	    						catch(Exception Ex){
	    							showBuiltInCrouton(INFINITEERROR, Ex.getMessage());
	    						}
	    		            }
	    		        }).show();
						break;
					}
                	
                   // Toast.makeText(EventViewEditActivity.this, "position:" + position, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
    
    @Override
	public void onSaveInstanceState(Bundle state) {
	    super.onSaveInstanceState(state);
	
	    state.putInt("ClientPosition", ClientPosition);
	    state.putInt("PackagePosition", PackagePosition);
	    state.putInt("EventTypePosition", EventTypePosition);
	    state.putSerializable("_PackageItems", (Serializable) _PackageItems);
	    state.putSerializable("_SelectedEvent", _SelectedEvent);
	    state.putSerializable("_PackagesSelected", (Serializable) _PackagesSelected);
	}
    
    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
    	if(item.getItemId() == android.R.id.home)
    	{
			finish();
    	}
    	else if(item.getNumericShortcut() == 'S'){
    		
    	}
    	return false;
    }
    
    @Override
    public void onResume(){
    	super.onResume();
    }
    
    public void DisableAllControls()
	{
    	try{
			RelativeLayout layout = (RelativeLayout)findViewById(R.id.event_form);
			
			for(int i = 0; i < layout.getChildCount();i++)
			{
				if((layout.getChildAt(i) instanceof EditText) ||
						(layout.getChildAt(i) instanceof RadioButton) ||
						(layout.getChildAt(i) instanceof Spinner)||
						(layout.getChildAt(i) instanceof ActionProcessButton)||
						(layout.getChildAt(i) instanceof Button))
				{
					layout.getChildAt(i).setEnabled(false);
				}
			}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
	}
    
	public void GetEventInformation(){

		try{
			SimpleDateFormat f = new SimpleDateFormat("dd MMM yyyy HH:ss", Locale.US);
	        
	        if(_SelectedEvent != null){
		     
		        editEventName.setText(_SelectedEvent.getDescription());
		        editEventDate.setText(f.format(_SelectedEvent.getEventDate()));
		        editPrimaryCost.setText(fmt.format(_SelectedEvent.getPrimaryCost()));
		        editComments.setText(_SelectedEvent.getComments());
		        editEventVenue.setText(_SelectedEvent.getEventVenue());
		        
		        boolean payed = _SelectedEvent.getIsPaymentSettled() == 1? true:false;
		        
		        chkPaymentSettled.setChecked(payed);
	        }
		}
		catch(Exception Ex){
			
		}
	}
	
	public void PopulatePackagesGrid()
	{
		_SelectPackageAdapter = new SelectPackageAdapter(context, R.layout.package_row_item, _PackageItems, false);
		_ListPackages.setAdapter(_SelectPackageAdapter);
	}

	private void PopulateEventTyeAdapter(List<TableEventTypes> lsData)
	{
		String[] _ArraySt = new String[lsData.size()];
		int arrcount = 0;
		
		for(TableEventTypes _LookUpData : lsData)
		{
			_ArraySt[arrcount] = _LookUpData.getDescription();
			arrcount++;
		}
		
		_EventTypeAdater = new ArrayAdapter<String>(this,
				R.layout.simple_list_view, _ArraySt);
		
		_EventTypeAdater.setDropDownViewResource(R.layout.custom_spinner);
	}
	
	private void PopulateClientAdapter(List<TableClients> lsData)
	{
		String[] _ArraySt = new String[lsData.size()];
		int arrcount = 0;
		
		for(TableClients _LookUpData : lsData)
		{
			_ArraySt[arrcount] = _LookUpData.getLastName() + " " +_LookUpData.getFullName() ;
			arrcount++;
		}
		
		_ClientNamesAdapter = new ArrayAdapter<String>(this,
				R.layout.simple_list_view, _ArraySt);
		
		_ClientNamesAdapter.setDropDownViewResource(R.layout.custom_spinner);
	}
	
	@SuppressWarnings("unused")
	private void PopulatePackagesAdapter(List<TablePackages> lsData)
	{
		String[] _ArraySt = new String[lsData.size()];
		int arrcount = 0;
		
		for(TablePackages _LookUpData : lsData)
		{
			_ArraySt[arrcount] = _LookUpData.getDescription();
			arrcount++;
		}
		
		_PackagesAdater = new ArrayAdapter<String>(this,
				R.layout.simple_list_view, _ArraySt);
		
		_PackagesAdater.setDropDownViewResource(R.layout.custom_spinner);
	}
	
	
	OnItemSelectedListener onItemSelectedListenerPackages = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position,
				long id2) {
			// TODO Auto-generated method stub
			PackagePosition = position;
			getPackageByID(parent.getItemAtPosition(position).toString());
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {}
	};

	OnItemSelectedListener onItemSelectedListenerEventTypes = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position,
				long id2) {
			// TODO Auto-generated method stub
			EventTypePosition = position;
			_SelectedEventType = getEventTypeByID(parent.getItemAtPosition(position).toString());
			editPrimaryCost.setText(String.valueOf(_SelectedEventType.getPrice()));
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {}
	};
	
	OnItemSelectedListener onItemSelectedListenerClients = new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int position,
				long id2) {
			// TODO Auto-generated method stub
			ClientPosition = position;
			getClientByID(parent.getItemAtPosition(position).toString());
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {}
	};
	
	public String GetSelectedEventTypeName(String Value){
		String returnv = null;
    	
    	try 
    	{
        	for(TableEventTypes t : _TableEventTypes)
    		{
        		if(Value.equalsIgnoreCase(t.getID()))
    			{
    				returnv = t.getDescription();
    				break;
    			}
    		}
		} catch (Exception e) {
			// TODO: handle exception
		}		
    	return returnv;
	}
    
    public String GetSelectedClientName(String Value){
		String returnv = null;
    	
    	try 
    	{
        	for(TableClients c : _TableClients)
    		{
        		if(Value.equalsIgnoreCase(c.getID()))
    			{
    				returnv = c.getLastName() + " " + c.getFullName();
    				break;
    			}
    		}
		} catch (Exception e) {
			// TODO: handle exception
		}		
    	return returnv;
	}
    
	public TablePackages getPackageByID(String Value)
    {
    	TablePackages result = null;
    	
    	try {
			 
        	for(TablePackages _Obj : _TablePackages)
    		{
        		if(Value.equalsIgnoreCase(_Obj.getDescription()))
    			{
        			result = _Obj;
    				break;
    			}
    		}
		} catch (Exception e) {
			// TODO: handle exception
		}		
    	return result;
    }
	
	public TableClients getClientByID(String Value)
    {
		TableClients result = null;
    	
    	try {
			 
        	for(TableClients _Obj : _TableClients)
    		{
        		String clientname = _Obj.getLastName() + " " + _Obj.getFullName();
        		
        		if(Value.equalsIgnoreCase(clientname))
    			{
        			result = _Obj;
    				break;
    			}
    		}
		} catch (Exception e) {
			// TODO: handle exception
		}		
    	return result;
    }
	
	public TableEventTypes getEventTypeByID(String Value)
    {
		TableEventTypes result = null;
    	
    	try {
			 
        	for(TableEventTypes _Obj : _TableEventTypes)
    		{
        		if(Value.equalsIgnoreCase(_Obj.getDescription()))
    			{
        			result = _Obj;
    				break;
    			}
    		}
		} catch (Exception e) {
			// TODO: handle exception
		}		
    	return result;
    }
	
	@Override
    public void onBackPressed() {
		setResult(1);
		finish();
	}

}
