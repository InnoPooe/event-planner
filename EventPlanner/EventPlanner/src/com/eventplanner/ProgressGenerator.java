package com.eventplanner;

import com.dd.processbutton.ProcessButton;
import com.eventplanner.dataclass.GlobalVariables;

import android.os.Handler;

import java.util.Random;

public class ProgressGenerator {

    public interface OnCompleteListener {

        public void onComplete();
    }

    private OnCompleteListener mListener;
    private int mProgress;
    private Handler handler;
    
    public ProgressGenerator(OnCompleteListener listener) {
        mListener = listener;
    }

    public void start(final ProcessButton button) {
        handler = new Handler();
    
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mProgress += 10;
                button.setProgress(mProgress);
                if (mProgress < GlobalVariables.ButtonAnimationControl) {
                    handler.postDelayed(this, generateDelay());
                } else {
                	button.setProgress(0);
                    mListener.onComplete();
                }
            }
        }, generateDelay());
    }
    
    public void stop(){
    	//handler.re
    	mListener.onComplete();
    }
    
    private Random random = new Random();

    private int generateDelay() {
        return random.nextInt(1000);
    }
}
