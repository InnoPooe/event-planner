package com.eventplanner;

import java.io.Serializable;
import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.dd.processbutton.iml.ActionProcessButton;
import com.eventplanner.database.EventPlannerRepository;
import com.eventplanner.database.EventPlannerSQLiteDB;
import com.eventplanner.databasetables.TableSettings;
import com.eventplanner.databasetables.TableUsers;
import com.eventplanner.dataclass.GlobalVariables;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


/**
 * Android login screen Activity
 */
public class LoginActivity extends SherlockActivity implements ValidationListener,
ProgressGenerator.OnCompleteListener{
    private EditText editEmail;
    private EditText editPassword;
    private TextView signUpTextView;
    private ProgressGenerator progressGenerator;
    private ActionProcessButton btnSignIn;

	private int RequestCode = 1;
	private Validator validator;
	
	private EventPlannerSQLiteDB databaseHelper;
	private EventPlannerRepository dataRepository;
	
	GlobalVariables global = new GlobalVariables();
	
	private static final Style INFINITE = new Style.Builder().
		    setBackgroundColorValue(Style.holoBlueLight).build();
	private static final de.keyboardsurfer.android.widget.crouton.Configuration CONFIGURATION_INFINITE = new de.keyboardsurfer.android.widget.crouton.Configuration.Builder()
	          .setDuration(de.keyboardsurfer.android.widget.crouton.Configuration.DURATION_INFINITE)
	          .build();
	
	private void showBuiltInCrouton(final Style croutonStyle, String croutonText) {
	    showCrouton(croutonText, croutonStyle, de.keyboardsurfer.android.widget.crouton.Configuration.DEFAULT);
	 }
	
	private void showCrouton(String croutonText, Style croutonStyle, de.keyboardsurfer.android.widget.crouton.Configuration configuration) {
	    final boolean infinite = INFINITE == croutonStyle;
	    
	       
	    final Crouton crouton;
	      crouton = Crouton.makeText(this, croutonText, croutonStyle);
	    
	    if (infinite) {
	    }
	    crouton.setConfiguration((de.keyboardsurfer.android.widget.crouton.Configuration) (infinite ? CONFIGURATION_INFINITE : configuration)).show();
	}
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getSupportActionBar().show();
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.background_img4));
       
    	getSupportActionBar().setTitle("Event Planner");
        getSupportActionBar().setSubtitle("Managing Events");
        
        getSupportActionBar().setLogo(R.drawable.ic_launcher);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        
        databaseHelper = new EventPlannerSQLiteDB(this);
        dataRepository = new EventPlannerRepository(databaseHelper);
        
        
        editEmail = (EditText)findViewById(R.id.email);
        editPassword = (EditText)findViewById(R.id.password);
        
        //editEmail.setText("kgauledwaba@gmail.com");
        //editPassword.setText("P@ssword01");
        
        signUpTextView = (TextView) findViewById(R.id.signUpTextView);
        //loginButton = (Button) findViewById(R.id.email_sign_in_button);
        
        progressGenerator = new ProgressGenerator(this);
        btnSignIn = (ActionProcessButton) findViewById(R.id.btnSignIn);
        
        validator = new Validator(this);
        validator.setValidationListener(this);

        btnSignIn.setMode(ActionProcessButton.Mode.ENDLESS);
        
        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               validator.validate();
            }
        });
        
        signUpTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("LoginActivity", "Sign Up Activity activated.");
                // this is where you should start the signup Activity
                startActivityForResult(new Intent(LoginActivity.this, SignupActivity.class), RequestCode);
            }
        });
        
        
        TableSettings _TableSetting = dataRepository.GetSettings();
        
        if(_TableSetting == null){
        	_TableSetting = new TableSettings(0, 1, 1, 1, 1,"Please Find Quote Attached", "I have attached a quote as per our agreement. Should you have queries please do not hesitate to contact me.");
        	dataRepository.SaveOrUpdateSettings(_TableSetting);
        }
		
		 validator.put(editEmail, global.RequiredRule);
		 validator.put(editEmail, global._ValidateEmailAddress);
		 validator.put(editPassword, global.RequiredRule);
		     
		// startActivityForResult(new Intent(LoginActivity.this, DashboardActivity.class), RequestCode);
    }
    
	public double GetScreenSize(){
		DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        
        int widthPixels = metrics.widthPixels;
        int heightPixels = metrics.heightPixels;
        
        		
        float widthDpi = metrics.xdpi;
        float heightDpi = metrics.ydpi;
        
        float widthInches = widthPixels / widthDpi;
        float heightInches = heightPixels / heightDpi;
        
        double diagonalInches = Math.sqrt(
        	    (widthInches * widthInches) 
        	    + (heightInches * heightInches));
        
		return diagonalInches;
	}
	
	public boolean isOnline() {
        ConnectivityManager cm =
            (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        
        NetworkInfo i = cm.getActiveNetworkInfo();
        
        if (i == null)
          return false;
        
        if (!i.isConnected())
          return false;
        
        if (!i.isAvailable())
          return false;
        
        return true;
    }
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
    	if(item.getItemId() == android.R.id.home)
    	{
			finish();
    	}
    	
    	return false;
	}
	
	@Override
	public void onValidationSucceeded() {
		// TODO Auto-generated method stub
		GlobalVariables.ButtonAnimationControl = 10000;
		progressGenerator.start(btnSignIn);
        btnSignIn.setEnabled(false);
        editEmail.setEnabled(false);
        editPassword.setEnabled(false);
        
        String userName = editEmail.getText().toString();
        String password = editPassword.getText().toString();
        
        
        if(!isOnline()){
        	TableUsers _TableUser = dataRepository.signInOffile(userName, password);
        	if(_TableUser != null){
        		Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
        		Bundle bundle = new Bundle();
        		
        		GlobalVariables._LoggedUser = _TableUser;
        		
        		bundle.putSerializable("_TableUser", (Serializable) _TableUser);
        		intent.putExtras(bundle);
        		startActivityForResult(intent, RequestCode);
        	}else{
        		btnSignIn.setEnabled(true);
		        editEmail.setEnabled(true);
		        editPassword.setEnabled(true);

		        
		        showBuiltInCrouton(Style.ALERT, "No such user exist, please signup"); 
        	}
        }
        else{
			ParseUser.logInInBackground(userName, password,
				new LogInCallback() {
					public void done(ParseUser UserLogged, ParseException e) {
						GlobalVariables.ButtonAnimationControl = 0;
						
						if (UserLogged != null) {

							TableUsers _TableUser = dataRepository.signInOffile(editEmail.getText().toString(), editPassword.getText().toString());
							
							if(_TableUser == null){
								String cloundid = UserLogged.getObjectId();
								String fullname = UserLogged.getString("fullName"); 
								String lastname = UserLogged.getString("lastName"); 
								String mobilenumber = UserLogged.getString("mobileNumber");
								String email = editEmail.getText().toString();
								String password = editPassword.getText().toString();
								
								_TableUser = new TableUsers(cloundid, fullname, lastname, mobilenumber, email, password);
	
								dataRepository.SaveOrUpdateUser(_TableUser);
							}
							
							GlobalVariables._LoggedUser = _TableUser;
							
							Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
			        		Bundle bundle = new Bundle();
			        		
			        		bundle.putSerializable("_TableUser", (Serializable) _TableUser);
			        		intent.putExtras(bundle);
			        		startActivityForResult(intent, RequestCode);
							
						} else {
							btnSignIn.setEnabled(true);
					        editEmail.setEnabled(true);
					        editPassword.setEnabled(true);
					        
					        if(e == null)
					        	showBuiltInCrouton(Style.ALERT, "No such user exist, please signup");
					        else
					        	showBuiltInCrouton(Style.ALERT, "Internet connection is unstable.");
						}
					}
			});
        }
	}

	@Override
	public void onValidationFailed(View failedView, Rule<?> failedRule) {
		// TODO Auto-generated method stub
		String message = failedRule.getFailureMessage();

        if (failedView instanceof EditText) {
            failedView.requestFocus();
            ((EditText) failedView).setError(message);
        } else {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
	}

	@Override
	public void onComplete() {
		// TODO Auto-generated method stub
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		try{
			if(RequestCode == resultCode)
			{
				ParseUser user = ParseUser.getCurrentUser();
				if(user != null)
					ParseUser.logOut();
				
				finish();
			}
			else
			{
				ParseUser user = ParseUser.getCurrentUser();
				if(user != null)
					ParseUser.logOut();
				
				editEmail.setText(null);
				editPassword.setText(null);
				
				btnSignIn.setEnabled(true);
		        editEmail.setEnabled(true);
		        editPassword.setEnabled(true);
			}
		}catch(Exception Ex){
			Ex.printStackTrace();
		}
	}
}
