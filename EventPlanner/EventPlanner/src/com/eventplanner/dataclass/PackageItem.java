package com.eventplanner.dataclass;

import java.io.Serializable;

public class PackageItem implements Serializable{
	private static final long serialVersionUID = 1L;
	
	public String dbId;
	public String quantiy;
	public String packagename;
	public String price;
	public String subtotal;
	public boolean isNewitem = false;
	
	public PackageItem(String dbId, String quantiy, String packagename, String price, String subtotal, boolean isNewitem) {
		// TODO Auto-generated constructor stub
		this.dbId = dbId;
		this.quantiy = quantiy;
		this.packagename = packagename;
		this.subtotal = subtotal;
		this.price = price;
		this.isNewitem = isNewitem;
	}
}
