package com.eventplanner.dataclass;

import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.util.Log;
import android.widget.EditText;

import com.eventplanner.Mail;
import com.eventplanner.adapters.ClientListAdapter;
import com.eventplanner.adapters.EventListAdapter;
import com.eventplanner.adapters.EventTypeListAdapter;
import com.eventplanner.adapters.PackagesListAdapter;
import com.eventplanner.database.EventPlannerRepository;
import com.eventplanner.databasetables.TableClients;
import com.eventplanner.databasetables.TableEventTypes;
import com.eventplanner.databasetables.TableEvents;
import com.eventplanner.databasetables.TablePackages;
import com.eventplanner.databasetables.TableQuoteItem;
import com.eventplanner.databasetables.TableSettings;
import com.eventplanner.databasetables.TableUsers;
import com.mobsandgeeks.saripaar.Rule;

public class GlobalVariables {

	public static int ButtonAnimationControl;
	public static EventListAdapter _EventListAdapter;
	public static ClientListAdapter _ClientListAdapter;
	public static PackagesListAdapter adapterPackages;
	public static EventTypeListAdapter adapterEventType;
    
	public static TableUsers _LoggedUser;
	private Pattern pattern;
	private Matcher matcher;
 
	private static final String EMAIL_PATTERN = 
		"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	
	public static String BROADCAST_ACTION = "com.eventplanner";
	public Rule<EditText> RequiredRule;
	public Rule<EditText> _ValidatePhoneNumber;
	public Rule<EditText> _ValidateEmailAddress;
	public Rule<EditText> _ValidateGreaterThanZero;
	public Rule<EditText> _ValidatePasswordMatch;
	
	public GlobalVariables() {
		// TODO Auto-generated constructor stub
		 _ValidatePhoneNumber = new Rule<EditText>("Phone Number Must Be 10 Numeric Characters!") {

	            @Override
	            public boolean isValid(EditText domainNameEditText) {
	                String domainName = domainNameEditText.getText().toString();
	                
	                return ValidatePhoneNumber(domainName);
	            }
	        };
	        
	        RequiredRule = new Rule<EditText>("Required Field(s) Cannot Be Empty!") {

	            @Override
	            public boolean isValid(EditText domainNameEditText) {
	                String domainName = domainNameEditText.getText().toString();
	                
	                return ValidateRequire(domainName);
	            }
	        };
	        
	        _ValidateEmailAddress = new Rule<EditText>("Email Invalid, Please Rectify! E.g example@gmail.com") {

	            @Override
	            public boolean isValid(EditText domainNameEditText) {
	                String domainName = domainNameEditText.getText().toString();
	                
	                return _ValidateEmail(domainName);
	            }
	        };
	        
	        _ValidateGreaterThanZero = new Rule<EditText>("Numeric Value Of Greater Than Zero Expected!") {
				
				@Override
				public boolean isValid(EditText domainNameEditText) {
					String domainName = domainNameEditText.getText().toString();
					
					return ValidateGreaterThanZero(domainName);
				}
			};
	}
	
	public String _StripSpaces(String parameter)
    {
    	String validValue = "";

        if(parameter.length() > 0){
	    	for (int i = 0; i < parameter.length(); i++)
	        {
	            if (parameter.charAt(i) != ' ')
	                validValue = validValue + parameter.charAt(i);
	        }
    	}
        return validValue.toUpperCase(Locale.US);
    }
	
	public boolean ValidateRequire(String value)
	{
		boolean result = false;
		if(value != null && value.trim().length() > 0)
			result = true;
		else
			result = false;
		
		return result;
	}
	
	public boolean ValidateGreaterThanZero(String value)
	{
		boolean result = false;
		
		double _DoubleVal = 0.0;
		
		if(value != null && value.trim().length() > 0)
		{
			_DoubleVal = Double.parseDouble(value);
			if(_DoubleVal > 0)
				result = true;
			else 
				result = false;
		}
		else
		{
			result = false;
		}
		return result;
	}
	
	public boolean ValidatePhoneNumber(String value)
	{
		boolean result = false;
		if(value != null && value.trim().length() >= 10)
		{
			result = true;
		}
		if(value != null && (value.length() > 0 && value.length() < 10))
		{
			result = false;
		}
		else
		{
			result = true;
		}
		
		return result;
	}
	
	public boolean _ValidateEmail(final String _EmailString) {
		
		boolean _isValid = false;
		
		if(_EmailString == null || _EmailString.trim().length() <= 0)
			_isValid = true;
		else
		{
			pattern = Pattern.compile(EMAIL_PATTERN);
			matcher = pattern.matcher(_EmailString);
			_isValid = matcher.matches();
		}
		
		return _isValid;
	}
	
	public boolean ValidatePasswordMatch(final String _Password,final String _PasswordConfirm)
	{
		boolean result = false;
		if(_Password.equals(_PasswordConfirm))
			result = true;
		else
			result = false;
		
		return result;
	}
	
	public String SendGeneratedQuote(String fileName, TableEvents _Event, String message, String subject){
    	final String username = "kgau.appmail@gmail.com";
    	final String password = "1634kgauledwaba";

    	String result = "";
    	
    	boolean isEmailSent = false;
    	
    	Mail m = new Mail(username, password); 
    	 
        String[] toArr = {_Event.getClient().getEmail(), "kgauledwaba@gmail.com"}; 
        m.setTo(toArr);
        m.setFrom("kgau.appmail@gmail.com"); 
        
        if(subject.trim().length() > 0)
        	m.setSubject(subject);
        else
        	m.setSubject("Please Find Quote Attached"); 
        
        String messageBody = "";
        
        if(message.trim().length() <= 0){
	        messageBody = "Dear " + _Event.getClient().getLastName() + " \n\n";
	        messageBody += "I have attached a quote as per our agreement. Should you have queries please do not hesitate to contact me.\n\n";
        }else{
        	messageBody = "Dear " + _Event.getClient().getLastName() + " \n\n";
        	messageBody += message + "\n\n";
        }
        
        messageBody += "Regards\n";
        if(_LoggedUser != null)
        	messageBody += _LoggedUser.getLastName() + " " + _LoggedUser.getFullName().charAt(0);
        
        m.setBody(messageBody);
                
        try { 
        	m.addAttachment(fileName);
        	isEmailSent = m.send();
          
        } catch(Exception e) { 
          isEmailSent = false;
        	//Toast.makeText(MailApp.this, "There was a problem sending the email.", Toast.LENGTH_LONG).show(); 
          Log.e("EventPlanner", "Could not send email", e); 
        }
        
        if(isEmailSent)
        	result = "Sent";
        else
        	result = "Failed";
        
        return result;
    }

	public void UploadLocalDataToClound(TableSettings _Settings, EventPlannerRepository dataRepository){
		try
		{
		   ParseCloudQueryObject ParseSyncCloud = new ParseCloudQueryObject(dataRepository);
		   
		   List<TableClients> _TableClients = dataRepository.GetClientsToSync();
 		   List<TableEvents> _TableEvents = dataRepository.GetEventsToSync();
 		   List<TableEventTypes> _TableEventTypes = dataRepository.GetEventTypesToSync();
 		   List<TablePackages> _TablePackages = dataRepository.GetPackagesToSync();
 		   List<TableQuoteItem> _TableQuoteItem = dataRepository.GetQuoteItemsToSync();
 		   
		   if(_TablePackages != null)
			   ParseSyncCloud.SyncPackagesToCloud(_TablePackages);
		   
		   if(_TableClients != null)
		   		ParseSyncCloud.SyncClientsToCloud(_TableClients);
		   
		   if(_TableQuoteItem != null)
			   ParseSyncCloud.SyncQuoteItemsToCloud(_TableQuoteItem);
		   
		   if(_TableEventTypes != null)
			   ParseSyncCloud.SyncEventTypesToCloud(_TableEventTypes);
		   
		   if(_TableEvents != null)
			   ParseSyncCloud.SyncEventsToCloud(_TableEvents);
		   
		   _Settings.setLastSyncDateUpload(new Date());
   		   dataRepository.SaveOrUpdateSettings(_Settings);
		}
		catch(Exception Ex){
			Ex.printStackTrace();
		}
	}
}
