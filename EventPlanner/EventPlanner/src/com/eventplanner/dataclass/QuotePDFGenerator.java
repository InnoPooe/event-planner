package com.eventplanner.dataclass;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import com.eventplanner.databasetables.TableEvents;
import com.eventplanner.databasetables.TableQuoteItem;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class QuotePDFGenerator {

	private static String FILE;
	private static Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 25,
			Font.BOLD, BaseColor.BLUE);
	
	@SuppressWarnings("unused")
	private static Font redFont = new Font(Font.FontFamily.TIMES_ROMAN, 12,
			Font.NORMAL, BaseColor.RED);
	
	private static Font subFont = new Font(Font.FontFamily.TIMES_ROMAN, 16,
			Font.UNDERLINE, BaseColor.DARK_GRAY);
	
	private static Font smallOrangeBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
			Font.BOLD, BaseColor.BLUE);
	
	private static Font smallHeadingTextBold = new Font(Font.FontFamily.TIMES_ROMAN, 14,
			Font.BOLD, BaseColor.BLUE);
	
	private static TableEvents _Event;
	private static ArrayList<TableQuoteItem> quoteItems;
	
	public QuotePDFGenerator(String file) {
		FILE = file;
	}
	
	public QuotePDFGenerator(String file, TableEvents event,ArrayList<TableQuoteItem> items) {
		FILE = file;
		_Event = event;
		quoteItems = items;
	}
	
	public void GetGeneratedQuoteDocumentStream(){
		Document document = new Document();
		try {
			
			PdfWriter.getInstance(document, new FileOutputStream(FILE));
			
			document.open();
			addMetaData(document);
			addQuoteContent(document);
			document.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void GetGeneratedDocumentStream(){
		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream(FILE));
			
			document.open();
			addMetaData(document);
			addQuoteTitlePage(document);
			addQuoteContent(document);
			
			//document.
			
			document.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static void addMetaData(Document document) {
		document.addTitle("Quote Document");
		document.addSubject("Total cost for the event and packages selected.");
		document.addKeywords("Quote, Packages, Cost");
		document.addAuthor("Kgaugelo");
		document.addCreator("Ledwaba");
	}

	private static void addQuoteTitlePage(Document document)
			throws DocumentException {
		Paragraph preface = new Paragraph();
		Paragraph paragraph = new Paragraph("Quotation",catFont); 
		paragraph.setAlignment(Element.ALIGN_CENTER);
		
		preface.add(paragraph);
		document.add(preface);
	}

	@SuppressWarnings("unused")
	private static void addQuoteContent(Document document) throws DocumentException {
		SimpleDateFormat f = new SimpleDateFormat("dd MMM yyyy HH:mm", Locale.US);
		

		Paragraph paragraph = new Paragraph("Quotation",catFont); 
		paragraph.setAlignment(Element.ALIGN_CENTER);
		document.add(paragraph);
		//Chapter catPart = new Chapter(0);
		//catPart.setNumberDepth(0);
		//Section subCatPart = catPart.addSection(paragraph);
		document.add(new Paragraph(" "));
		document.add(new Paragraph(" "));
		
		paragraph = new Paragraph("Customer Information",subFont); 
		paragraph.setAlignment(Element.ALIGN_LEFT);
		//subCatPart = catPart.addSection(paragraph);

		document.add(paragraph);
		document.add(new Paragraph(" "));
		
		PdfPTable table = createQuoteHeadingTable(null);
		document.add(table);
		//addEmptyLine(new Paragraph(""), 2);
		
		paragraph = new Paragraph("Event Details", subFont);
		paragraph.setAlignment(Element.ALIGN_LEFT);
		//subCatPart = catPart.addSection(paragraph);

		document.add(paragraph);
		document.add(new Paragraph(" "));
		
		table = createEventDetailsTable(null);
		document.add(table);
		
		paragraph = new Paragraph("Packages", subFont); 
		paragraph.setAlignment(Element.ALIGN_LEFT);
		//subCatPart = catPart.addSection(paragraph);
		document.add(paragraph);
		document.add(new Paragraph(" "));
		
		table = createQuotePackagesTable(null);
		document.add(table);
		
		paragraph = new Paragraph("Planner Information", subFont);
		paragraph.setAlignment(Element.ALIGN_LEFT);
		//subCatPart = catPart.addSection(paragraph);
		document.add(paragraph);
		document.add(new Paragraph(" "));
		table = createPlannerTable(null);
		 
		document.add(table);
	}

	private static PdfPTable createQuoteHeadingTable(Section subCatPart)
			throws BadElementException {
		PdfPTable table = new PdfPTable(2);

		PdfPCell cell1 = new PdfPCell(new Phrase("Name"));
		PdfPCell cell2 = new PdfPCell(new Phrase(_Event.getClient().getLastName() + " " + _Event.getClient().getFullName()));
		
		//cell1.
		
		cell1.setBorder(Rectangle.NO_BORDER);
		cell2.setBorder(Rectangle.NO_BORDER);
		
		table.addCell(cell1);
		table.addCell(cell2);
		
		cell1 = new PdfPCell(new Phrase("Address"));
		cell2 = new PdfPCell(new Phrase(_Event.getClient().getAddress()));

		cell1.setBorder(Rectangle.NO_BORDER);
		cell2.setBorder(Rectangle.NO_BORDER);
		
		table.addCell(cell1);
		table.addCell(cell2);
		
		cell1 = new PdfPCell(new Phrase("Email"));
		cell2 = new PdfPCell(new Phrase(_Event.getClient().getEmail()));
		
		cell1.setBorder(Rectangle.NO_BORDER);
		cell2.setBorder(Rectangle.NO_BORDER);
		
		table.addCell(cell1);
		table.addCell(cell2); 	
		
		String tel = _Event.getClient().getTelNumber();
		String cell = _Event.getClient().getMobileNumber();
		String contacts = cell + ((tel != null && tel.length() > 0)? " | " + tel:"");
		
		cell1 = new PdfPCell(new Phrase("Contact Number(s)"));
		cell2 = new PdfPCell(new Phrase(contacts));
		
		cell1.setBorder(Rectangle.NO_BORDER);
		cell2.setBorder(Rectangle.NO_BORDER);
		
		table.addCell(cell1);
		table.addCell(cell2);
		
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		//subCatPart.add(table);

		return table;
	}

	private static PdfPTable createEventDetailsTable(Section subCatPart) {
		PdfPTable table = new PdfPTable(2);

		Locale locale = new Locale("en_ZA");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
		
		PdfPCell cell1 = new PdfPCell(new Phrase("Name"));
		PdfPCell cell2 = new PdfPCell(new Phrase(_Event.getDescription()));

		SimpleDateFormat f = new SimpleDateFormat("dd MMM yyyy HH:mm", Locale.US);
		
		cell1.setBorder(Rectangle.NO_BORDER);
		cell2.setBorder(Rectangle.NO_BORDER);
		
		table.addCell(cell1);
		table.addCell(cell2);
		
		cell1 = new PdfPCell(new Phrase("Type"));
		cell2 = new PdfPCell(new Phrase(_Event.getEventType().getDescription()));
		
		cell1.setBorder(Rectangle.NO_BORDER);
		cell2.setBorder(Rectangle.NO_BORDER);
		
		table.addCell(cell1);
		table.addCell(cell2);
		
		cell1 = new PdfPCell(new Phrase("Venue"));
		cell2 = new PdfPCell(new Phrase(_Event.getEventVenue()));
		
		cell1.setBorder(Rectangle.NO_BORDER);
		cell2.setBorder(Rectangle.NO_BORDER);
		
		table.addCell(cell1);
		table.addCell(cell2);
		
		cell1 = new PdfPCell(new Phrase("Date"));
		cell2 = new PdfPCell(new Phrase(f.format(_Event.getEventDate())));
		
		cell1.setBorder(Rectangle.NO_BORDER);
		cell2.setBorder(Rectangle.NO_BORDER);
		
		table.addCell(cell1);
		table.addCell(cell2);
		
		
		cell1 = new PdfPCell(new Phrase("Primary Cost"));
		cell2 = new PdfPCell(new Phrase(fmt.format(_Event.getPrimaryCost())));
		
		cell1.setBorder(Rectangle.NO_BORDER);
		cell2.setBorder(Rectangle.NO_BORDER);
		
		table.addCell(cell1);
		table.addCell(cell2);
		
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		//subCatPart.add(table);
		
		return table;
	}
	
	private static PdfPTable createQuotePackagesTable(Section subCatPart)
			throws BadElementException {
		PdfPTable table = new PdfPTable(4);
		
		Locale locale = new Locale("en_ZA");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
		
		PdfPCell c1 = new PdfPCell(new Phrase("Description", smallHeadingTextBold));
		c1.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Quantity", smallHeadingTextBold));
		c1.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(c1);

		c1 = new PdfPCell(new Phrase("Unit Price", smallHeadingTextBold));
		c1.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(c1);
		
		c1 = new PdfPCell(new Phrase("Total", smallHeadingTextBold));
		c1.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.addCell(c1);
		table.setHeaderRows(1);

		float totalPrice = _Event.getPrimaryCost();
		
		try{
			if(quoteItems != null){
				for(TableQuoteItem item: quoteItems){
					table.addCell(item.getPackage().getDescription());
					table.addCell(String.valueOf(item.getQuantity()));
					float price = item.getTotalPrice() / item.getQuantity();
					table.addCell(fmt.format(price));
					table.addCell(fmt.format(item.getTotalPrice()));
					totalPrice += item.getTotalPrice();
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		table.addCell("");
		table.addCell("");
		table.addCell(new Phrase("Total Cost", smallOrangeBold));
		table.addCell(new Phrase(fmt.format(totalPrice), smallOrangeBold));

		table.setHorizontalAlignment(Element.ALIGN_LEFT);
	//	subCatPart.add(table);

		return table;
	}
	
	private static PdfPTable createPlannerTable(Section subCatPart) {
		PdfPTable table = new PdfPTable(2);
		
		PdfPCell cell1 = new PdfPCell(new Phrase("Name"));
		PdfPCell cell2 = new PdfPCell(new Phrase(GlobalVariables._LoggedUser.getLastName() + " " + GlobalVariables._LoggedUser.getLastName()));
		
		cell1.setBorder(Rectangle.NO_BORDER);
		cell2.setBorder(Rectangle.NO_BORDER);
		
		table.addCell(cell1);
		table.addCell(cell2);
		
		cell1 = new PdfPCell(new Phrase("Contact Numbers"));
		cell2 = new PdfPCell(new Phrase(GlobalVariables._LoggedUser.getMobileNumber()));
		
		cell1.setBorder(Rectangle.NO_BORDER);
		cell2.setBorder(Rectangle.NO_BORDER);
		
		table.addCell(cell1);
		table.addCell(cell2);
		
		cell1 = new PdfPCell(new Phrase("Email"));
		cell2 = new PdfPCell(new Phrase(GlobalVariables._LoggedUser.getEmail()));
		
		cell1.setBorder(Rectangle.NO_BORDER);
		cell2.setBorder(Rectangle.NO_BORDER);
		
		table.addCell(cell1);
		table.addCell(cell2);
		
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		//subCatPart.add(table);
		
		return table;
	}

	@SuppressWarnings("unused")
	private static void addEmptyLine(Paragraph paragraph, int number) {
		for (int i = 0; i < number; i++) {
			paragraph.add(new Paragraph(" "));
		}
	}

}
