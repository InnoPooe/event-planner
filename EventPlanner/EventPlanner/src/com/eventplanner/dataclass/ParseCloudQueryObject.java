package com.eventplanner.dataclass;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.eventplanner.database.EventPlannerRepository;
import com.eventplanner.databasetables.TableClients;
import com.eventplanner.databasetables.TableEventTypes;
import com.eventplanner.databasetables.TableEvents;
import com.eventplanner.databasetables.TablePackages;
import com.eventplanner.databasetables.TableQuoteItem;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class ParseCloudQueryObject {
 
	private EventPlannerRepository dataRepository;
	
	public ParseCloudQueryObject() {
		// TODO Auto-generated constructor stub
	}

	public ParseCloudQueryObject(EventPlannerRepository dataRepository) {
		// TODO Auto-generated constructor stub
		this.dataRepository = dataRepository;
	}
	
	public boolean SyncCloundDataToDevice(String UserId){
		boolean passed = false;
		/*Client Section*/
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Client");
		query.whereEqualTo("UserId", UserId);
		
		List<ParseObject> clients = null;
		try {
			clients = query.find();
			passed = true;
		} catch (ParseException e) {
			passed = false;
			e.printStackTrace();
		}
		
		if(clients != null){
			for(ParseObject client: clients){
				String ClientId = client.getString("ClientId");
				String Fullname = client.getString("Fullname");
				String Lastname = client.getString("Lastname");
				String MobileNumber = client.getString("MobileNumber");
				String TelNumber = client.getString("TelNumber");
				String Email = client.getString("Email");
				String Address = client.getString("Address");
				Date DateAdded = client.getDate("DateAdded");
				boolean IsActive = client.getBoolean("IsActive");
				
				int isactive = IsActive? 1: 0;
				
				TableClients _Client = new TableClients(ClientId, Fullname,
						Lastname, MobileNumber, 
						TelNumber, Email, 
						Address, DateAdded, isactive, 0);
				
				dataRepository.SaveOrUpdateClient(_Client);
			}
		}
		
		query = ParseQuery.getQuery("Packages");
		query.whereEqualTo("UserId", UserId);
		
		List<ParseObject> results = null;
		try {
			results = query.find();
			passed = true;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			passed = false;
			e.printStackTrace();
		}
		
		if(results != null){
			for(ParseObject result: results){
				String PackageId = result.getString("PackageId");
				String Description = result.getString("Description");
				float Price = (float) result.getDouble("Price");
				boolean IsActive = result.getBoolean("IsActive");
				
				int isactive = IsActive?1:0;
				
				TablePackages _Package = new TablePackages(PackageId, Description, 
						Price, isactive, 0);
				
				dataRepository.SaveOrUpdatePackage(_Package);
			}
		}
		
		query = ParseQuery.getQuery("EventType");
		query.whereEqualTo("UserId", UserId);
		
		results = null;
		try {
			results = query.find();
			passed = true;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			passed = false;
			e.printStackTrace();
		}
		
		if(results != null){
			for(ParseObject result: results){
				String EventTypeId = result.getString("EventTypeId");
				String Description = result.getString("Description");
				float Price = (float) result.getDouble("Price");
				boolean IsActive = result.getBoolean("IsActive");
				
				int isactive = IsActive?1:0;
				
				TableEventTypes _EventType = new TableEventTypes(EventTypeId, Description, 
						Price, isactive, 0);
				
				dataRepository.SaveOrUpdateEventType(_EventType);
			}
		}
		
		query = ParseQuery.getQuery("Event");
		query.whereEqualTo("UserId", UserId);
		
		results = null;
		try {
			results = query.find();
			passed = true;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			passed = false;
			e.printStackTrace();
		}
		
		if(results != null){
			for(ParseObject result: results){
				String Comments = result.getString("Comments");
				String EventTypeId = result.getString("EventTypeId");
				String EventId = result.getString("EventId");
				String ClientId = result.getString("ClientId");
				String EventVenue = result.getString("EventVenue");
				float PrimaryCost = (float) result.getDouble("PrimaryCost");
				Date EventDate = result.getDate("EventDate");
				boolean IsCancelled = result.getBoolean("IsCancelled");
				boolean IsPaymentSettled = result.getBoolean("IsPaymentSettled");
				String Description = result.getString("Description");
				
				int ispaymentsettled = IsPaymentSettled?1:0;
				int iscancelled = IsCancelled?1:0;
				
				TableClients _Client = dataRepository.GetClient(ClientId);
				TableEventTypes _EventType = dataRepository.GetEventType(EventTypeId);
				
				SimpleDateFormat formater = new SimpleDateFormat("yyyy/MM/dd HH:mm", Locale.US);
				
				TableEvents _Event = new TableEvents(EventId, Description, _EventType,
					_Client, formater.format(EventDate), PrimaryCost, 
						EventVenue, ispaymentsettled, iscancelled ,Comments, 0);
				
				dataRepository.SaveOrUpdateEvent(_Event);
			}
		}
		
		query = ParseQuery.getQuery("QuoteItem");
		query.whereEqualTo("UserId", UserId);
		
		results = null;
		try {
			results = query.find();
			passed = true;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			passed = false;
			e.printStackTrace();
		}
		
		if(results != null){
			for(ParseObject result: results){
				String QuoteItemId = result.getString("QuoteItemId");
				String PackageId = result.getString("PackageId");
				String EventId = result.getString("EventId");
				Date DateCreated = result.getDate("DateCreated");
				Date DateUpdated = result.getDate("DateUpdated");
				int Quantity = result.getInt("Quantity");
				float TotalPrice = (float) result.getDouble("TotalPrice");
				
				TableEvents _TableEvent = dataRepository.GetEvent(EventId);
				TablePackages _TablePackage = dataRepository.GetPackage(PackageId);
				
				TableQuoteItem _QuoteItem = new TableQuoteItem(QuoteItemId, _TablePackage, _TableEvent,
						Quantity, TotalPrice, DateCreated, DateUpdated, 0);
				
				dataRepository.SaveOrUpdateQuoteItem(_QuoteItem);
			}
		}
		
		return passed;
	}
	
	public void SyncClientsToCloud(List<TableClients> data){
		for(TableClients obj: data){
			ParseQuery<ParseObject> query = ParseQuery.getQuery("Client");
			query.whereEqualTo("ClientId", obj.getID());
			
			try {
				List<ParseObject> results = query.find();
				if(results == null || results.size() < 1){
					ParseObject quoteItem = new ParseObject("Client");
					quoteItem.put("Fullname", obj.getFullName());
					quoteItem.put("Lastname", obj.getLastName());
					quoteItem.put("MobileNumber", obj.getMobileNumber());
					quoteItem.put("TelNumber", obj.getTelNumber());
					quoteItem.put("Email", obj.getEmail());
					quoteItem.put("ClientId", obj.getID());
					quoteItem.put("Address", obj.getAddress());
					quoteItem.put("DateAdded", obj.getDateAdded());
					quoteItem.put("IsActive", obj.getIsActive() == 1?true:false);
					quoteItem.put("UserId", GlobalVariables._LoggedUser.getCloudID());
					quoteItem.saveInBackground();
				}
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void SyncEventsToCloud(List<TableEvents> data){
		for(TableEvents obj: data){
			ParseQuery<ParseObject> query = ParseQuery.getQuery("Event");
			query.whereEqualTo("EventId", obj.getID());
			
			try {
				List<ParseObject> results = query.find();
				if(results == null || results.size() < 1){
					ParseObject quoteItem = new ParseObject("Event");
					quoteItem.put("Description", obj.getDescription());
					quoteItem.put("EventVenue", obj.getEventVenue());
					quoteItem.put("Comments", obj.getComments());
					quoteItem.put("EventDate", obj.getEventDate());
					quoteItem.put("EventId", obj.getID());
					quoteItem.put("ClientId", obj.getClient().getID());
					quoteItem.put("EventTypeId", obj.getEventType().getID());
					quoteItem.put("IsCancelled", obj.getIsCancelled() == 1?true:false);
					quoteItem.put("IsPaymentSettled", obj.getIsPaymentSettled() == 1?true:false);
					quoteItem.put("UserId", GlobalVariables._LoggedUser.getCloudID());
					quoteItem.put("PrimaryCost", obj.getPrimaryCost());
					quoteItem.saveInBackground();
				}
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void SyncEventTypesToCloud(List<TableEventTypes> data){
		for(TableEventTypes obj: data){
			ParseQuery<ParseObject> query = ParseQuery.getQuery("EventType");
			query.whereEqualTo("EventTypeId", obj.getID());
			
			try {
				List<ParseObject> results = query.find();
				if(results == null || results.size() < 1){
					ParseObject quoteItem = new ParseObject("EventType");
					quoteItem.put("Description", obj.getDescription());
					quoteItem.put("EventTypeId", obj.getID());
					quoteItem.put("IsActive", obj.getIsActive() == 1?true:false);
					quoteItem.put("UserId", GlobalVariables._LoggedUser.getCloudID());
					quoteItem.put("Price", obj.getPrice());
					quoteItem.saveInBackground();
				}
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
	public void SyncPackagesToCloud(List<TablePackages> data){
		for(TablePackages obj: data){
			ParseQuery<ParseObject> query = ParseQuery.getQuery("Packages");
			query.whereEqualTo("PackageId", obj.getID());
			
			try {
				List<ParseObject> results = query.find();
				if(results == null || results.size() < 1){
					ParseObject quoteItem = new ParseObject("Packages");
					quoteItem.put("Description", obj.getDescription());
					quoteItem.put("PackageId", obj.getID());
					quoteItem.put("IsActive", obj.getIsActive() == 1?true:false);
					quoteItem.put("UserId", GlobalVariables._LoggedUser.getCloudID());
					quoteItem.put("Price", obj.getPrice());
					quoteItem.saveInBackground();
				}
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
	public void SyncQuoteItemsToCloud(List<TableQuoteItem> data){
		for(TableQuoteItem obj: data){
			ParseQuery<ParseObject> query = ParseQuery.getQuery("QuoteItem");
			query.whereEqualTo("QuoteItemId", obj.getID());
			
			try {
				List<ParseObject> results = query.find();
				
				if(results == null || results.size() < 1){
					ParseObject quoteItem = new ParseObject("QuoteItem");
					quoteItem.put("EventId", obj.getEvent().getID());
					quoteItem.put("PackageId", obj.getPackage().getID());
					quoteItem.put("Quantity", obj.getQuantity());
					quoteItem.put("UserId", GlobalVariables._LoggedUser.getCloudID());
					quoteItem.put("DateCreated", obj.getDateCreated());
					quoteItem.put("TotalPrice", obj.getTotalPrice());
					quoteItem.put("QuoteItemId", obj.getID());
					quoteItem.saveInBackground();
				}
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
}
