package com.eventplanner.dataclass;

import java.io.Serializable;
import com.eventplanner.databasetables.TableClients;
import com.eventplanner.databasetables.TableEventTypes;

public class EventInfo implements Serializable{
	private static final long serialVersionUID = 1L;

	public String dbId;
	
	public String description;
	
	public TableEventTypes _TableEventType;
	
	public TableClients _TableClient;
	
	public float primary_cost;
	
	public String event_date;
	
	public String event_venue;
	
	public boolean ispaymentsettled; 
	
	public String comments;
	
	public EventInfo(){
		
	}
	
	public EventInfo(String dbId, String description, TableEventTypes _TableEventType, TableClients _TableClient,
			float primary_cost, String event_date, String event_venue, boolean ispaymentsettled, String comments) {
		// TODO Auto-generated constructor stub
		this.dbId = dbId;
		this.event_date = event_date;
        this.description = description;
        this.comments = comments;
        this._TableClient = _TableClient;
        this._TableEventType = _TableEventType;
        this.event_venue = event_venue;
        this.ispaymentsettled = ispaymentsettled;
        this.primary_cost = primary_cost;
	}
	
	public EventInfo(String description, TableEventTypes _TableEventType, TableClients _TableClient,
			float primary_cost, String event_date, String event_venue, boolean ispaymentsettled, String comments) {
		// TODO Auto-generated constructor stub
		this.event_date = event_date;
        this.description = description;
        this.comments = comments;
        this._TableClient = _TableClient;
        this._TableEventType = _TableEventType;
        this.event_venue = event_venue;
        this.ispaymentsettled = ispaymentsettled;
        this.primary_cost = primary_cost;
	}

}
