package com.eventplanner.database;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import android.util.Log;

import com.eventplanner.databasetables.TableClients;
import com.eventplanner.databasetables.TableEventTypes;
import com.eventplanner.databasetables.TableEvents;
import com.eventplanner.databasetables.TablePackages;
import com.eventplanner.databasetables.TableQuoteItem;
import com.eventplanner.databasetables.TableSettings;
import com.eventplanner.databasetables.TableUsers;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

public class EventPlannerRepository {

	private Dao<TableUsers, String> _TableUsersDao = null;
	private Dao<TableClients, String> _TableClientsDao = null;
	private Dao<TableEventTypes, String> _TableEventTypesDao = null;
	private Dao<TablePackages, String> _TablePackagesDao = null;
	private Dao<TableQuoteItem, String> _TableQuoteItemDao = null;
	private Dao<TableEvents, String> _TableEventsDao = null;
	private Dao<TableSettings, String> _TableSettingsDao = null;
	
	public EventPlannerRepository(final EventPlannerSQLiteDB databaseHelper) {
		// TODO Auto-generated constructor stub
		
		this._TableUsersDao = getTableUsersDao(databaseHelper);
		this._TableClientsDao = getTableClientsDao(databaseHelper);
		this._TableEventTypesDao = getTableEventTypesDao(databaseHelper);
		this._TablePackagesDao = getTablePackagesDao(databaseHelper);
		this._TableQuoteItemDao = getTableQuoteItemDao(databaseHelper);
		this._TableEventsDao = getTableEventsDao(databaseHelper);
		this._TableSettingsDao = getTableSettingsDao(databaseHelper);
	}

	private Dao<TableSettings, String> getTableSettingsDao(final EventPlannerSQLiteDB databaseHelper) {
		if (this._TableSettingsDao == null) {
			try {
				this._TableSettingsDao = databaseHelper.getTableSettingsDao();
			}
			catch (final SQLException e) {
				Log.e(EventPlannerRepository.class.getName(), "Unable to load DAO: " + e.getMessage());
				e.printStackTrace();
			}
		}
		
		return this._TableSettingsDao;
	}
	
	private Dao<TableUsers, String> getTableUsersDao(final EventPlannerSQLiteDB databaseHelper) {
		if (this._TableUsersDao == null) {
			try {
				this._TableUsersDao = databaseHelper.getTableUsersDao();
			}
			catch (final SQLException e) {
				Log.e(EventPlannerRepository.class.getName(), "Unable to load DAO: " + e.getMessage());
				e.printStackTrace();
			}
		}
		
		return this._TableUsersDao;
	}
	
	private Dao<TableClients, String> getTableClientsDao(final EventPlannerSQLiteDB databaseHelper) {
		if (this._TableClientsDao == null) {
			try {
				this._TableClientsDao = databaseHelper.getTableClientsDao();
			}
			catch (final SQLException e) {
				Log.e(EventPlannerRepository.class.getName(), "Unable to load DAO: " + e.getMessage());
				e.printStackTrace();
			}
		}
		
		return this._TableClientsDao;
	}
	
	private Dao<TableEventTypes, String> getTableEventTypesDao(final EventPlannerSQLiteDB databaseHelper) {
		if (this._TableEventTypesDao == null) {
			try {
				this._TableEventTypesDao = databaseHelper.getTableEventTypesDao();
			}
			catch (final SQLException e) {
				Log.e(EventPlannerRepository.class.getName(), "Unable to load DAO: " + e.getMessage());
				e.printStackTrace();
			}
		}
		
		return this._TableEventTypesDao;
	}
	
	private Dao<TablePackages, String> getTablePackagesDao(final EventPlannerSQLiteDB databaseHelper) {
		if (this._TablePackagesDao == null) {
			try {
				this._TablePackagesDao = databaseHelper.getTablePackagesDao();
			}
			catch (final SQLException e) {
				Log.e(EventPlannerRepository.class.getName(), "Unable to load DAO: " + e.getMessage());
				e.printStackTrace();
			}
		}
		
		return this._TablePackagesDao;
	}
	
	private Dao<TableEvents, String> getTableEventsDao(final EventPlannerSQLiteDB databaseHelper) {
		if (this._TableEventsDao == null) {
			try {
				this._TableEventsDao = databaseHelper.getTableEventsDao();
			}
			catch (final SQLException e) {
				Log.e(EventPlannerRepository.class.getName(), "Unable to load DAO: " + e.getMessage());
				e.printStackTrace();
			}
		}
		
		return this._TableEventsDao;
	}
	
	private Dao<TableQuoteItem, String> getTableQuoteItemDao(final EventPlannerSQLiteDB databaseHelper) {
		if (this._TableQuoteItemDao == null) {
			try {
				this._TableQuoteItemDao = databaseHelper.getTableQuoteItemDao();
			}
			catch (final SQLException e) {
				Log.e(EventPlannerRepository.class.getName(), "Unable to load DAO: " + e.getMessage());
				e.printStackTrace();
			}
		}
		
		return this._TableQuoteItemDao;
	}
	
	public void DeleteEvent(String Id)
	{
		try {
			_TableEventsDao.deleteById(Id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void DeleteClient(String Id)
	{
		try {
			_TableClientsDao.deleteById(Id);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void DeleteQuoteItems(List<TableQuoteItem> items)
	{
		try {
			_TableQuoteItemDao.delete(items);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public boolean SaveOrUpdateUser(TableUsers _TableUsers){
		boolean isCommited = false;
		try 
		{
			long before = 0;
			long after = 0;
			
			before = _TableUsersDao.countOf();
			this._TableUsersDao.createOrUpdate(_TableUsers);
			after = _TableUsersDao.countOf();
			
			if(after > before)
				isCommited = true;
		}
		catch (final SQLException e) {
			e.printStackTrace();
		}
		
		return isCommited;
	}
	
	public boolean SaveOrUpdateClient(TableClients _TableClients){
		boolean isCommited = false;
		try 
		{
			long before = 0;
			long after = 0;
			
			before = _TableClientsDao.countOf();
			this._TableClientsDao.createOrUpdate(_TableClients);
			after = _TableClientsDao.countOf();
			
			if(after > before)
				isCommited = true;
		}
		catch (final SQLException e) {
			e.printStackTrace();
		}
		
		return isCommited;
	}
	
	public boolean SaveOrUpdateEventType(TableEventTypes _TableEventTypes){
		boolean isCommited = false;
		try 
		{
			long before = 0;
			long after = 0;
			
			before = _TableEventTypesDao.countOf();
			this._TableEventTypesDao.createOrUpdate(_TableEventTypes);
			after = _TableEventTypesDao.countOf();
			
			if(after > before)
				isCommited = true;
		}
		catch (final SQLException e) {
			e.printStackTrace();
		}
		
		return isCommited;
	}
	
	public boolean SaveOrUpdatePackage(TablePackages _TablePackages){
		boolean isCommited = false;
		try 
		{
			long before = 0;
			long after = 0;
			
			before = _TablePackagesDao.countOf();
			this._TablePackagesDao.createOrUpdate(_TablePackages);
			after = _TablePackagesDao.countOf();
			
			if(after > before)
				isCommited = true;
		}
		catch (final SQLException e) {
			e.printStackTrace();
		}
		
		return isCommited;
	}
	
	@SuppressWarnings("unused")
	public boolean SaveOrUpdateEvent(TableEvents _TableEvents){
		boolean isCommited = false;
		String id = null;
		try 
		{
			long before = 0;
			long after = 0;
			
			before = _TableEventsDao.countOf();
			this._TableEventsDao.createOrUpdate(_TableEvents);
			after = _TableEventsDao.countOf();
			
			if(after > before){
				isCommited = true;
				id = _TableEvents.getID();
			}
		}
		catch (final SQLException e) {
			e.printStackTrace();
		}
		
		return isCommited;
	}
	
	public boolean SaveOrUpdateQuoteItem(TableQuoteItem _TableQuoteItem){
		boolean isCommited = false;
		try 
		{
			long before = 0;
			long after = 0;
			
			before = _TableQuoteItemDao.countOf();
			this._TableQuoteItemDao.createOrUpdate(_TableQuoteItem);
			after = _TableQuoteItemDao.countOf();
			
			if(after > before)
				isCommited = true;
		}
		catch (final SQLException e) {
			e.printStackTrace();
		}
		
		return isCommited;
	}
	
	
	public boolean SaveOrUpdateSettings(TableSettings _TableSetting){
		boolean isCommited = true;
		try 
		{
			this._TableSettingsDao.createOrUpdate(_TableSetting);
			isCommited = true;
		}
		catch (final SQLException e) {
			isCommited = false;
			e.printStackTrace();
		}
		
		return isCommited;
	}
	
	public TableUsers signInOffile(String username, String password)
	{
		TableUsers _TableUser = null;
		
		try 
		{
			QueryBuilder<TableUsers, String> _Qb = _TableUsersDao.queryBuilder();
			Where<TableUsers, String> whereClause = _Qb.where();
			
			//String raw = "email = '" + username + "' and password = ' " + password + "'";
			
			whereClause.eq("email", username).and().eq("password", password);
			_Qb.setWhere(whereClause);
		
			_TableUser = _Qb.queryForFirst();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return _TableUser;
	}
	
	public TableUsers GetUser()
	{
		List<TableUsers> _TableUsers = null;
		TableUsers _User = null;
		
		try 
		{
			QueryBuilder<TableUsers, String> _Qb = _TableUsersDao.queryBuilder();
			_TableUsers = _Qb.query();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(_TableUsers != null && _TableUsers.size() > 0)
			_User = _TableUsers.get(0);
		
		return _User;
	}
	
	public List<TableEventTypes> GetAllEventTypes()
	{
		List<TableEventTypes> results = null;
		
		try 
		{
			QueryBuilder<TableEventTypes, String> _Qb = _TableEventTypesDao.queryBuilder();
			Where<TableEventTypes, String> whereClause = _Qb.where();
			
			whereClause.eq("isactive", 1);
			_Qb.setWhere(whereClause);
		
			results = _Qb.query();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return results;
	}
	
	public List<TablePackages> GetAllPackages()
	{
		List<TablePackages> results = null;
		
		try 
		{
			QueryBuilder<TablePackages, String> _Qb = _TablePackagesDao.queryBuilder();
			Where<TablePackages, String> whereClause = _Qb.where();
			
			whereClause.eq("isactive", 1);
			_Qb.setWhere(whereClause);
		
			results = _Qb.query();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return results;
	}
	
	public List<TableClients> GetAllClients()
	{
		List<TableClients> results = null;
		
		try 
		{
			QueryBuilder<TableClients, String> _Qb = _TableClientsDao.queryBuilder();
			Where<TableClients, String> whereClause = _Qb.where();
			
			whereClause.eq("isactive", 1);
			_Qb.setWhere(whereClause);
		
			results = _Qb.query();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return results;
	}
	
	public List<TableEvents> GetAllEvents()
	{
		List<TableEvents> results = null;
		
		try 
		{
			QueryBuilder<TableEvents, String> _Qb = _TableEventsDao.queryBuilder();
			//Where<TableEvents, String> whereClause = _Qb.where();
			
			//Date today = new Date();
			
			//whereClause.ge("event_date", today);//  eq("isactive", 1);
			//_Qb.setWhere(whereClause);
		
			results = _Qb.query();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return results;
	}
	
	public List<TableEvents> GetAllActiveEvents()
	{
		List<TableEvents> results = null;
		
		try 
		{
			QueryBuilder<TableEvents, String> _Qb = _TableEventsDao.queryBuilder();
			Where<TableEvents, String> whereClause = _Qb.where();
			
			Date today = new Date();
			
			whereClause.ge("event_date", today).and().eq("iscancelled", 0);
			_Qb.setWhere(whereClause);
		
			results = _Qb.query();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return results;
	}
	
	public List<TableEvents> GetAllEventsByClientID(String Id)
	{
		List<TableEvents> results = null;
		
		try 
		{
			QueryBuilder<TableEvents, String> _Qb = _TableEventsDao.queryBuilder();
			Where<TableEvents, String> whereClause = _Qb.where();
			
			whereClause.eq("fk_client_id", Id);
			_Qb.setWhere(whereClause);
		
			results = _Qb.query();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return results;
	}
	
	public List<TableQuoteItem> GetQuoteItemByEventID(String id)
	{
		List<TableQuoteItem> _TableQuoteItems = null;
		
		try 
		{
			QueryBuilder<TableQuoteItem, String> _Qb = _TableQuoteItemDao.queryBuilder();
			Where<TableQuoteItem, String> whereClause = _Qb.where();
			
			whereClause.eq("fk_event_id", id);
			_Qb.setWhere(whereClause);
		
			_TableQuoteItems = _Qb.query();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return _TableQuoteItems;
	}
	
	public TableQuoteItem GetQuoteItemByIDs(String eventId, String eventPackageId)
	{
		TableQuoteItem _TableQuoteItem = null;
		
		try 
		{
			QueryBuilder<TableQuoteItem, String> _Qb = _TableQuoteItemDao.queryBuilder();
			Where<TableQuoteItem, String> whereClause = _Qb.where();
			
			whereClause.eq("fk_event_id", eventId).and().eq("fk_package_id", eventPackageId);
			_Qb.setWhere(whereClause);
		
			_TableQuoteItem = _Qb.queryForFirst();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return _TableQuoteItem;
	}
	
	public TablePackages GetPackage(String id)
	{
		TablePackages _TablePackages = null;
		
		try 
		{
			QueryBuilder<TablePackages, String> _Qb = _TablePackagesDao.queryBuilder();
			Where<TablePackages, String> whereClause = _Qb.where();
			
			whereClause.eq("id", id);
			_Qb.setWhere(whereClause);
		
			_TablePackages = _Qb.queryForFirst();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return _TablePackages;
	}
	
	public TableEventTypes GetEventType(String id)
	{
		TableEventTypes _TableEventTypes = null;
		
		try 
		{
			QueryBuilder<TableEventTypes, String> _Qb = _TableEventTypesDao.queryBuilder();
			Where<TableEventTypes, String> whereClause = _Qb.where();
			
			whereClause.eq("id", id);
			_Qb.setWhere(whereClause);
		
			_TableEventTypes = _Qb.queryForFirst();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return _TableEventTypes;
	}
	
	public TableClients GetClient(String id)
	{
		TableClients _TableClients = null;
		
		try 
		{
			QueryBuilder<TableClients, String> _Qb = _TableClientsDao.queryBuilder();
			Where<TableClients, String> whereClause = _Qb.where();
			
			whereClause.eq("id", id);
			_Qb.setWhere(whereClause);
		
			_TableClients = _Qb.queryForFirst();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return _TableClients;
	}
	
	public TableEvents GetEvent(String id)
	{
		TableEvents _TableEvents = null;
		
		try 
		{
			QueryBuilder<TableEvents, String> _Qb = _TableEventsDao.queryBuilder();
			Where<TableEvents, String> whereClause = _Qb.where();
			
			whereClause.eq("id", id);
			_Qb.setWhere(whereClause);
		
			_TableEvents = _Qb.queryForFirst();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return _TableEvents;
	}
	
	public TableSettings GetSettings()
	{
		List<TableSettings> _TableSettings = null;
		TableSettings _TableSetting = null;
		
		try 
		{
			QueryBuilder<TableSettings, String> _Qb = _TableSettingsDao.queryBuilder();
			_TableSettings = _Qb.query();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(_TableSettings != null && _TableSettings.size()>0)
			_TableSetting = _TableSettings.get(0);
		else
			_TableSetting = null;
		
		return _TableSetting;
	}
	
	public List<TableEvents> GetEventsToSync()
	{
		List<TableEvents> _TableEvents = null;
		
		try 
		{
			QueryBuilder<TableEvents, String> _Qb = _TableEventsDao.queryBuilder();
			Where<TableEvents, String> whereClause = _Qb.where();
			
			whereClause.eq("pendingsync", 1);
			_Qb.setWhere(whereClause);
		
			_TableEvents = _Qb.query();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return _TableEvents;
	}
	
	public List<TableEventTypes> GetEventTypesToSync()
	{
		List<TableEventTypes> _TableEventTypes = null;
		
		try 
		{
			QueryBuilder<TableEventTypes, String> _Qb = _TableEventTypesDao.queryBuilder();
			Where<TableEventTypes, String> whereClause = _Qb.where();
			
			whereClause.eq("pendingsync", 1);
			_Qb.setWhere(whereClause);
		
			_TableEventTypes = _Qb.query();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return _TableEventTypes;
	}
	
	public List<TablePackages> GetPackagesToSync()
	{
		List<TablePackages> _TablePackages = null;
		
		try 
		{
			QueryBuilder<TablePackages, String> _Qb = _TablePackagesDao.queryBuilder();
			Where<TablePackages, String> whereClause = _Qb.where();
			
			whereClause.eq("pendingsync", 1);
			_Qb.setWhere(whereClause);
		
			_TablePackages = _Qb.query();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return _TablePackages;
	}
	
	public List<TableClients> GetClientsToSync()
	{
		List<TableClients> _TableClients = null;
		
		try 
		{
			QueryBuilder<TableClients, String> _Qb = _TableClientsDao.queryBuilder();
			Where<TableClients, String> whereClause = _Qb.where();
			
			whereClause.eq("pendingsync", 1);
			_Qb.setWhere(whereClause);
		
			_TableClients = _Qb.query();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return _TableClients;
	}
	
	public List<TableQuoteItem> GetQuoteItemsToSync()
	{
		List<TableQuoteItem> _TableQuoteItem = null;
		
		try 
		{
			QueryBuilder<TableQuoteItem, String> _Qb = _TableQuoteItemDao.queryBuilder();
			Where<TableQuoteItem, String> whereClause = _Qb.where();
			
			whereClause.eq("pendingsync", 1);
			_Qb.setWhere(whereClause);
		
			_TableQuoteItem = _Qb.query();
		} 
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return _TableQuoteItem;
	}
}
