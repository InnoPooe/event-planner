package com.eventplanner.database;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;

import com.eventplanner.databasetables.TableClients;
import com.eventplanner.databasetables.TableEventTypes;
import com.eventplanner.databasetables.TableEvents;
import com.eventplanner.databasetables.TablePackages;
import com.eventplanner.databasetables.TableQuoteItem;
import com.eventplanner.databasetables.TableSettings;
import com.eventplanner.databasetables.TableUsers;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class EventPlannerSQLiteDB extends OrmLiteSqliteOpenHelper{

	private static final int DATABASE_VERSION = 2;
	 
    // Database Name
    private static final String DATABASE_NAME = "EventPlannerDB.db";
    public static Context context;
    
    SQLiteDatabase db;
    ConnectivityManager cm;

    private Dao<TableUsers, String> _TableUsersDao = null;
	private Dao<TableClients, String> _TableClientsDao = null;
	private Dao<TableEventTypes, String> _TableEventTypesDao = null;
	private Dao<TablePackages, String> _TablePackagesDao = null;
	private Dao<TableQuoteItem, String> _TableQuoteItemDao = null;
	private Dao<TableEvents, String> _TableEventsDao = null;
	private Dao<TableSettings, String> _TableSettingsDao = null;
	
	@SuppressWarnings("static-access")
	public EventPlannerSQLiteDB(Context  context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
		this.context = context;
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
		// TODO Auto-generated method stub
		try {
			TableUtils.createTableIfNotExists(connectionSource, TableUsers.class);
			TableUtils.createTableIfNotExists(connectionSource, TableEventTypes.class);
			TableUtils.createTableIfNotExists(connectionSource, TablePackages.class);
			TableUtils.createTableIfNotExists(connectionSource, TableClients.class);
			TableUtils.createTableIfNotExists(connectionSource, TableEvents.class);
			TableUtils.createTableIfNotExists(connectionSource, TableQuoteItem.class);
			TableUtils.createTableIfNotExists(connectionSource, TableSettings.class);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion,
			int newVersion) {
		// TODO Auto-generated method stub
		
		try {
			TableUtils.dropTable(connectionSource, TableUsers.class, true);
			TableUtils.dropTable(connectionSource, TableEventTypes.class, true);
			TableUtils.dropTable(connectionSource, TablePackages.class, true);
			TableUtils.dropTable(connectionSource, TableClients.class, true);
			TableUtils.dropTable(connectionSource, TableEvents.class, true);
			TableUtils.dropTable(connectionSource, TableQuoteItem.class, true);
			TableUtils.dropTable(connectionSource, TableSettings.class, true);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public Dao<TableSettings, String> getTableSettingsDao() throws SQLException {
		if (this._TableSettingsDao == null) {
			this._TableSettingsDao = getDao(TableSettings.class);
		}
		return this._TableSettingsDao;
	}
	
	public Dao<TableClients, String> getTableClientsDao() throws SQLException {
		if (this._TableClientsDao == null) {
			this._TableClientsDao = getDao(TableClients.class);
		}
		return this._TableClientsDao;
	}
	
	public Dao<TableUsers, String> getTableUsersDao() throws SQLException {
		if (this._TableUsersDao == null) {
			this._TableUsersDao = getDao(TableUsers.class);
		}
		return this._TableUsersDao;
	}
	
	public Dao<TableEventTypes, String> getTableEventTypesDao() throws SQLException {
		if (this._TableEventTypesDao == null) {
			this._TableEventTypesDao = getDao(TableEventTypes.class);
		}
		return this._TableEventTypesDao;
	}
	
	public Dao<TablePackages, String> getTablePackagesDao() throws SQLException {
		if (this._TablePackagesDao == null) {
			this._TablePackagesDao = getDao(TablePackages.class);
		}
		return this._TablePackagesDao;
	}
	
	public Dao<TableEvents, String> getTableEventsDao() throws SQLException {
		if (this._TableEventsDao == null) {
			this._TableEventsDao = getDao(TableEvents.class);
		}
		return this._TableEventsDao;
	}
	
	public Dao<TableQuoteItem, String> getTableQuoteItemDao() throws SQLException {
		if (this._TableQuoteItemDao == null) {
			this._TableQuoteItemDao = getDao(TableQuoteItem.class);
		}
		return this._TableQuoteItemDao;
	}
	
	@Override
	public void close() {
		super.close();
		this._TableClientsDao = null;
		this._TableEventsDao = null;
		this._TablePackagesDao = null;
		this._TableEventTypesDao = null;
		this._TableQuoteItemDao = null;
		this._TableUsersDao = null;
		this._TableSettingsDao = null;
	}

}
