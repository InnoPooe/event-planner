package com.eventplanner;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseUser;

public class ParseApplication extends Application {
	 
    @Override
    public void onCreate() {
        super.onCreate();
 
        // Add your initialization code here
        
        String APP_ID =  getResources().getString(R.string.Parse_APP_ID);
        String CLIENT_ID =  getResources().getString(R.string.Parse_CLIENT_ID);
        
        Parse.initialize(this, APP_ID, CLIENT_ID);
 
        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();
 
        // If you would like all objects to be private by default, remove this
        // line.
        defaultACL.setPublicReadAccess(true);
 
        ParseACL.setDefaultACL(defaultACL, true);
    }
 
}
