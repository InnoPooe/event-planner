package com.eventplanner;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.ViewAnimator;

import cn.pedant.SweetAlert.SweetAlertDialog;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.dd.processbutton.iml.ActionProcessButton;
import com.eventplanner.database.EventPlannerRepository;
import com.eventplanner.database.EventPlannerSQLiteDB;
import com.eventplanner.databasetables.TableClients;
import com.eventplanner.dataclass.GlobalVariables;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.tekle.oss.android.animation.AnimationFactory;
import com.tekle.oss.android.animation.AnimationFactory.FlipDirection;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class ClientFormActivity extends SherlockFragmentActivity implements ValidationListener{
	private EditText editEmail;
    private EditText editAddress;
    private EditText editFullname;
    private EditText editLastname;
    private EditText editMobileNo;
    private EditText editTelNo;
    private ActionProcessButton btnSaveDetails;
    
    private boolean IsEditMode = false;
    private boolean FieldsEditable = true;
    private TableClients _TableClient;
    private TableClients _SelectedClient;
	private Validator validator;
    
    private EventPlannerSQLiteDB databaseHelper;
	private EventPlannerRepository dataRepository;
	
	GlobalVariables global = new GlobalVariables();
	
	private static final Style INFINITE = new Style.Builder().
		    setBackgroundColorValue(Style.holoBlueLight).build();
	
	private static final Style INFINITEERROR = new Style.Builder().
		    setBackgroundColorValue(Style.holoRedLight).build();
	
	private static final de.keyboardsurfer.android.widget.crouton.Configuration CONFIGURATION_INFINITE = new de.keyboardsurfer.android.widget.crouton.Configuration.Builder()
	          .setDuration(de.keyboardsurfer.android.widget.crouton.Configuration.DURATION_INFINITE)
	          .build();
	
	private void showBuiltInCrouton(final Style croutonStyle, String croutonText) {
	    showCrouton(croutonText, croutonStyle, de.keyboardsurfer.android.widget.crouton.Configuration.DEFAULT);
	 }
	
	private void showCrouton(String croutonText, Style croutonStyle, de.keyboardsurfer.android.widget.crouton.Configuration configuration) {
	    final boolean infinite = INFINITE == croutonStyle || INFINITEERROR == croutonStyle;
	    
	       
	    final Crouton crouton;
	      crouton = Crouton.makeText(this, croutonText, croutonStyle);
	    
	    if (infinite) {
	    }
	    crouton.setConfiguration((de.keyboardsurfer.android.widget.crouton.Configuration) (infinite ? CONFIGURATION_INFINITE : configuration)).show();
	}
	
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.clientinfo_layout);
        getSupportActionBar().show();
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.background_img4));
        
      
    	getSupportActionBar().setTitle("Event Planner");
        getSupportActionBar().setSubtitle("Managing Events");
        
        getSupportActionBar().setLogo(R.drawable.ic_launcher);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        
        databaseHelper = new EventPlannerSQLiteDB(this);
        dataRepository = new EventPlannerRepository(databaseHelper);
        
        validator = new Validator(this);
        validator.setValidationListener(this);
        
        editEmail = (EditText)findViewById(R.id.email);
        editAddress = (EditText)findViewById(R.id.address);
        editFullname = (EditText)findViewById(R.id.fullname);
        editTelNo = (EditText)findViewById(R.id.telno);
        editLastname = (EditText)findViewById(R.id.lastname);
        editMobileNo = (EditText)findViewById(R.id.mobileno);
        btnSaveDetails = (ActionProcessButton) findViewById(R.id.btnSave);
        
        btnSaveDetails.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				validator.validate();
			}
		});
        
        validator.put(editFullname, global.RequiredRule);
		validator.put(editLastname, global.RequiredRule);
		validator.put(editEmail, global._ValidateEmailAddress);
		validator.put(editMobileNo, global.RequiredRule);
		validator.put(editAddress, global.RequiredRule);
		
		if(savedInstanceState == null){
			if(getIntent().getExtras() != null){
				Bundle bundle = getIntent().getExtras();
				IsEditMode = bundle.getBoolean("IsEditMode");
				_TableClient = (TableClients) bundle.getSerializable("_TableClient");
				
				_SelectedClient = _TableClient;
				
				if(!IsEditMode)
					FieldsEditable = false;
			}
		}
		else{
			_TableClient = (TableClients) savedInstanceState.getSerializable("_TableClient");
			IsEditMode = savedInstanceState.getBoolean("IsEditMode");
			FieldsEditable = savedInstanceState.getBoolean("FieldsEditable");
			_SelectedClient = (TableClients) savedInstanceState.getSerializable("_SelectedClient");
		}
		
		if(_TableClient != null){
			editEmail.setText(_TableClient.getEmail());
	        editAddress.setText(_TableClient.getAddress());
	        editFullname.setText(_TableClient.getFullName());
	        editTelNo.setText(_TableClient.getTelNumber());
	        editLastname.setText(_TableClient.getLastName());
	        editMobileNo.setText(_TableClient.getMobileNumber());
		}
		
		if(!FieldsEditable){
			DisableAllControls();
		}
		
    	final ViewAnimator viewAnimator = (ViewAnimator)findViewById(R.id.viewFlipper);
    	AnimationFactory.flipTransition(viewAnimator, FlipDirection.LEFT_RIGHT);
	}
	
	public void PopulateClientInformation(){
		
	}
	
	public void DisableAllControls()
	{
		LinearLayout layout = (LinearLayout)findViewById(R.id.client_form);
		
		for(int i = 0; i < layout.getChildCount();i++)
		{
			if((layout.getChildAt(i) instanceof EditText) ||
					(layout.getChildAt(i) instanceof RadioButton) ||
					(layout.getChildAt(i) instanceof Spinner)||
					(layout.getChildAt(i) instanceof ActionProcessButton)||
					(layout.getChildAt(i) instanceof Button))
			{
				layout.getChildAt(i).setEnabled(false);
			}
		}
	}
	
	@Override
	public void onSaveInstanceState(Bundle state) {
	    super.onSaveInstanceState(state);
	
	    state.putBoolean("IsEditMode", IsEditMode);
	    state.putSerializable("_TableClient", _TableClient);
	    state.putBoolean("FieldsEditable", FieldsEditable);
	    state.putSerializable("_SelectedClient", _SelectedClient);
	}
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
		
		try
		{
		}
		catch(Exception Ex)
		{
			Ex.printStackTrace();
		}
		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
    	if(item.getItemId() == android.R.id.home)
    	{
    		setResult(2);
			finish();
    	}
    	
    	return false;
	}
	
	@Override
    public void onBackPressed() {
		setResult(2);
		finish();
	}
	
	@Override
	public void onValidationSucceeded() {
		// TODO Auto-generated method stub
		new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
        .setTitleText("Are you sure?")
        .setContentText("Client details will be saved to you device.")
        .setCancelText("No!")
        .setConfirmText("Yes!")
        .showCancelButton(true)
        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                // reuse previous dialog instance, keep widget user state, reset them if you need
                sDialog.setTitleText("Oops!")
                        .setContentText("Operation cancelled.")
                        .setConfirmText("OK")
                        .showCancelButton(false)
                        .setCancelClickListener(null)
                        .setConfirmClickListener(null)
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
            }
        })
        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
            	
            	if(IsEditMode){
            		_TableClient = new TableClients(_SelectedClient.getID(), editFullname.getText().toString(), editLastname.getText().toString(),
							editMobileNo.getText().toString(), editTelNo.getText().toString(), editEmail.getText().toString(), 
							editAddress.getText().toString(), _SelectedClient.getDateAdded(), _SelectedClient.getIsActive(), 1);
	                
	                dataRepository.SaveOrUpdateClient(_TableClient);
	                
	              // sDialog.setco
	                
	                
	                sDialog.setTitleText("Sweet!")
                    .setContentText("Client details successfuly updated.")
                    .setConfirmText("OK")
                    .showCancelButton(false)
                    .setCancelClickListener(null)
                    .setConfirmClickListener(null)
                    .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
            	}else{
	                _TableClient = new TableClients(editFullname.getText().toString(), editLastname.getText().toString(),
							editMobileNo.getText().toString(), editTelNo.getText().toString(), 
							editEmail.getText().toString(), editAddress.getText().toString());
	                
	                boolean saved = dataRepository.SaveOrUpdateClient(_TableClient);
					if(saved){
						sDialog.setTitleText("Sweet!")
	                     .setContentText("Client details successfuly saved.")
	                     .setConfirmText("OK")
	                     .showCancelButton(false)
	                     .setCancelClickListener(null)
	                     .setConfirmClickListener(null)
	                     .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
						 
						editEmail.setText(null);
				        editAddress.setText(null);
				        editFullname.setText(null);
				        editTelNo.setText(null);
				        editLastname.setText(null);
				        editMobileNo.setText(null);
				        
				        editFullname.requestFocus();
					}else{
						
						showBuiltInCrouton(INFINITEERROR, "Unidentified Error Has Occured, Client Details Not Saved.");
					}
            	}
            	
				
            }
        }).show();
	}

	@Override
	public void onValidationFailed(View failedView, Rule<?> failedRule) {
		// TODO Auto-generated method stub
		String message = failedRule.getFailureMessage();

        if (failedView instanceof EditText) {
            failedView.requestFocus();
            ((EditText) failedView).setError(message);
        } else {
            Toast.makeText(DashboardActivity._DashboardContext, message, Toast.LENGTH_SHORT).show();
        }
	}

}
