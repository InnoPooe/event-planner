package com.eventplanner.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Switch;

import cn.pedant.SweetAlert.SweetAlertDialog;

import com.actionbarsherlock.app.SherlockFragment;
import com.dd.processbutton.iml.ActionProcessButton;
import com.eventplanner.DashboardActivity;
import com.eventplanner.R;
import com.eventplanner.database.EventPlannerRepository;
import com.eventplanner.database.EventPlannerSQLiteDB;
import com.eventplanner.databasetables.TableSettings;

public class ManageSettingsFragment extends SherlockFragment {
	private static final String ARG_SECTION_NUMBER = "section_number";
	
	private EventPlannerSQLiteDB databaseHelper;
	private EventPlannerRepository dataRepository;
	
	private TableSettings _TableSetting;
	
	private Switch enableAutoSync, enableRingtone, enableVibrate, enableReminders; 
	private ActionProcessButton btnSaveChanges, btnRestoreDefault;
	private EditText editSubject, editMessage;
	
	public static EventFragment newInstance(int sectionNumber) {
		EventFragment fragment = new EventFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public ManageSettingsFragment() {
    	
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.settings_layout, container, false);
        
        databaseHelper = new EventPlannerSQLiteDB(DashboardActivity._DashboardContext);
        dataRepository = new EventPlannerRepository(databaseHelper);
        
        
        
        enableAutoSync = (Switch) rootView.findViewById(R.id.switchAutoSync);
        enableRingtone = (Switch) rootView.findViewById(R.id.switchPlaySound);
        enableVibrate = (Switch) rootView.findViewById(R.id.switchVibrate);
        enableReminders = (Switch) rootView.findViewById(R.id.switchEnableReminders);
        
        editSubject = (EditText) rootView.findViewById(R.id.editEmailSubject);
        editMessage = (EditText) rootView.findViewById(R.id.editEmailBody);
        
        btnRestoreDefault = (ActionProcessButton) rootView.findViewById(R.id.btnRestoreDefault);
        btnSaveChanges = (ActionProcessButton) rootView.findViewById(R.id.btnSaveChanges);
        
        _TableSetting = dataRepository.GetSettings();
        
        if(_TableSetting == null){
        	_TableSetting = new TableSettings(0, 1, 1, 1, 1, "Please Find Quote Attached", "I have attached a quote as per our agreement. Should you have queries please do not hesitate to contact me.");
        	dataRepository.SaveOrUpdateSettings(_TableSetting);
        	
        	RestoreDefault();
        	
        }else{
        	boolean syncEnabled = _TableSetting.getEnableAutoSync() == 1?true:false;
        	boolean reminderEnabled = _TableSetting.getEnableReminders() == 1?true:false;
        	boolean toneEnabled = _TableSetting.getPlayToneOnReminder() == 1?true:false;
        	boolean vibrateEnabled = _TableSetting.getVibrateOnReminder() == 1?true:false;
        	
        	String message = _TableSetting.getMessage();
        	String subject = _TableSetting.getSubject();
        	
        	enableAutoSync.setChecked(syncEnabled);
        	enableReminders.setChecked(reminderEnabled);
        	enableRingtone.setChecked(toneEnabled);
        	enableVibrate.setChecked(vibrateEnabled);
        	
        	editMessage.setText(message);
        	editSubject.setText(subject);
        }
        
        enableAutoSync.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				int checked = isChecked?1:0;
				_TableSetting.setEnableAutoSync(checked);
			}
		});
        
        enableReminders.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				int checked = isChecked?1:0;
				_TableSetting.setEnableReminders(checked);
				
				enableVibrate.setEnabled(isChecked);
				enableRingtone.setEnabled(isChecked);
			}
		});
        
        
        enableVibrate.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				int checked = isChecked?1:0;
				_TableSetting.setVibrateOnReminder(checked);
			}
		});
        
        enableRingtone.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				// TODO Auto-generated method stub
				int checked = isChecked?1:0;
				_TableSetting.setPlayToneOnReminder(checked);
			}
		});

        btnSaveChanges.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				new SweetAlertDialog(DashboardActivity._DashboardContext, SweetAlertDialog.WARNING_TYPE)
		        .setTitleText("Are you sure?")
		        .setContentText("Settings will be updated on your device.")
		        .setCancelText("No!")
		        .setConfirmText("Yes!")
		        .showCancelButton(true)
		        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
		            @Override
		            public void onClick(SweetAlertDialog sDialog) {
		                // reuse previous dialog instance, keep widget user state, reset them if you need
		                sDialog.setTitleText("Oops!")
		                        .setContentText("Operation cancelled.")
		                        .setConfirmText("OK")
		                        .showCancelButton(false)
		                        .setCancelClickListener(null)
		                        .setConfirmClickListener(null)
		                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
		            }
		        })
		        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
		            @Override
		            public void onClick(SweetAlertDialog sDialog) {

		            	_TableSetting.setSubject(editSubject.getText().toString());
		            	_TableSetting.setMessage(editMessage.getText().toString());
		            	
						dataRepository.SaveOrUpdateSettings(_TableSetting);
						
		                sDialog.setTitleText("Sweet!")
	                    .setContentText("Settings successfuly updated.")
	                    .setConfirmText("OK")
	                    .showCancelButton(false)
	                    .setCancelClickListener(null)
	                    .setConfirmClickListener(null)
	                    .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
		            }
		        }).show();
			}
		});
        
        btnRestoreDefault.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				RestoreDefault();
				dataRepository.SaveOrUpdateSettings(_TableSetting);
			}
		});
        
		setHasOptionsMenu(true);
		
        return rootView;
    }
    
    public void RestoreDefault(){
    	enableAutoSync.setChecked(true);
    	enableReminders.setChecked(true);
    	enableRingtone.setChecked(true);
    	enableVibrate.setChecked(true);
    }
}
