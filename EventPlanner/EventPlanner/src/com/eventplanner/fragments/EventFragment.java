package com.eventplanner.fragments;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.andexert.expandablelayout.library.ExpandableLayout;
import com.andexert.expandablelayout.library.ExpandableLayoutListView;
import com.dd.processbutton.iml.ActionProcessButton;
import com.eventplanner.DashboardActivity;
import com.eventplanner.EventFormActivity;
import com.eventplanner.ProgressGenerator;
import com.eventplanner.R;
import com.eventplanner.adapters.EventListAdapter;
import com.eventplanner.adapters.ManageEventsAdapter_oldcode;
import com.eventplanner.adapters.ManageEventsAdapter;
import com.eventplanner.database.EventPlannerRepository;
import com.eventplanner.database.EventPlannerSQLiteDB;
import com.eventplanner.databasetables.TableSettings;
import com.eventplanner.dataclass.GlobalVariables;
import com.eventplanner.dataclass.ParseCloudQueryObject;
import com.tekle.oss.android.animation.AnimationFactory;
import com.tekle.oss.android.animation.AnimationFactory.FlipDirection;
import com.viewpagerindicator.PageIndicator;
import com.viewpagerindicator.TitlePageIndicator;
import com.viewpagerindicator.TitlePageIndicator.IndicatorStyle;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;


@SuppressWarnings("unused")
public class EventFragment extends SherlockFragment implements OnPageChangeListener{

	private static final String ARG_SECTION_NUMBER = "section_number";
	//private ViewFlow viewFlow;
	private ListView listView;
	
	private ProgressGenerator progressGenerator;
	private ActionProcessButton btnSignIn;
	    
	public boolean isEventTabActive = true;
	private boolean isEventsSelected = false;
	
	private LinearLayout ParentViewEvents;
	private View childView;
	private LayoutInflater _LayoutInflater;
	private ManageEventsAdapter_oldcode adapter;
	private int currentViewPos = 0;
	
    private View headerView = null;
	
    ManageEventsAdapter mAdapter;
    ViewPager mPager;
    PageIndicator mIndicator;
    TitlePageIndicator indicator;
    
    private EventPlannerSQLiteDB databaseHelper;
	private EventPlannerRepository dataRepository;
	
    private Intent _ParentIntent;
    
	public static EventFragment newInstance(int sectionNumber) {
		EventFragment fragment = new EventFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public EventFragment() {
    	
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.simple_titles, container, false);
        
        _LayoutInflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        
        databaseHelper = new EventPlannerSQLiteDB(DashboardActivity._DashboardContext);
	    dataRepository = new EventPlannerRepository(databaseHelper);
	       
        mPager = (ViewPager)rootView.findViewById(R.id.pager);
        indicator = (TitlePageIndicator)rootView.findViewById(R.id.indicator);
        
        mAdapter = new ManageEventsAdapter(getChildFragmentManager());
        mPager.setAdapter(mAdapter);

        indicator.setViewPager(mPager);
        indicator.setFooterIndicatorStyle(IndicatorStyle.Triangle);
        mIndicator = indicator;
        
        mIndicator.setOnPageChangeListener(this);
        
		setHasOptionsMenu(true);
		
        return rootView;
    }
    
    @Override
	public void onSaveInstanceState(Bundle state) {
	   // if(mPager != null)
	  //  	mPager.setAdapter(null);
	    
	    state.putInt("Dummy", 0);
	    super.onSaveInstanceState(state);
	}
    
    public void setViewPager(){
    	
    } 
    
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
    	super.onViewCreated(view, savedInstanceState);
    	
    	//View selectedView = viewFlow.getSelectedView();
    	//currentViewPos = viewFlow.getSelectedItemPosition();
    }

    @Override
	public void onConfigurationChanged(Configuration newConfig) {
    	super.onConfigurationChanged(newConfig);  
    	
    	//if(mPager != null)
		//    mPager.setAdapter(null);
	}
    
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		
		try
		{
			if(isEventTabActive){
				menu.add("New Event").setNumericShortcut('A')
				.setIcon(R.drawable.icon_calendar)
	    		.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
			}
			
			menu.add("Syncronize").setNumericShortcut('S')
			.setIcon(R.drawable.refresh_icon)
    		.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		}
		catch(Exception Ex)
		{
			Ex.printStackTrace();
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getNumericShortcut() == 'S'){
			try{
				getSherlockActivity().setProgressBarIndeterminateVisibility(true);
				
				ParseCloudQueryObject ParseSyncCloud = new ParseCloudQueryObject(dataRepository);
				TableSettings _Settings = dataRepository.GetSettings();
				
	 		    ParseSyncCloud.SyncCloundDataToDevice(GlobalVariables._LoggedUser.getCloudID());
			   _Settings.setLastSyncDateDownload(new Date());
	   		    dataRepository.SaveOrUpdateSettings(_Settings);
	   		    
	   		    getSherlockActivity().setProgressBarIndeterminateVisibility(false);
	   		    
	   		    GlobalVariables global = new GlobalVariables();
	   		    global.UploadLocalDataToClound(_Settings, dataRepository);
			}
			catch(Exception Ex){
				Ex.printStackTrace();
			}
		}else{
			ShowCaptureDetailsActivity();
		}
		
		getSherlockActivity().invalidateOptionsMenu();
		
		return false;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		Log.e("EventFra", "onResume");
	}

	@Override
	public void onPause() {
		super.onPause();
		Log.e("EventFra", "onPause");
	}
		
	public void ShowCaptureDetailsActivity()
    {
		
		switch (currentViewPos) {
		case 0:
			Intent _Intent = new Intent(getActivity(), EventFormActivity.class);
			startActivityForResult(_Intent, 1);
			break;
		case 1:
			
			break;
		case 3:
			
			break;
		}
		
    	//final ViewAnimator viewAnimator = (ViewAnimator)childView.findViewById(R.id.viewFlipper);
    	//AnimationFactory.flipTransition(viewAnimator, FlipDirection.LEFT_RIGHT);	
    }
	
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
    }

	@Override
	public void onPageScrollStateChanged(int arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onPageSelected(int pos) {
		// TODO Auto-generated method stub
		isEventTabActive = pos == 0;
		getSherlockActivity().invalidateOptionsMenu();
	}
    
	private void SendBroadCastToRefreshEvents()
	{
		_ParentIntent = new Intent(GlobalVariables.BROADCAST_ACTION);
		_ParentIntent.putExtra("Refresh", "Refresh");
		DashboardActivity._DashboardContext.sendBroadcast(_ParentIntent);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch(resultCode){
			case 2:
				try{
					//SendBroadCastToRefreshEvents();
					//Toast.makeText(DashboardActivity._DashboardContext, "onActivityResult", Toast.LENGTH_SHORT).show();
					mAdapter = new ManageEventsAdapter(getChildFragmentManager());
			        mPager.setAdapter(mAdapter);

			        indicator.setViewPager(mPager);
			        indicator.setFooterIndicatorStyle(IndicatorStyle.Triangle);
			        mIndicator = indicator;
			        
			        mIndicator.setOnPageChangeListener(this);
			        
			       // mAdapter.getItem(0);
			        
			        if(GlobalVariables._EventListAdapter != null)
			        	GlobalVariables._EventListAdapter.notifyDataSetChanged();
			        
				}catch(Exception Ex){
					
				}
			break;
		}
	}
}
