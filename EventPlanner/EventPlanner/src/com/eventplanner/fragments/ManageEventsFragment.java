package com.eventplanner.fragments;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import net.londatiga.android.ActionItem;
import net.londatiga.android.QuickAction;

import cn.pedant.SweetAlert.SweetAlertDialog;

import com.actionbarsherlock.app.SherlockFragment;
import com.andexert.expandablelayout.library.ExpandableLayout;
import com.andexert.expandablelayout.library.ExpandableLayoutListView;
import com.eventplanner.DashboardActivity;
import com.eventplanner.EventFormActivity;
import com.eventplanner.EventViewEditActivity;
import com.eventplanner.R;
import com.eventplanner.adapters.EventListAdapter;
import com.eventplanner.adapters.EventTypeListAdapter;
import com.eventplanner.adapters.PackagesListAdapter;
import com.eventplanner.database.EventPlannerRepository;
import com.eventplanner.database.EventPlannerSQLiteDB;
import com.eventplanner.databasetables.TableEventTypes;
import com.eventplanner.databasetables.TableEvents;
import com.eventplanner.databasetables.TablePackages;
import com.eventplanner.databasetables.TableQuoteItem;
import com.eventplanner.databasetables.TableSettings;
import com.eventplanner.dataclass.GlobalVariables;
import com.eventplanner.dataclass.QuotePDFGenerator;
import de.keyboardsurfer.android.widget.crouton.Configuration;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public final class ManageEventsFragment extends SherlockFragment {
    private static final String KEY_CONTENT = "ManageEventsFragment:Content";
    
    private int position = 0;
    private int currentView = R.layout.events_layout;

	SimpleDateFormat formater = new SimpleDateFormat("dd MMM yy mmss", Locale.US);
	

	private LayoutInflater _LayoutInflater;
    private ExpandableLayoutListView expandableLayoutListView;
    private ExpandableLayout expandableLayout;
    
    
    private Button btnSaveNew, btnUpdate;
    private EditText edtDescription;
    private EditText edtPrice;
    private CheckBox chkIsActive;
    private EventPlannerSQLiteDB databaseHelper;
    private EventPlannerRepository dataRepository;
    

	private List<TablePackages> _TablePackages;
	private TablePackages _SelectedPackage = null;
	
	private List<TableEventTypes> _TableEventTypes;
	private TableEventTypes _SelectedEventType = null;
	
	private List<TableEvents> _TableEvents = null;
	
	private TableSettings _TableSetting = null;
	
	private ListView _ListViewEvents;
	private TextView noEvents;
	
	private int mSelectedRow;
	
	private static final int ID_DETAILS = 1;
	private static final int ID_EDIT= 2;
	private static final int ID_SENDQUOTE = 3;
	private static final int ID_DELETE = 4;
	
	private GlobalVariables global = null;
	
    private static final Style INFINITE = new Style.Builder().
		    setBackgroundColorValue(Style.holoBlueLight).build();
    
    private static final Style INFINITEERROR = new Style.Builder().
		    setBackgroundColorValue(Style.holoRedLight).build();
    
	private static final Configuration CONFIGURATION_INFINITE = new Configuration.Builder()
	          .setDuration(Configuration.DURATION_LONG)
	          .build();
	
	private void showBuiltInCrouton(final Style croutonStyle, String croutonText) {
	    showCrouton(croutonText, croutonStyle, Configuration.DEFAULT);
	}
	
	private void showCrouton(String croutonText, Style croutonStyle, Configuration configuration) {
	    final boolean infinite = INFINITE == croutonStyle || INFINITEERROR == croutonStyle;
	    
	       
	    final Crouton crouton;
	      crouton = Crouton.makeText(getActivity(), croutonText, croutonStyle);
	    
	    if (infinite) {
	    }
	    crouton.setConfiguration(infinite ? CONFIGURATION_INFINITE : configuration).show();
	}
	
    public static ManageEventsFragment newInstance(int position) {
        ManageEventsFragment fragment = new ManageEventsFragment();
        fragment.position = position;
        switch (position) {
		case 0:
			fragment.currentView = R.layout.events_layout;
			break;
		case 1:
			fragment.currentView = R.layout.packages_layout;
			break;
		default:
			fragment.currentView = R.layout.packages_layout;
			break;
		}

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT)) {
            savedInstanceState.getString(KEY_CONTENT);
        }
    }

    @SuppressLint("InflateParams")
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	View rootView = inflater.inflate(currentView, container, false);
    	
    	global = new GlobalVariables();
    	
    	databaseHelper = new EventPlannerSQLiteDB(DashboardActivity._DashboardContext);
    	dataRepository = new EventPlannerRepository(databaseHelper);
    	
    	_TableSetting = dataRepository.GetSettings();
    	
    	Log.e("Position", String.valueOf(position));
    	
    	switch (position) {
    	case 0:

    		ActionItem addItem 		= new ActionItem(ID_EDIT, "Edit", getResources().getDrawable(R.drawable.edit));
    		ActionItem acceptItem 	= new ActionItem(ID_DETAILS, "Details", getResources().getDrawable(R.drawable.info));
            ActionItem uploadItem 	= new ActionItem(ID_SENDQUOTE, "Send Quote", getResources().getDrawable(R.drawable.envelope));
            ActionItem deleteItem 	= new ActionItem(ID_DELETE, "Delete", getResources().getDrawable(R.drawable.trash));
            
            final QuickAction mQuickAction 	= new QuickAction(DashboardActivity._DashboardContext);
    		
    		mQuickAction.addActionItem(addItem);
    		mQuickAction.addActionItem(deleteItem);
    		mQuickAction.addActionItem(acceptItem);
    		mQuickAction.addActionItem(uploadItem);
    		
    		mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
    			@Override
    			public void onItemClick(QuickAction quickAction, int pos, int actionId) {
    				//ActionItem actionItem = quickAction.getActionItem(pos);
    				switch (actionId) {
					case ID_SENDQUOTE:

				        _LayoutInflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			        	final View convertView = _LayoutInflater.inflate(R.layout.email_messagebody_layout, null);
			        	
	    		        
						new SweetAlertDialog(DashboardActivity._DashboardContext, SweetAlertDialog.WARNING_TYPE)
	    		        .setTitleText("Are you sure?")
	    		        .setContentText("Quote will be sent to Client (" + _TableEvents.get(mSelectedRow).getClient().getEmail() +") linked to this event!")
	    		        .setCancelText("No!")
	    		        .setConfirmText("Yes!")
	    		        .showCancelButton(true)
	    		        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
	    		            @Override
	    		            public void onClick(SweetAlertDialog sDialog) {
	    		                // reuse previous dialog instance, keep widget user state, reset them if you need
	    		                sDialog.setTitleText("Oops!")
	    		                        .setContentText("Operation cancelled.")
	    		                        .setConfirmText("OK")
	    		                        .showCancelButton(false)
	    		                        .setCancelClickListener(null)
	    		                        .setConfirmClickListener(null)
	    		                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
	    		            }
	    		        })
	    		        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
	    		            @SuppressLint("SdCardPath")
							@Override
	    		            public void onClick(SweetAlertDialog sDialog) {
	    		            	try{
	    		            		sDialog.dismissWithAnimation();
	    		            		
	    		            		final Builder dialog = new AlertDialog.Builder(DashboardActivity._DashboardContext);
	    		        			dialog.setView(convertView);
	    		        			dialog.setCancelable(false);
	    		        			dialog.setTitle("Custom Message");
	    		         
	    		        			// set the custom dialog components - text, image and button
	    		        			final EditText subject = (EditText) convertView.findViewById(R.id.textSubject);
	    		        			final EditText message = (EditText) convertView.findViewById(R.id.textMessageBody);
	    		         
	    		        			subject.setText(_TableSetting.getSubject());
	    		        			message.setText(_TableSetting.getMessage());
	    		        			
	    		        			dialog.setNegativeButton("Skip", new DialogInterface.OnClickListener() {
										
										@Override
										public void onClick(DialogInterface dialog, int which) {
											// TODO Auto-generated method stub
											TableEvents _Event = _TableEvents.get(mSelectedRow);
				    		            	
			    		            		if(_Event != null){
				    		                	Date t = new Date();
			    		            			String clientName = _Event.getClient().getLastName();
			    		            			
			    		            			ArrayList<TableQuoteItem> items = (ArrayList<TableQuoteItem>) dataRepository.GetQuoteItemByEventID(_Event.getID());
			    		            			
			    		            			if(getSherlockActivity() != null)
			    		            				getSherlockActivity().setProgressBarIndeterminateVisibility(true);
			    		            			
				    		                	String fileName = "/sdcard/" + clientName  + " " + formater.format(t) + ".pdf";
				    		            		QuotePDFGenerator _Generator = new QuotePDFGenerator(fileName,_Event, items);
				    		            		_Generator.GetGeneratedQuoteDocumentStream();
				    		            		
				    		            		new SendAsyncMail(_Event, fileName,_TableSetting.getMessage(),_TableSetting.getSubject()).execute();
			    		            		}
										}
									}).setPositiveButton("Send with message", new DialogInterface.OnClickListener() {
										
										@Override
										public void onClick(DialogInterface dialog, int which) {
											// TODO Auto-generated method stub
											
											if(message.getText().toString().length() > 0){
											
												String strMessage = message.getText().toString();
												String strSubject = subject.getText().toString();
												
												TableEvents _Event = _TableEvents.get(mSelectedRow);
					    		            	
				    		            		if(_Event != null){
					    		                	Date t = new Date();
				    		            			String clientName = _Event.getClient().getLastName();
				    		            			
				    		            			ArrayList<TableQuoteItem> items = (ArrayList<TableQuoteItem>) dataRepository.GetQuoteItemByEventID(_Event.getID());
				    		            			
				    		            			if(getSherlockActivity() != null)
				    		            				getSherlockActivity().setProgressBarIndeterminateVisibility(true);
				    		            			
					    		                	String fileName = "/sdcard/" + clientName  + " " + formater.format(t) + ".pdf";
					    		            		QuotePDFGenerator _Generator = new QuotePDFGenerator(fileName,_Event, items);
					    		            		_Generator.GetGeneratedQuoteDocumentStream();
					    		            		
					    		            		new SendAsyncMail(_Event, fileName,strMessage, strSubject).execute();
				    		            		}
			    		            		}else{
			    		            			showBuiltInCrouton(INFINITEERROR, "Please provide email message body");
			    		            		}
										}
									}).show();
	    						}
	    						catch(Exception Ex){
	    							showBuiltInCrouton(INFINITEERROR, Ex.getMessage());
	    						}
	    		            }
	    		        }).show();
						
						break;
					case ID_DETAILS:
						TableEvents _Event = _TableEvents.get(mSelectedRow);
						Bundle bundle = new Bundle();
						bundle.putSerializable("_TableEvent", _Event);
						Intent intent = new Intent(DashboardActivity._DashboardContext, EventViewEditActivity.class);
						intent.putExtra("Data", bundle);
						
						startActivityForResult(intent,1);
						break;
					case ID_DELETE:
						new SweetAlertDialog(DashboardActivity._DashboardContext, SweetAlertDialog.WARNING_TYPE)
	    		        .setTitleText("Are you sure?")
	    		        .setContentText("Event will be deleted permanetly!")
	    		        .setCancelText("No!")
	    		        .setConfirmText("Yes!")
	    		        .showCancelButton(true)
	    		        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
	    		            @Override
	    		            public void onClick(SweetAlertDialog sDialog) {
	    		                // reuse previous dialog instance, keep widget user state, reset them if you need
	    		                sDialog.setTitleText("Oops!")
	    		                        .setContentText("Operation cancelled.")
	    		                        .setConfirmText("OK")
	    		                        .showCancelButton(false)
	    		                        .setCancelClickListener(null)
	    		                        .setConfirmClickListener(null)
	    		                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
	    		            }
	    		        })
	    		        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
	    		            @Override
	    		            public void onClick(SweetAlertDialog sDialog) {
	    		            	try{
	    		            		TableEvents _SelectedEvent = _TableEvents.get(mSelectedRow);
	    		            	
	    		            		List<TableQuoteItem> _QuoteItems = dataRepository.GetQuoteItemByEventID(_SelectedEvent.getID());
	    		            		
	    		            		if(_QuoteItems != null)
	    		            			dataRepository.DeleteQuoteItems(_QuoteItems);
	    		            		
	    		            		dataRepository.DeleteEvent(_SelectedEvent.getID());
	    		            		
	    		            		sDialog.setTitleText("Sweet!")
	    		                     .setContentText("Event details successfuly deleted.")
	    		                     .setConfirmText("OK")
	    		                     .showCancelButton(false)
	    		                     .setCancelClickListener(null)
	    		                     .setConfirmClickListener(null)
	    		                     .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
	    						}
	    						catch(Exception Ex){
	    							showBuiltInCrouton(INFINITEERROR, Ex.getMessage());
	    						}
	    		            }
	    		        }).show();
						break;
					default:
						TableEvents Event = _TableEvents.get(mSelectedRow);
						Bundle _bundle = new Bundle();
						_bundle.putSerializable("_TableEvent", Event);
						_bundle.putBoolean("IsEditMode", true);
						Intent _intent = new Intent(DashboardActivity._DashboardContext, EventFormActivity.class);
						_intent.putExtra("Data", _bundle);
						
						startActivityForResult(_intent, 1);
						break;
					}
    				
    			}
    		});
    		
			_TableEvents = dataRepository.GetAllEvents();
			_ListViewEvents = (ListView) rootView.findViewById(R.id.event_list);
			noEvents = (TextView) rootView.findViewById(R.id.noEventsFound);
			
			if(_TableEvents != null && _TableEvents.size() > 0){
				noEvents.setVisibility(View.GONE);
				GlobalVariables._EventListAdapter = new EventListAdapter(DashboardActivity._DashboardContext, R.layout.events_row_item, _TableEvents, dataRepository);
				_ListViewEvents.setAdapter(GlobalVariables._EventListAdapter);
			}else{
				noEvents.setVisibility(View.VISIBLE);
			}
			
			_ListViewEvents.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					mSelectedRow = position; //set the selected row
					
					mQuickAction.show(view);
				}
			});
			
    		break;
		case 1:
			_TablePackages = dataRepository.GetAllPackages();
            
			if(_TablePackages == null)
				_TablePackages = new ArrayList<TablePackages>();
			
			GlobalVariables.adapterPackages = new PackagesListAdapter(DashboardActivity._DashboardContext, R.layout.view_row, _TablePackages);
            
            
            expandableLayoutListView = (ExpandableLayoutListView) rootView.findViewById(R.id.listview);

            expandableLayoutListView.setAdapter(GlobalVariables.adapterPackages);
            
            expandableLayout = (ExpandableLayout)rootView.findViewById(R.id.first);
            
            
            btnSaveNew = (Button) expandableLayout.findViewById(R.id.button);
    		edtDescription = (EditText) expandableLayout.findViewById(R.id.text);
    		edtPrice = (EditText) expandableLayout.findViewById(R.id.value);
    		chkIsActive = (CheckBox) expandableLayout.findViewById(R.id.isactive);
    		
    		chkIsActive.setEnabled(false);
    		
    		btnSaveNew.setOnClickListener(new View.OnClickListener() {
    			
    			@Override
    			public void onClick(View v) {
    				// TODO Auto-generated method stub

    				new SweetAlertDialog(DashboardActivity._DashboardContext, SweetAlertDialog.WARNING_TYPE)
    		        .setTitleText("Are you sure?")
    		        .setContentText("Package details will be saved to you device.")
    		        .setCancelText("No!")
    		        .setConfirmText("Yes!")
    		        .showCancelButton(true)
    		        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
    		            @Override
    		            public void onClick(SweetAlertDialog sDialog) {
    		                // reuse previous dialog instance, keep widget user state, reset them if you need
    		                sDialog.setTitleText("Oops!")
    		                        .setContentText("Operation cancelled.")
    		                        .setConfirmText("OK")
    		                        .showCancelButton(false)
    		                        .setCancelClickListener(null)
    		                        .setConfirmClickListener(null)
    		                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
    		            }
    		        })
    		        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
    		            @Override
    		            public void onClick(SweetAlertDialog sDialog) {
    		            	
    		            	String desc = edtDescription.getText().toString();
    						String price = edtPrice.getText().toString();
    						
    						sDialog.dismissWithAnimation();
    						
    						if(price.length() < 1 || desc.length() < 1){
    							showBuiltInCrouton(INFINITEERROR, "Please enter Package description and Price.");
    						}
    						else
    						{
	    						TablePackages _Package = new TablePackages(desc, Double.valueOf(price));
	    						
	    						boolean saved = dataRepository.SaveOrUpdatePackage(_Package);
	    						if(saved){
	    							//showBuiltInCrouton(Style.CONFIRM, "Package Details Successfuly Saved.");
	    							GlobalVariables.adapterPackages.add(_Package);
	    							GlobalVariables.adapterPackages.notifyDataSetChanged();
	    							
	    							edtDescription.setText(null);
	    							edtPrice.setText(null);
	    							
	    							showBuiltInCrouton(Style.CONFIRM, "Package details successfuly saved.");
	    							
	    							edtDescription.requestFocus();
	    							
	    							
	    							/*sDialog.setTitleText("Sweet!")
		   		                     .setContentText("Package details successfuly saved.")
		   		                     .setConfirmText("OK")
		   		                     .showCancelButton(false)
		   		                     .setCancelClickListener(null)
		   		                     .setConfirmClickListener(null)
		   		                     .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);*/
	    							
	    						}else{
	    							showBuiltInCrouton(INFINITEERROR, "Unidentified Error Has Occured, Package Details Not Saved.");
	    						}
    						}
    		            }
    		        }).show();
    			}
    		});
            
            expandableLayoutListView.setOnItemClickListener(new OnItemClickListener() {
     
    			@Override
    			public void onItemClick(AdapterView<?> adapterview, View v, int pos,
    					long id) {
    				// TODO Auto-generated method stub
    				final int index = pos;
    				final View view = v;
    				final long ids = id;
    				
    				btnUpdate = (Button) view.findViewById(R.id.button);
    				final EditText editDesc = (EditText) view.findViewById(R.id.text);
    				final EditText editPrice = (EditText) view.findViewById(R.id.value);
    				final CheckBox _chkIsActive = (CheckBox) view.findViewById(R.id.isactive);
    	    		
    	    		_SelectedPackage = _TablePackages.get(pos);
    	    		
    	    		editDesc.setText(_SelectedPackage.getDescription());
    	    		editPrice.setText(String.valueOf(_SelectedPackage.getPrice()));
    	    		
    	    		boolean isActive = _SelectedPackage.getIsActive() == 1? true:false;
    	    		
    				_chkIsActive.setChecked(isActive);
    				
    				btnUpdate.setOnClickListener(new View.OnClickListener() {
    					
    					@Override
    					public void onClick(View v) {
    						// TODO Auto-generated method stub
    						
    						new SweetAlertDialog(DashboardActivity._DashboardContext, SweetAlertDialog.WARNING_TYPE)
    	    		        .setTitleText("Are you sure?")
    	    		        .setContentText("Package details will be saved to you device.")
    	    		        .setCancelText("No!")
    	    		        .setConfirmText("Yes!")
    	    		        .showCancelButton(true)
    	    		        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
    	    		            @Override
    	    		            public void onClick(SweetAlertDialog sDialog) {
    	    		                // reuse previous dialog instance, keep widget user state, reset them if you need
    	    		                sDialog.setTitleText("Oops!")
    	    		                        .setContentText("Operation cancelled.")
    	    		                        .setConfirmText("OK")
    	    		                        .showCancelButton(false)
    	    		                        .setCancelClickListener(null)
    	    		                        .setConfirmClickListener(null)
    	    		                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
    	    		            }
    	    		        })
    	    		        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
    	    		            @Override
    	    		            public void onClick(SweetAlertDialog sDialog) {
    	    		            	try{
    	    		            		
    	    		            		sDialog.dismissWithAnimation();
    		    						
    		    						if(editDesc.getText().toString().length() < 1 || editPrice.getText().toString().length() < 1){
    		    							showBuiltInCrouton(INFINITEERROR, "Please enter Package description and Price.");
    		    						}
    		    						else
    		    						{
    	    		            		
	    		    						_SelectedPackage.setDescription(editDesc.getText().toString());
	    		    						_SelectedPackage.setPrice(Float.valueOf(editPrice.getText().toString()));
	    		    						_SelectedPackage.setPendingSync();
	    		    						
	    		    						int _isactive = _chkIsActive.isChecked()?1:0;
	    									_SelectedPackage.setIsActive(_isactive);
	    									
	    		    						dataRepository.SaveOrUpdatePackage(_SelectedPackage);
	    		    						
	    		    						GlobalVariables.adapterPackages.notifyDataSetChanged();
	    		    						showBuiltInCrouton(Style.CONFIRM, "Package Details Successfuly Updated.");
	    		    						
	    		    						expandableLayoutListView.performItemClick(view, index, ids);
    		    						}
    	    						}
    	    						catch(Exception Ex){
    	    							showBuiltInCrouton(INFINITEERROR, Ex.getMessage());
    	    						}
    	    		            }
    	    		        }).show();
    					}
    				});
    			}
    		});
			break;
			
		case 2:
			
			_TableEventTypes = dataRepository.GetAllEventTypes();
            
			if(_TableEventTypes == null)
				_TableEventTypes = new ArrayList<TableEventTypes>();
			
			GlobalVariables.adapterEventType = new EventTypeListAdapter(DashboardActivity._DashboardContext, R.layout.view_row, _TableEventTypes);
            
            
            expandableLayoutListView = (ExpandableLayoutListView) rootView.findViewById(R.id.listview);

            expandableLayoutListView.setAdapter(GlobalVariables.adapterEventType);
            
            expandableLayout = (ExpandableLayout)rootView.findViewById(R.id.first);
            
            
            btnSaveNew = (Button) expandableLayout.findViewById(R.id.button);
    		edtDescription = (EditText) expandableLayout.findViewById(R.id.text);
    		edtPrice = (EditText) expandableLayout.findViewById(R.id.value);
    		chkIsActive = (CheckBox) expandableLayout.findViewById(R.id.isactive);
    		
    		chkIsActive.setEnabled(false);
    		
    		btnSaveNew.setOnClickListener(new View.OnClickListener() {
    			
    			@Override
    			public void onClick(View v) {
    				// TODO Auto-generated method stub
    				
    				new SweetAlertDialog(DashboardActivity._DashboardContext, SweetAlertDialog.WARNING_TYPE)
    		        .setTitleText("Are you sure?")
    		        .setContentText("Event Type details will be saved to you device.")
    		        .setCancelText("No!")
    		        .setConfirmText("Yes!")
    		        .showCancelButton(true)
    		        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
    		            @Override
    		            public void onClick(SweetAlertDialog sDialog) {
    		                // reuse previous dialog instance, keep widget user state, reset them if you need
    		                sDialog.setTitleText("Oops!")
    		                        .setContentText("Operation cancelled.")
    		                        .setConfirmText("OK")
    		                        .showCancelButton(false)
    		                        .setCancelClickListener(null)
    		                        .setConfirmClickListener(null)
    		                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
    		            }
    		        })
    		        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
    		            @Override
    		            public void onClick(SweetAlertDialog sDialog) {
    						try{
	    						String desc = edtDescription.getText().toString();
	    						String price = edtPrice.getText().toString();
	    						sDialog.dismissWithAnimation();
	    						
	    						if(price.length() < 1 || desc.length() < 1){
	    							showBuiltInCrouton(INFINITEERROR, "Please enter Event Type description and Price.");
	    						}
	    						else
	    						{
		    						TableEventTypes _EventType = new TableEventTypes(desc, Float.valueOf(price));
		    						
		    						boolean saved = dataRepository.SaveOrUpdateEventType(_EventType);
		    						
		    						if(saved){
		    							//
	    							GlobalVariables.adapterEventType.add(_EventType);
	    							GlobalVariables.adapterEventType.notifyDataSetChanged();
	    							
	    							edtDescription.setText(null);
	    							edtPrice.setText(null);
	    							
	    							showBuiltInCrouton(Style.CONFIRM, "Event Type Details Successfuly Saved.");
	    							
	    							edtDescription.requestFocus();
	    							
		    						}
		    						else
		    						{
		    							showBuiltInCrouton(INFINITEERROR, "Unidentified Error Has Occured, Event Type Details Not Saved.");
		    						}
	    						}
    						}catch(Exception e){
    							e.printStackTrace();
    						}
    		            }
    		        }).show();
    			}
    		});

            expandableLayoutListView.setOnItemClickListener(new OnItemClickListener() {
     
    			@Override
    			public void onItemClick(AdapterView<?> adapterview, View v, int pos,
    					long id) {
    				// TODO Auto-generated method stub
    				final int index = pos;
    				final View view = v;
    				final long ids = id;
    				
    				btnUpdate = (Button) view.findViewById(R.id.button);
    	    		final EditText editDesc = (EditText) view.findViewById(R.id.text);
    	    		final EditText editPrice = (EditText) view.findViewById(R.id.value);
    	    		final CheckBox _chkIsActive = (CheckBox) view.findViewById(R.id.isactive);
    	    		
    				_SelectedEventType = _TableEventTypes.get(pos);
    				editDesc.setText(_SelectedEventType.getDescription());
    				editPrice.setText(String.valueOf(_SelectedEventType.getPrice()));
    				
    				boolean isActive = _SelectedEventType.getIsActive() == 1? true:false;
    	    		
    				_chkIsActive.setChecked(isActive);
    				
    				btnUpdate.setOnClickListener(new View.OnClickListener() {
    					
    					@Override
    					public void onClick(View v) {
    						// TODO Auto-generated method stub
    						
    						new SweetAlertDialog(DashboardActivity._DashboardContext, SweetAlertDialog.WARNING_TYPE)
    	    		        .setTitleText("Are you sure?")
    	    		        .setContentText("Event Type details will be saved to you device.")
    	    		        .setCancelText("No!")
    	    		        .setConfirmText("Yes!")
    	    		        .showCancelButton(true)
    	    		        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
    	    		            @Override
    	    		            public void onClick(SweetAlertDialog sDialog) {
    	    		                // reuse previous dialog instance, keep widget user state, reset them if you need
    	    		                sDialog.setTitleText("Oops!")
    	    		                        .setContentText("Operation cancelled.")
    	    		                        .setConfirmText("OK")
    	    		                        .showCancelButton(false)
    	    		                        .setCancelClickListener(null)
    	    		                        .setConfirmClickListener(null)
    	    		                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
    	    		            }
    	    		        })
    	    		        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
    	    		            @Override
    	    		            public void onClick(SweetAlertDialog sDialog) {
    	    		            	try{
    	    		            		sDialog.dismissWithAnimation();
    	    		            		
    	    		            		if(editDesc.getText().toString().length() > 0 && editPrice.getText().toString().length() > 0){
	    	    		            		_SelectedEventType.setDescription(editDesc.getText().toString());
	    	    		            		_SelectedEventType.setPrice(Float.valueOf(editPrice.getText().toString()));
	    	    		            		_SelectedEventType.setPendingSync();
	        	    						
	        	    						int _isactive = _chkIsActive.isChecked()?1:0;
	        	    						_SelectedEventType.setIsActive(_isactive);
	        								
	        	    						dataRepository.SaveOrUpdatePackage(_SelectedPackage);
	        	    						
	        	    						showBuiltInCrouton(Style.CONFIRM, "Event Type Details Successfuly Updated.");
	        	    						
	        	    						GlobalVariables.adapterEventType.notifyDataSetChanged();
	        	    						
	        	    						expandableLayoutListView.performItemClick(view, index, ids);
    	    		            		}
    	    		            		else{
    	    		            			showBuiltInCrouton(INFINITEERROR, "Please enter Event Type description and Price");
    	    		            		}
    	    						}
    	    						catch(Exception Ex){
    	    							showBuiltInCrouton(INFINITEERROR, Ex.getMessage());
    	    						}
    	    		            }
    	    		        }).show();
    					}
    				});
    			}
    		});
			break;
			
			default:
				break;
		}
    	
        return rootView;
    }

    @Override
   	public void onSaveInstanceState(Bundle state) {
   	    super.onSaveInstanceState(state);
   	
   	    state.putInt("Dummy", 0);
   	}
    
    @Override
	public void onResume() {
		super.onResume();
		DashboardActivity._DashboardContext.registerReceiver(broadcastReceiver, new IntentFilter(
				GlobalVariables.BROADCAST_ACTION));
	}

	@Override
	public void onPause() {
		super.onPause();
		DashboardActivity._DashboardContext.unregisterReceiver(broadcastReceiver);
	}
	
	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			DoSomethingWithDataReceived(intent);
		}
	};
	
	public void DoSomethingWithDataReceived(Intent intent)
	{
		String strData = intent.getStringExtra("Refresh");
		if(strData != null && strData.equalsIgnoreCase("Refresh")){
			_TableEvents = dataRepository.GetAllEvents();
			
			if(_TableEvents != null && _TableEvents.size() > 0){
				noEvents.setVisibility(View.GONE);
				GlobalVariables._EventListAdapter = new EventListAdapter(DashboardActivity._DashboardContext, R.layout.events_row_item, _TableEvents, dataRepository);
				_ListViewEvents.setAdapter(GlobalVariables._EventListAdapter);
			}else{
				noEvents.setVisibility(View.VISIBLE);
			}
			
			Toast.makeText(DashboardActivity._DashboardContext, "DoSomethingWithDataReceived", Toast.LENGTH_SHORT).show();
		}
	}
    
    public boolean isOnline() {
        ConnectivityManager cm =
            (ConnectivityManager) DashboardActivity._DashboardContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        }
        return false;
    }
    
	private String status = "";
	private class SendAsyncMail extends AsyncTask<Void, Integer, String> {
		String fileName;
		TableEvents event;
		String message, subject;
		
		public SendAsyncMail(TableEvents event, String fileName, String message, String subject){
			this.event = event;
			this.fileName = fileName;
			this.message = message;
			this.subject = subject;
		}
		@Override
		protected String doInBackground(Void... arg0) {
		    status = global.SendGeneratedQuote(fileName, event,message, subject);
			return status;
		}
		
		@Override
		protected void onProgressUpdate(Integer... progress) {
	         //btnSendMail.setProgress(progress[0]);
	     }
		 
		@Override
		protected void onPostExecute(String result) {
			if(getSherlockActivity() != null)
				getSherlockActivity().setProgressBarIndeterminateVisibility(false);
			
			if(result == "Sent") { 
				showBuiltInCrouton(INFINITE, "Email was sent successfully."); 
	          } else { 
	        	  showBuiltInCrouton(INFINITEERROR, "Email was not sent.");
	          } 
        }
    }
}
