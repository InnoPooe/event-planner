package com.eventplanner.fragments;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.londatiga.android.ActionItem;
import net.londatiga.android.QuickAction;
import net.londatiga.android.QuickAction.OnActionItemClickListener;

import cn.pedant.SweetAlert.SweetAlertDialog;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.eventplanner.ClientFormActivity;
import com.eventplanner.DashboardActivity;
import com.eventplanner.R;
import com.eventplanner.adapters.ClientListAdapter;
import com.eventplanner.database.EventPlannerRepository;
import com.eventplanner.database.EventPlannerSQLiteDB;
import com.eventplanner.databasetables.TableClients;
import com.eventplanner.databasetables.TableEvents;
import com.eventplanner.databasetables.TableQuoteItem;
import com.eventplanner.databasetables.TableSettings;
import com.eventplanner.dataclass.GlobalVariables;
import com.eventplanner.dataclass.ParseCloudQueryObject;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.Validator.ValidationListener;
import com.tekle.oss.android.animation.AnimationFactory;
import com.tekle.oss.android.animation.AnimationFactory.FlipDirection;
import de.keyboardsurfer.android.widget.crouton.Configuration;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewAnimator;

public class ManageClientsFragment  extends SherlockFragment implements ValidationListener{

	private static final String ARG_SECTION_NUMBER = "section_number";

	TextView layout1, layout2;
	private View rootView;
	private View childView = null;
	private LayoutInflater _LayoutInflater;
	private LinearLayout ParentView;
	
	private EditText editEmail;
    private EditText editAddress;
    private EditText editFullname;
    private EditText editLastname;
    private EditText editMobileNo;
    private EditText editTelNo;
    
	private Validator validator;
    
    private EventPlannerSQLiteDB databaseHelper;
	private EventPlannerRepository dataRepository;
	
	GlobalVariables global = new GlobalVariables();
	
	public ClientListAdapter _Heading;
	public List<TableClients> _TableClients; 
	public ListView _ListViewClients;//, _ListHeading;
	
	private int mSelectedRow;
	private QuickAction mQuickAction;
	
	private static final int ID_DETAILS = 1;
	private static final int ID_EDIT= 2;
	private static final int ID_DELETE = 3;
	
	private static final Style INFINITE = new Style.Builder().
		    setBackgroundColorValue(Style.holoBlueLight).build();
	
	private static final Style INFINITEERROR = new Style.Builder().
		    setBackgroundColorValue(Style.holoRedLight).build();
	
	private static final Configuration CONFIGURATION_INFINITE = new Configuration.Builder()
	          .setDuration(Configuration.DURATION_LONG)
	          .build();
	
	private void showBuiltInCrouton(final Style croutonStyle, String croutonText) {
	    showCrouton(croutonText, croutonStyle, Configuration.DEFAULT);
	}
	
	private void showCrouton(String croutonText, Style croutonStyle, Configuration configuration) {
	    final boolean infinite = INFINITE == croutonStyle || INFINITEERROR == croutonStyle;
	    
	       
	    final Crouton crouton;
	      crouton = Crouton.makeText(getActivity(), croutonText, croutonStyle);
	    
	    if (infinite) {
	    }
	    crouton.setConfiguration(infinite ? CONFIGURATION_INFINITE : configuration).show();
	}
	
    public static ManageClientsFragment newInstance(int sectionNumber) {
    	ManageClientsFragment fragment = new ManageClientsFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public ManageClientsFragment() {
    	
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
    	
    	this._LayoutInflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	
        rootView = inflater.inflate(R.layout.fragment_clients, container, false);
       // ParentView = (LinearLayout)rootView.findViewById(R.id.mainContent);
        
        databaseHelper = new EventPlannerSQLiteDB(DashboardActivity._DashboardContext);
        dataRepository = new EventPlannerRepository(databaseHelper);
        
        validator = new Validator(this);
        validator.setValidationListener(this);
        
        ActionItem addItem 		= new ActionItem(ID_EDIT, "Edit", getResources().getDrawable(R.drawable.edit));
		ActionItem acceptItem 	= new ActionItem(ID_DETAILS, "Details", getResources().getDrawable(R.drawable.info));
        ActionItem uploadItem 	= new ActionItem(ID_DELETE, "Delete", getResources().getDrawable(R.drawable.trash));
        
        mQuickAction = new QuickAction(DashboardActivity._DashboardContext);
		
		mQuickAction.addActionItem(addItem);
		mQuickAction.addActionItem(acceptItem);
		mQuickAction.addActionItem(uploadItem);
		
        UpdateClientList();
        
        
        
        setHasOptionsMenu(true);
        
        return rootView;
    }
    
    @SuppressLint("InflateParams")
	public void UpdateClientList()
    {
    	final ViewAnimator viewAnimator = (ViewAnimator)rootView.findViewById(R.id.viewFlipper);
    	
    	
    	/*_ListHeading = (ListView) rootView.findViewById(R.id.client_listheading);
    	
    	List<TableClients> heading = new ArrayList<TableClients>();
    	heading.add(new TableClients(" ", "Client Name", "Mobile Number", "", "Email Address", "Address"));
    	
    	_Heading = new ClientListAdapter(DashboardActivity._DashboardContext, R.layout.clientlist_rowitem_layout_old, heading);
    	_ListHeading.setAdapter(_Heading);*/
    	
		_ListViewClients = (ListView) rootView.findViewById(R.id.client_list);
		
		_TableClients = dataRepository.GetAllClients();
		
		if(_ListViewClients != null && _TableClients != null){
			GlobalVariables._ClientListAdapter = new ClientListAdapter(DashboardActivity._DashboardContext, R.layout.clientlist_rowitem_layout, _TableClients);
			_ListViewClients.setAdapter(GlobalVariables._ClientListAdapter);
		}
		
		_ListViewClients.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				mSelectedRow = position;
				mQuickAction.show(view);
			}
		});
		
		mQuickAction.setOnActionItemClickListener(new OnActionItemClickListener() {
			
			@Override
			public void onItemClick(QuickAction source, int pos, int actionId) {
				// TODO Auto-generated method stub
				switch (actionId) {
				case ID_EDIT:
					TableClients Client = _TableClients.get(mSelectedRow);
					Bundle _bundle = new Bundle();
					_bundle.putSerializable("_TableClient", Client);
					_bundle.putBoolean("IsEditMode", true);
					Intent intent = new Intent(DashboardActivity._DashboardContext, ClientFormActivity.class);
					intent.putExtras(_bundle);
					
					startActivityForResult(intent, 1);
					break;
				case ID_DETAILS:
					TableClients _Client = _TableClients.get(mSelectedRow);
					Bundle bundle = new Bundle();
					bundle.putSerializable("_TableClient", _Client);
					bundle.putBoolean("IsEditMode", false);
					Intent _intent = new Intent(DashboardActivity._DashboardContext, ClientFormActivity.class);
					_intent.putExtras(bundle);
					
					startActivity(_intent);
					break;
				default:
					new SweetAlertDialog(DashboardActivity._DashboardContext, SweetAlertDialog.WARNING_TYPE)
    		        .setTitleText("Are you sure?")
    		        .setContentText("Client and all data linked to him/her will be deleted permanetly!")
    		        .setCancelText("No!")
    		        .setConfirmText("Yes!")
    		        .showCancelButton(true)
    		        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
    		            @Override
    		            public void onClick(SweetAlertDialog sDialog) {
    		                // reuse previous dialog instance, keep widget user state, reset them if you need
    		                sDialog.setTitleText("Oops!")
    		                        .setContentText("Operation cancelled.")
    		                        .setConfirmText("OK")
    		                        .showCancelButton(false)
    		                        .setCancelClickListener(null)
    		                        .setConfirmClickListener(null)
    		                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);
    		            }
    		        })
    		        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
    		            @Override
    		            public void onClick(SweetAlertDialog sDialog) {
    		            	try{
    		            		sDialog.dismiss();
    		            		TableClients _Client = _TableClients.get(mSelectedRow);
    		            	
    		            		List<TableEvents> _Events = dataRepository.GetAllEventsByClientID(_Client.getID());
    		            		if(_Events != null){
    		            			for (TableEvents event : _Events) {
    		            				List<TableQuoteItem> _QuoteItems = dataRepository.GetQuoteItemByEventID(event.getID());
    	    		            		
    	    		            		if(_QuoteItems != null)
    	    		            			dataRepository.DeleteQuoteItems(_QuoteItems);
    	    		            		
    	    		            		dataRepository.DeleteEvent(event.getID());
									}
    		            		}
    		            		
    		            		dataRepository.DeleteClient(_Client.getID());
    		            		
    		            		sDialog.setTitleText("Sweet!")
	   		                     .setContentText("Client details successfuly deleted.")
	   		                     .setConfirmText("OK")
	   		                     .showCancelButton(false)
	   		                     .setCancelClickListener(null)
	   		                     .setConfirmClickListener(null)
	   		                     .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
    						}
    						catch(Exception Ex){
    							showBuiltInCrouton(INFINITEERROR, Ex.getMessage());
    						}
    		            }
    		        }).show();
					break;
				}
			}
		});
		
		AnimationFactory.flipTransition(viewAnimator, FlipDirection.LEFT_RIGHT);
    }
    
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
    	super.onViewCreated(view, savedInstanceState);
    	
    //	UpdateClientList();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		
		try
		{
			menu.add("New Client").setNumericShortcut('A')
			.setIcon(R.drawable.add_client)
    		.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
			
			menu.add("Syncronize").setNumericShortcut('S')
			.setIcon(R.drawable.refresh_icon)
    		.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		}
		catch(Exception Ex)
		{
			Ex.printStackTrace();
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		if(item.getNumericShortcut() == 'S'){
			try{
				getSherlockActivity().setProgressBarIndeterminateVisibility(true);
				
				ParseCloudQueryObject ParseSyncCloud = new ParseCloudQueryObject(dataRepository);
				TableSettings _Settings = dataRepository.GetSettings();
				
	 		    ParseSyncCloud.SyncCloundDataToDevice(GlobalVariables._LoggedUser.getCloudID());
			   _Settings.setLastSyncDateDownload(new Date());
	   		    dataRepository.SaveOrUpdateSettings(_Settings);
	   		    
	   		    getSherlockActivity().setProgressBarIndeterminateVisibility(false);
	   		    
	   		    GlobalVariables global = new GlobalVariables();
	   		    global.UploadLocalDataToClound(_Settings, dataRepository);
			}
			catch(Exception Ex){
				Ex.printStackTrace();
			}
		}else{
			startActivityForResult(new Intent(DashboardActivity._DashboardContext, ClientFormActivity.class), 1);
		}
		return false;
	}
	
	@SuppressLint("InflateParams")
	public void InflateView(MenuItem item)
    {
		
		if(item.getNumericShortcut() == 'A'){
			ParentView.removeView(childView);     
			childView = _LayoutInflater.inflate(R.layout.clientinfo_layout, null);
			ParentView.addView(childView);
			
			editEmail = (EditText)childView.findViewById(R.id.email);
	        editAddress = (EditText)childView.findViewById(R.id.address);
	        editFullname = (EditText)childView.findViewById(R.id.fullname);
	        editTelNo = (EditText)childView.findViewById(R.id.telno);
	        editLastname = (EditText)childView.findViewById(R.id.lastname);
	        editMobileNo = (EditText)childView.findViewById(R.id.mobileno);
	        
	        validator.put(editFullname, global.RequiredRule);
			validator.put(editLastname, global.RequiredRule);
			validator.put(editEmail, global._ValidateEmailAddress);
			validator.put(editMobileNo, global.RequiredRule);
			validator.put(editAddress, global.RequiredRule);
			getSherlockActivity().invalidateOptionsMenu();
			
	    	final ViewAnimator viewAnimator = (ViewAnimator)childView.findViewById(R.id.viewFlipper);
	    	AnimationFactory.flipTransition(viewAnimator, FlipDirection.LEFT_RIGHT);
		}
		else if(item.getNumericShortcut() == 'S'){
			validator.validate();
		}	
    }
	
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
    }

	@Override
	public void onValidationSucceeded() {
		// TODO Auto-generated method stub
		
		new SweetAlertDialog(DashboardActivity._DashboardContext, SweetAlertDialog.WARNING_TYPE)
        .setTitleText("Are you sure?")
        .setContentText("Client details will be saved to you device.")
        .setCancelText("No!")
        .setConfirmText("Yes!")
        .showCancelButton(true)
        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                // reuse previous dialog instance, keep widget user state, reset them if you need
                sDialog.setTitleText("Oops!")
                        .setContentText("Operation cancelled.")
                        .setConfirmText("OK")
                        .showCancelButton(false)
                        .setCancelClickListener(null)
                        .setConfirmClickListener(null)
                        .changeAlertType(SweetAlertDialog.ERROR_TYPE);

                // or you can new a SweetAlertDialog to show
               /* sDialog.dismiss();
                new SweetAlertDialog(SampleActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Cancelled!")
                        .setContentText("Your imaginary file is safe :)")
                        .setConfirmText("OK")
                        .show();*/
            }
        })
        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                TableClients _TableClient = new TableClients(editFullname.getText().toString(), editLastname.getText().toString(),
						editMobileNo.getText().toString(), editTelNo.getText().toString(), 
						editEmail.getText().toString(), editAddress.getText().toString());
				
				boolean saved = dataRepository.SaveOrUpdateClient(_TableClient);
				if(saved){
					UpdateClientList();
					
					sDialog.setTitleText("Sweet!")
                     .setContentText("Client details successfuly saved.")
                     .setConfirmText("OK")
                     .showCancelButton(false)
                     .setCancelClickListener(null)
                     .setConfirmClickListener(null)
                     .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
					 
					//showBuiltInCrouton(Style.CONFIRM, "Client Details Successfuly Saved.");
				}else{
					showBuiltInCrouton(INFINITEERROR, "Unidentified Error Has Occured, Client Details Not Saved.");
				}
				getSherlockActivity().invalidateOptionsMenu();
            }
        }).show();
		
		/*new AlertDialog.Builder(DashboardActivity._DashboardContext)
		.setTitle("Save Client Details")
		.setMessage("Are You Sure")
		.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
				
				
				
				dialog.dismiss();
			}
		}).setNegativeButton("No", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
			}
		}).show();*/
	}

	@Override
	public void onValidationFailed(View failedView, Rule<?> failedRule) {
		// TODO Auto-generated method stub
		String message = failedRule.getFailureMessage();

        if (failedView instanceof EditText) {
            failedView.requestFocus();
            ((EditText) failedView).setError(message);
        } else {
            Toast.makeText(DashboardActivity._DashboardContext, message, Toast.LENGTH_SHORT).show();
        }
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch(resultCode){
			case 2:
				try{
					UpdateClientList();
				}catch(Exception Ex){
					
				}
			break;
		}
	}
}
