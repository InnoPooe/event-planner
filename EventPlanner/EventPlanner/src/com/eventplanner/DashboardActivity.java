package com.eventplanner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.Window;
import com.eventplanner.database.EventPlannerRepository;
import com.eventplanner.database.EventPlannerSQLiteDB;
import com.eventplanner.databasetables.TableClients;
import com.eventplanner.databasetables.TableEventTypes;
import com.eventplanner.databasetables.TableEvents;
import com.eventplanner.databasetables.TablePackages;
import com.eventplanner.databasetables.TableQuoteItem;
import com.eventplanner.databasetables.TableSettings;
import com.eventplanner.databasetables.TableUsers;
import com.eventplanner.dataclass.GlobalVariables;
import com.eventplanner.dataclass.ParseCloudQueryObject;
import com.eventplanner.fragments.EventFragment;
import com.eventplanner.fragments.ManageClientsFragment;
import com.eventplanner.fragments.ManageSettingsFragment;
import com.eventplanner.services.SyncCloudAndLocalDataService;
import com.special.ResideMenu.ResideMenu;
import com.special.ResideMenu.ResideMenuItem;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;


@SuppressWarnings("unused")
public class DashboardActivity extends SherlockFragmentActivity implements View.OnClickListener{

	  
	private DrawerLayout mDrawerLayout;
	//private SherlockActionBarDrawerToggle mDrawerToggle;
	private ListView mDrawerList;
	
	
    public static Context _DashboardContext;
	
    
	private int _CurrentPosition = 0;

	
	private TableUsers _TableUser;
    
	private ResideMenu resideMenu;
    private ResideMenuItem itemLogout;
    private ResideMenuItem itemProfiles;
    private ResideMenuItem itemEvents;
    private ResideMenuItem itemSettings;
	  
    private View menuView;
    
    private EventPlannerSQLiteDB databaseHelper;
	private EventPlannerRepository dataRepository;
	
	private static final Style INFINITE = new Style.Builder().
		    setBackgroundColorValue(Style.holoBlueLight).build();
	private static final de.keyboardsurfer.android.widget.crouton.Configuration CONFIGURATION_INFINITE = new de.keyboardsurfer.android.widget.crouton.Configuration.Builder()
	          .setDuration(de.keyboardsurfer.android.widget.crouton.Configuration.DURATION_INFINITE)
	          .build();
	
	private void showBuiltInCrouton(final Style croutonStyle, String croutonText) {
	    showCrouton(croutonText, croutonStyle, de.keyboardsurfer.android.widget.crouton.Configuration.DEFAULT);
	 }
	
	private void showCrouton(String croutonText, Style croutonStyle, de.keyboardsurfer.android.widget.crouton.Configuration configuration) {
	    final boolean infinite = INFINITE == croutonStyle;
	    
	       
	    final Crouton crouton;
	      crouton = Crouton.makeText(this, croutonText, croutonStyle);
	    
	    if (infinite) {
	    }
	    crouton.setConfiguration((de.keyboardsurfer.android.widget.crouton.Configuration) (infinite ? CONFIGURATION_INFINITE : configuration)).show();
	}
	
	@SuppressWarnings("unchecked")
	@SuppressLint("InflateParams")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.dashboard_main);

		_DashboardContext = this;
		
		getSupportActionBar().show();
        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.drawable.background_img4));
      
     	getSupportActionBar().setTitle("");
        getSupportActionBar().setSubtitle("");
         
         getSupportActionBar().setLogo(R.drawable.ic_launcher);
         getSupportActionBar().setDisplayHomeAsUpEnabled(false);
         getSupportActionBar().setHomeButtonEnabled(false);
         getSupportActionBar().setDisplayShowHomeEnabled(false);
        
        databaseHelper = new EventPlannerSQLiteDB(this);
        dataRepository = new EventPlannerRepository(databaseHelper);
        
        LayoutInflater inflator = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        
        menuView = inflator.inflate(R.layout.custom_menu, null);
        
        getSupportActionBar().setCustomView(menuView);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        
        setProgressBarIndeterminateVisibility(false);
      
        setUpMenu();
        
        try{
			Bundle _Bundles = getIntent().getExtras().getBundle("NOTIFICATION");
			
			if(_Bundles != null)
			{
				List<TableEvents> _EventsDue = (List<TableEvents>) _Bundles.getSerializable("_EventsDue");
				
				int NOTIFICATION_ID = _Bundles.getInt("NOTIFICATION_ID");
				
				NotificationManager mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
				
				if(NOTIFICATION_ID > 0)
					mNotificationManager.cancel(NOTIFICATION_ID);
				
				if(_EventsDue != null && _EventsDue.size() > 0){
					TableEvents _Event = _EventsDue.get(0);
					
					Bundle b = new Bundle();
					b.putSerializable("_TableEvent", _Event);
					
					Intent intent = new Intent(DashboardActivity.this, EventViewEditActivity.class);
					intent.putExtra("Data", b);
					startActivity(intent);
				}
			}
		}
		catch(Exception Ex)
		{
			Ex.printStackTrace();
		}
        
        
        if( savedInstanceState == null ){
        	
        	TableSettings _Settings = dataRepository.GetSettings();
        	if(_Settings != null && _Settings.getLastSyncDateDownload() == null){
        		
        		if(GlobalVariables._LoggedUser != null){
        			
        			try
        			{
        				setProgressBarIndeterminateVisibility(true);
        				String UserId = GlobalVariables._LoggedUser.getCloudID();
        				ParseCloudQueryObject ParseSyncCloud = new ParseCloudQueryObject(dataRepository);
        			   
        				boolean clientsDownloed = ParseSyncCloud.SyncCloundDataToDevice(UserId);
        				_Settings.setLastSyncDateDownload(new Date());
        	   		    dataRepository.SaveOrUpdateSettings(_Settings);
        	   		    
        	   		    setProgressBarIndeterminateVisibility(false);
        	   		    
        	   		    switch(_CurrentPosition){
        	   		    	case 0:
        	   		    		//if(GlobalVariables._ClientListAdapter != null)
        	   		    		//	GlobalVariables._ClientListAdapter.notifyDataSetChanged();
        	   		    		break;
        	   		    	case 1:
        	   		    		
            	   		    	break;
        	   		    }
        			}
        			catch(Exception Ex){
        				Ex.printStackTrace();
        			}
        			
        			//DownloadCloudDataToDevice(_Settings, GlobalVariables._LoggedUser.getCloudID());
        		}
        	}
        	
        	changeFragment(new ManageClientsFragment());
        }
        else{
        	_CurrentPosition = savedInstanceState.getInt("_CurrentPosition");
        }
        
		Intent syncService = new Intent(this,  SyncCloudAndLocalDataService.class);
	    startService(syncService);
	    
	}
	
	private void setUpMenu() {

        // attach to current activity;
        resideMenu = new ResideMenu(this);
        resideMenu.setBackground(R.drawable.bg_mix);
        resideMenu.attachToActivity(this);
        resideMenu.setMenuListener(menuListener);
        //valid scale factor is between 0.0f and 1.0f. leftmenu'width is 150dip. 
        resideMenu.setScaleValue(0.6f);

        // create menu items;
        itemProfiles     = new ResideMenuItem(this, R.drawable.icon_profile, "Manage Clients");
        itemEvents  = new ResideMenuItem(this, R.drawable.icon_calendar,  "Manage Events");
        itemSettings = new ResideMenuItem(this, R.drawable.icon_settings, "Settings");
        itemLogout = new ResideMenuItem(this, R.drawable.logout, "Logout");

        itemEvents.setOnClickListener(this);
        itemProfiles.setOnClickListener(this);
        itemLogout.setOnClickListener(this);
        itemSettings.setOnClickListener(this);

        resideMenu.addMenuItem(itemProfiles, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemEvents, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemSettings, ResideMenu.DIRECTION_LEFT);
        resideMenu.addMenuItem(itemLogout, ResideMenu.DIRECTION_LEFT);

        // You can disable a direction by setting ->
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_RIGHT);
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_LEFT);
        
        menuView.findViewById(R.id.title_bar_left_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resideMenu.openMenu(ResideMenu.DIRECTION_LEFT);
            }
        });
        menuView.findViewById(R.id.title_bar_right_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resideMenu.openMenu(ResideMenu.DIRECTION_RIGHT);
            }
        });
    }
 
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return resideMenu.dispatchTouchEvent(ev);
    }

    @Override
    public void onClick(View view) {

        if (view == itemProfiles){
        	_CurrentPosition = 0;
            changeFragment(new ManageClientsFragment());
        }else if (view == itemEvents){
        	_CurrentPosition = 1;
            changeFragment(new EventFragment());
        }else if (view == itemSettings){
        	_CurrentPosition = 3;
            changeFragment(new ManageSettingsFragment());
        }else if (view == itemLogout){
        	_CurrentPosition = 4;
            setResult(2);
            finish();
        }

        resideMenu.closeMenu();
    }

    private ResideMenu.OnMenuListener menuListener = new ResideMenu.OnMenuListener() {
        @Override
        public void openMenu() {
           // Toast.makeText(_DashboardContext, "Menu is opened!", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void closeMenu() {
            //Toast.makeText(_DashboardContext, "Menu is closed!", Toast.LENGTH_SHORT).show();
        }
    };

    private void changeFragment(Fragment targetFragment){
        resideMenu.clearIgnoredViewList();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment, targetFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    // What good method is to access resideMenu？
    public ResideMenu getResideMenu(){
        return resideMenu;
    }
	
	@Override
	public void onSaveInstanceState(Bundle state) {
	    super.onSaveInstanceState(state);
	
	    state.putInt("_CurrentPosition", _CurrentPosition);
	    state.putSerializable("_TableUser", _TableUser);
	}
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
		try
		{
			
		}
		catch(Exception Ex)
		{
			Ex.printStackTrace();
		}
		return true;
	}

	private static long back_pressed;
	@Override
    public void onBackPressed() {
		if (back_pressed + 2000 > System.currentTimeMillis()) 
		{
			setResult(1);
			finish();
		}
        else 
        {
        	showBuiltInCrouton(Style.INFO, "Press Once Again To Exit!");
        }
		
        back_pressed = System.currentTimeMillis();
    };
	
	@Override
	public void onResume() {
		super.onResume();
		
	}

	@Override
	public void onPause() {
		super.onPause();
	    //unregisterReceiver(broadcastReceiver);
	}
	
	private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			DoSomethingWithDataReceived(intent);
		}
	};
	
	private void DoSomethingWithDataReceived(Intent _Intent) {
		try
		{
			
		}
		catch(Exception Ex)
		{
			Ex.printStackTrace();
		}
	}
}
