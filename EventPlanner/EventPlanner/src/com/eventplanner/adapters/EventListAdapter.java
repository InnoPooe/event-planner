package com.eventplanner.adapters;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.eventplanner.R;
import com.eventplanner.database.EventPlannerRepository;
import com.eventplanner.databasetables.TableEvents;
import com.eventplanner.databasetables.TableQuoteItem;

public class EventListAdapter extends ArrayAdapter<TableEvents>{

	public List<TableEvents> _TableEvents;
	private LayoutInflater inflater;
    private int itemLayout;
    private EventPlannerRepository dataRepository;
    
	public EventListAdapter(Context context, int textViewResourceId,
			List<TableEvents> _TableEvents, EventPlannerRepository dataRepository) {
		super(context, textViewResourceId, _TableEvents);
		// TODO Auto-generated constructor stub
		
		this._TableEvents = _TableEvents;
		this.itemLayout = textViewResourceId;
		this.inflater = LayoutInflater.from(context);
		this.dataRepository = dataRepository;
	}
	
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
		
		 ViewHolder holder;

        if (convertView == null) {
        	convertView = inflater.inflate(itemLayout, null);
        	holder = new ViewHolder();
             
		   
  		   	convertView.setTag(holder);
  		 
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        
        holder.clientname = (TextView) convertView.findViewById(R.id.textClientName);
        holder.eventdesc = (TextView) convertView.findViewById(R.id.textEventDescription);
        holder.eventtype = (TextView) convertView.findViewById(R.id.textEventType);
        holder.eventvenue = (TextView) convertView.findViewById(R.id.textEventVenue);
        holder.eventdate = (TextView) convertView.findViewById(R.id.textEventDate);
        holder.packagecount = (TextView) convertView.findViewById(R.id.textPackageCount);
        holder.totalprice = (TextView) convertView.findViewById(R.id.textTotalPrice);
        
        TableEvents _Obj = _TableEvents.get(position);
        float totalPrice = _Obj.getPrimaryCost();
        int itemsCount = 0;
        List<TableQuoteItem> _TableQuoteItems =  dataRepository.GetQuoteItemByEventID(_Obj.getID());
        
        if(_TableQuoteItems != null){
        	itemsCount = _TableQuoteItems.size();
        	
        	for(TableQuoteItem item: _TableQuoteItems){
        		totalPrice += item.getTotalPrice();
        	}
        }

        SimpleDateFormat f = new SimpleDateFormat("d MMM yy HH:mm", Locale.US);
        
        Locale locale = new Locale("en_ZA");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
        
        String clientName = _Obj.getClient().getLastName() + " " + _Obj.getClient().getFullName();
        String eventType = _Obj.getEventType().getDescription();
        String eventDate = f.format(_Obj.getEventDate());
        
        holder.clientname.setText(clientName);
        holder.packagecount.setText("Packages: " + String.valueOf(itemsCount));
        holder.eventdate.setText(eventDate);
        holder.eventvenue.setText(_Obj.getEventVenue());
        holder.eventtype.setText(eventType);
        holder.eventdesc.setText(_Obj.getDescription());
        holder.totalprice.setText(fmt.format(totalPrice));
        
	    return convertView;
	}
	
	private class ViewHolder 
    {
         TextView totalprice;
         TextView eventdesc;
         TextView eventtype;
         TextView eventvenue;
         TextView eventdate;
         TextView packagecount;
         TextView clientname;
    }

}
