package com.eventplanner.adapters;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import com.eventplanner.R;
import com.eventplanner.dataclass.PackageItem;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class SelectPackageAdapter extends ArrayAdapter<PackageItem>{

	public List<PackageItem> packages;
	private LayoutInflater inflater;
    private int itemLayout;
    private boolean isHeading;
    
	public SelectPackageAdapter(Context context, int textViewResourceId,
			List<PackageItem> packages, boolean isHeading) {
		super(context, textViewResourceId, packages);
		// TODO Auto-generated constructor stub
		
		this.packages = packages;
		this.itemLayout = textViewResourceId;
		this.inflater = LayoutInflater.from(context);
		this.isHeading = isHeading;
	}
	
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
		
		 ViewHolder holder;

        if (convertView == null) {
        	convertView = inflater.inflate(itemLayout, null);
        	holder = new ViewHolder();
             
		   
  		   	convertView.setTag(holder);
  		 
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        
        holder.packagename = (TextView) convertView.findViewById(R.id.packagename);
        holder.quantity = (TextView) convertView.findViewById(R.id.quantity);
        holder.packageprice = (TextView) convertView.findViewById(R.id.price);
        holder.subtotal = (TextView) convertView.findViewById(R.id.subtotal);
        
        PackageItem _Obj = packages.get(position);
        
        holder.packagename.setText(_Obj.packagename);
        holder.quantity.setText(_Obj.quantiy);
        
        Locale locale = new Locale("en_ZA");
        NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
        
        if(isHeading){
        	holder.packageprice.setText(_Obj.price);
            holder.subtotal.setText(_Obj.subtotal);
        }
        else{
	        holder.packageprice.setText(fmt.format(Float.parseFloat(_Obj.price)));
	        holder.subtotal.setText(fmt.format(Float.parseFloat(_Obj.subtotal)));
        }
        
	    return convertView;
	}
	
	public void notifyChanges()
	{
		this.notifyDataSetChanged();
	}
	
	private class ViewHolder 
    {
		 TextView packageprice;
		 TextView quantity;
         TextView packagename;
         TextView subtotal;
    }
}
