package com.eventplanner.adapters;

import java.util.List;

import com.eventplanner.R;
import com.eventplanner.databasetables.TableClients;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ClientListAdapter extends ArrayAdapter<TableClients>{

	public List<TableClients> clients;
	private LayoutInflater inflater;
    private int itemLayout;
    public ClientListAdapter(Context context, int textViewResourceId,
			List<TableClients> clients) {
		super(context, textViewResourceId, clients);
		// TODO Auto-generated constructor stub
		
		this.clients = clients;
		this.itemLayout = textViewResourceId;
		this.inflater = LayoutInflater.from(context);
	}
	
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
		
		 ViewHolder holder;

        if (convertView == null) {
        	convertView = inflater.inflate(itemLayout, null);
        	holder = new ViewHolder();
             
		   
  		   	convertView.setTag(holder);
  		 
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        
        holder.clientname = (TextView) convertView.findViewById(R.id.clientname);
        holder.clientaddress = (TextView) convertView.findViewById(R.id.clientaddress);
        holder.clientemail = (TextView) convertView.findViewById(R.id.emailaddress);
        holder.clientcontactnumbers = (TextView) convertView.findViewById(R.id.contactnumber);
        holder.status = (TextView) convertView.findViewById(R.id.status);
        
        TableClients _Obj = clients.get(position);
        
        String clientName = _Obj.getLastName()+ " " + _Obj.getFullName().charAt(0);
        
        holder.clientname.setText(clientName);
        holder.clientaddress.setText(String.valueOf(_Obj.getAddress()));
        holder.clientemail.setText(_Obj.getEmail());
        
        if(_Obj.getIsActive() == 1){
        	holder.status.setText("Active");
        }else{
        	holder.status.setText("De-Activated");
        }
        
        String contacts = "";
        
        if(_Obj.getMobileNumber().length() > 0)
        	contacts = _Obj.getMobileNumber();
    	else if(_Obj.getTelNumber().length() > 0 )
    		contacts = _Obj.getTelNumber();
        
        holder.clientcontactnumbers.setText(contacts);
        
	    return convertView;
	}
	
	private class ViewHolder 
    {
         TextView clientname;
         TextView clientaddress;
         TextView clientemail;
         TextView clientcontactnumbers;
         TextView status;
    }
}
