package com.eventplanner.adapters;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.eventplanner.R;
import com.eventplanner.databasetables.TablePackages;

public class PackagesListAdapter extends ArrayAdapter<TablePackages>{

	private List<TablePackages> _TablePackages;
	private LayoutInflater inflater;
    private int itemLayout;
    public PackagesListAdapter(Context context, int textViewResourceId,
			List<TablePackages> _TablePackages) {
		super(context, textViewResourceId, _TablePackages);
		// TODO Auto-generated constructor stub
		
		this._TablePackages = _TablePackages;
		this.itemLayout = textViewResourceId;
		this.inflater = LayoutInflater.from(context);
	}

	
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
		
		 ViewHolder holder;

        if (convertView == null) {
        	convertView = inflater.inflate(itemLayout, null);
        	holder = new ViewHolder();
             
		   
  		   	convertView.setTag(holder);
  		 
        }else {
            holder = (ViewHolder) convertView.getTag();
        }
        
        Locale locale = new Locale("en_ZA");
		NumberFormat fmt = NumberFormat.getCurrencyInstance(locale);
		
        holder.description = (TextView) convertView.findViewById(R.id.desc);
        holder.price = (TextView) convertView.findViewById(R.id.price);
        
        TablePackages _Obj = _TablePackages.get(position);
        
        holder.description.setText(_Obj.getDescription());
        holder.price.setText(fmt.format(_Obj.getPrice()));
        
	    return convertView;
	}
	
	private class ViewHolder 
    {
         TextView description;
         TextView price;
    }

}
