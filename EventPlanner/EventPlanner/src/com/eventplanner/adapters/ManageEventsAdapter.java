package com.eventplanner.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.eventplanner.R;
import com.eventplanner.fragments.ManageEventsFragment;
import com.viewpagerindicator.IconPagerAdapter;

public class ManageEventsAdapter extends FragmentStatePagerAdapter implements IconPagerAdapter {
    protected static final String[] CONTENT = new String[] { "Events", "Packages", "Event Types", };
    protected static final int[] ICONS = new int[] {
            R.drawable.info,
            R.drawable.info,
            R.drawable.info,
            R.drawable.info
    }; 

    private int mCount = CONTENT.length;

    public ManageEventsAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
    	
        return ManageEventsFragment.newInstance(position);
    }

    @Override
    public int getCount() {
        return mCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
      return ManageEventsAdapter.CONTENT[position % CONTENT.length];
    }

    @Override
    public int getIconResId(int index) {
      return ICONS[index % ICONS.length];
    }

    public void setCount(int count) {
        if (count > 0 && count <= 10) {
            mCount = count;
            notifyDataSetChanged();
        }
    }
}